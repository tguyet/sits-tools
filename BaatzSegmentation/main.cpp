/*
 * main.cpp
 *
 *  Created on: 16 févr. 2013
 *      Author: tguyet
 */


#include "gdal.h"
#include "cpl_conv.h" /* for CPLMalloc() */
#include "cpl_string.h"
#include <iostream>
#include <vector>

using std::cout;
using std::endl;

#include "Baatz.h"

typedef char unsigned byte;

int main()
{
	GDALDatasetH  hDataset;

	GDALAllRegister();

	//hDataset = GDALOpen( "/home/tguyet/Enseignements/2012-2013/TASE-GAPE/ProgSIG/Tutoriel/data/MODIS/United_Kingdom_Ireland.2012247.terra.721.1km.tif", GA_ReadOnly );
	hDataset = GDALOpen( "/home/tguyet/Enseignements/2012-2013/TASE-GAPE/ProgSIG/QuickBird/Etoile1.tif", GA_ReadOnly );
	if( hDataset == NULL ) {
		exit(1);
	}

	int B=GDALGetRasterCount( hDataset );
	int W=GDALGetRasterXSize( hDataset );
	int H=GDALGetRasterYSize( hDataset );


	struct segment **segments_ptr_vector = NULL;
	struct segment *initial_segment = NULL;
	struct segment *final_segment = NULL;
	struct segmentation_parameters seg_parameters;

	float scale=50;
	float compactness=0.5;
	float color=0.1;
	seg_parameters.sp = scale;
	seg_parameters.wcmpt = compactness;
	seg_parameters.wcolor = color;
	seg_parameters.bands = B;
	//weights
	for (int i = 0; i < B; i++)
		seg_parameters.wband[i] = 1;

	float **float_input_image = (float **) malloc( B * sizeof(float) );
	for (int i = 0; i < B; i++)
		float_input_image[i] = (float *) malloc( H * W * sizeof(float) );


	GDALRasterBandH hBand;
	byte *pafScanline = (byte *) CPLMalloc(sizeof(byte)*W*H);
	for (int k=0; k<B; k++) {
		hBand = GDALGetRasterBand( hDataset, k+1 );
		GDALRasterIO( hBand, GF_Read, 0, 0, W, H, pafScanline, W, H, GDT_Byte, 0, 0 );
		for (int p=0; p<W*H; p++) {
			float_input_image[k][p] = (float) pafScanline[p];
		}
	}
	CPLFree(pafScanline);

	int  progress_enabled=0;
	initialize_segments(&segments_ptr_vector, &initial_segment, &final_segment, float_input_image, H, W, B, progress_enabled);
	segmentation(segments_ptr_vector, initial_segment, final_segment, H, W, seg_parameters, progress_enabled);

	GByte *output_image = (GByte *) CPLMalloc(sizeof(GByte)*W*H);
	write_segments(output_image, W, H, initial_segment, 0, progress_enabled);
	free_segment_list(&initial_segment, &segments_ptr_vector);


	const char *pszFormat = "GTiff";
	GDALDriverH hDriver = GDALGetDriverByName( pszFormat );
	char **papszOptions = NULL;
	GDALDatasetH hDstDS = GDALCreate( hDriver, "/home/tguyet/Enseignements/2012-2013/TASE-GAPE/ProgSIG/QuickBird/output_seg.tif",  W, H, 1, GDT_Byte, papszOptions );

	double adfGeoTransform[6];
	GDALGetGeoTransform(hDataset, adfGeoTransform);
	GDALSetGeoTransform(hDstDS, adfGeoTransform );

	if( hDstDS == NULL ) {
		GDALClose( hDstDS );
		exit(1);
	}

	GDALRasterBandH outputBand = GDALGetRasterBand( hDstDS, 1 );
	GDALRasterIO( outputBand, GF_Write, 0, 0, W, H, output_image, W, H, GDT_Byte, 0, 0 );

    /* Once we're done, close properly the dataset */
    GDALClose( hDstDS );
}


