#-*- coding: utf8 -*-
#!/bin/python

# file: staker.py
# author: M.C. Benyounes, T. Guyet
# year: 2013


################################
##    QQS FONCTIONS UTILES    ##
################################

import os

def singledate_stacker(inputdir, filename, srcwin=[], bands=["ndvi", "evi", "721"]):
    """
     Fonction qui extrait des bandes specifiques à partir d'une image MODIS multibandes hdf.
     Le fichier de sortie "tmp_comp.tif" correspond à un fichier contenant les trois bandes
     Le fichier de sortie "tmp_ndvi" correspond à un fichier contenant le NDVI
     Le fichier de sortie "tmp_evi" correspond à un fichier contenant le EVI

     \param inputdir : chemin (non-vide) vers les images
     \param filename : nom de l'image MODIS à traiter : la bande 2 comporte l'information PIR, et la bande 3 l'information Red.
     \param srcwin : fenetre utile exprimée en nombre de pixels [xoff yoff xsize ysize]
     \param bands : liste de bandes à extraire
    """
    #decompostion d'un fichier hdf en autant de bandes que necessaire + selection d'une sous-zone
    cmd="gdal_translate -q -sds "
    if(len(srcwin)==4):
        cmd=cmd+"-srcwin " + str(srcwin[0]) +" " + str(srcwin[1]) +" " + str(srcwin[2]) +" " + str(srcwin[3]) +" "
    cmd=cmd+inputdir+"/"+filename+".hdf tmp_img"
    os.system(cmd)
    print cmd
    if "721" in bands:
        os.system("gdal_merge.py -separate -o tmp_comp.tif tmp_img_04 tmp_img_05 tmp_img_07")
    if "ndvi" in bands:
        os.system("gdal_merge.py -separate -o tmp_ndvi.tif tmp_img_01")
    if "evi" in bands:
        os.system("gdal_merge.py -separate -o tmp_evi.tif tmp_img_02")
    #os.system("mv tmp_img1 tmp_ndvi")
    #os.system("mv tmp_img2 tmp_evi")
    os.system("rm tmp_img*")

def annual_stacker(inputdir, outputdir, basename, srcwin=[], bands=["ndvi", "evi", "721"], doclean=False):
    """
     Fonction de traitement par lot d'images satellite du répertoire inputdir
     Seuls les fichiers avec l'extension ".hdf" sont traités
     La fonction ne traite pas les dossiers récursivement.

     \param inputdir : chemin des images à traiter
     \param outputdir : chemin de sortie
     \param basename : début de nom des fichiers generé (sans extension)
     \param srcwin : définition d'une fenetre dans l'image à extraite (en pixels)
    """
    repfiles=os.listdir( inputdir )
    
    #creation du répertoire de sortie si nécessaire
    if not os.path.exists(outputdir):
        os.makedirs(outputdir)
    
    sorted_repfiles=sorted(repfiles) #les fichiers sont ordonnés pour être numerotés par dates.
    
    i=1
    param_str=""
    param_str_ndvi=""
    param_str_evi=""
    param_str_all=""

    for f in sorted_repfiles:
        if os.path.isdir(f):
            # On saute les cas où il s'agit d'un répertoire
            continue
        fileName, fileExtension = os.path.splitext(f)
        if fileExtension!=".hdf":
            # Si l'extension n'est pas ".hdf", on saute également ce fichier
            continue
        #ICI : on sait que f est le nom d'un fichier avec l'extension ".hdf"
        # on lance le traitement
        print "Extract bands from file : " + inputdir+"/"+f +"\n"
        singledate_stacker( inputdir, fileName, srcwin, bands)
        if "721" in bands:
            os.system("mv tmp_comp.tif "+outputdir+"/"+basename+"."+str(i)+".tif")
            param_str=param_str+" "+basename+"."+str(i)+".tif"
            param_str_all = param_str_all + " "+basename+"."+str(i)+".tif"
        if "ndvi" in bands:
            os.system("mv tmp_ndvi.tif "+outputdir+"/"+basename+"_ndvi."+str(i)+".tif")
            param_str_ndvi=param_str_ndvi + " " + basename+"_ndvi."+str(i)+".tif"
            param_str_all = param_str_all + " " + basename+"_ndvi."+str(i)+".tif"
        if "evi" in bands:
            os.system("mv tmp_evi.tif "+outputdir+"/"+basename+"_evi."+str(i)+".tif")
            param_str_evi = param_str_evi + " " + basename+"_evi."+str(i)+".tif"
            param_str_all = param_str_all + " " + basename+"_evi."+str(i)+".tif"
        i=i+1
        
    print "Merging images (this step must be long)\n"
    if len(bands)>1:
        cmd="cd "+outputdir+";gdal_merge.py -separate -o "+basename+"_all.tif " + param_str_all
        print "\t"+cmd +"\n"
        os.system(cmd)
    if "721" in bands:
        cmd="cd "+outputdir+";gdal_merge.py -separate -o "+basename+".tif " + param_str
        print "\t"+cmd +"\n"
        os.system(cmd)
        if doclean:
            os.system("cd "+outputdir+";rm -rf "+param_str)
    if "ndvi" in bands:
        cmd="cd "+outputdir+";gdal_merge.py -separate -o "+basename+"_ndvi.tif " + param_str_ndvi
        print "\t"+cmd +"\n"
        os.system(cmd)
        if doclean:
            os.system("cd "+outputdir+";rm -rf "+param_str_ndvi)
    if "evi" in bands:
        cmd="cd "+outputdir+";gdal_merge.py -separate -o "+basename+"_evi.tif " + param_str_evi
        print "\t"+cmd +"\n"
        os.system(cmd)
        if doclean:
            os.system("cd "+outputdir+";rm -rf "+param_str_evi)
        

def pluriannual_stacker(inputdir, years, outputdir, srcwin=[], bands=["ndvi", "evi", "721"], doclean=False):
    
    #creation du répertoire de sortie si nécessaire
    if not os.path.exists(outputdir):
        os.makedirs(outputdir)
    
    i=1
    param_str=""
    param_str_ndvi=""
    param_str_evi=""
    for y in years:
        basename = str(y)
        rep = inputdir + basename # !!! les repertoires doivent se terminer par l'annee !!!
        if not os.path.isdir(rep):
            # On saute les cas où ce ne soit pas un répertoire
            continue
        #construction des images pour l'année y
        annual_stacker(rep, outputdir, basename, srcwin, bands, doclean)
        if "721" in bands:
            param_str=param_str+" " +basename+".tif "
        if "ndvi" in bands:
            param_str_ndvi=param_str_ndvi+" " + basename+"_ndvi.tif "
        if "evi" in bands:
            param_str_evi=param_str_evi+" " + basename+"_evi.tif "
        
    print "Merging annual images (this step must be long)\n"
    if "721" in bands:
        cmd="cd "+outputdir+";gdal_merge.py -separate -o "+str(years[0])+"_"+str(years[len(years)-1])+".tif " + param_str
        print "\t"+cmd +"\n"
        os.system(cmd)
        if doclean:
            os.system("cd "+outputdir+";rm -rf "+param_str)
    if "ndvi" in bands:
        cmd="cd "+outputdir+";gdal_merge.py -separate -o "+str(years[0])+"_"+str(years[len(years)-1])+"_ndvi.tif " + param_str_ndvi
        print "\t"+cmd +"\n"
        os.system(cmd)
        if doclean:
            os.system("cd "+outputdir+";rm -rf "+param_str_ndvi)
    if "evi" in bands:
        cmd="cd "+outputdir+";gdal_merge.py -separate -o "+str(years[0])+"_"+str(years[len(years)-1])+"_evi.tif " + param_str_evi
        print "\t"+cmd +"\n"
        os.system(cmd)
        if doclean:
            os.system("cd "+outputdir+";rm -rf "+param_str_evi)
    return(outputdir+'/'+str(years[0])+"_"+str(years[len(years)-1]))


################################
##     PROGRAMME PRINCIPAL    ##
################################
if __name__=="__main__":
    year=2007
    homedir="/media/tguyet/955fa17f-904d-44b8-89d0-aa45f3c5a5f1/ImagesSen/MOD13Q1.5_250M_2002-2010/NDVI-EVI-"+str(year)
    srcwin=[1450, 1550, 2850, 2300] #[xoff, yoff, xsize, ysize] en nombre de pixels.

    #annual_stacker(homedir, "sub"+str(year), str(year), srcwin, bands=["ndvi", "evi", "721"])
    annual_stacker(homedir, "sub"+str(year), str(year), srcwin, bands=["ndvi"])
    #basename="subndvi/2005"
    #basename=pluriannual_stacker("/media/955fa17f-904d-44b8-89d0-aa45f3c5a5f1/ImagesSen/MOD13Q1.5_250M_2002-2010/NDVI-EVI-", [2001], "sub2001", srcwin, bands=['evi', 'ndvi'], doclean=False)

    # Les images produites ne sont pas dans une projection correcte.
    # Pour les caler, il faut les reprojeter avec la commande ci-dessous :
    """
    cmd='gdalwarp -t_srs "+proj=latlong +ellps=sphere" '+basename+'_'+vegindex+'.tif tmp.tif'
    os.system(cmd)
    cmd='rm -rf '+basename+'_'+vegindex+'.tif'
    os.system(cmd)
    cmd='gdal_translate -a_srs "EPSG:4326" tmp.tif tmp2.tif'
    os.system(cmd)
    cmd='rm -rf tmp.tif'
    os.system(cmd)
    cmd='gdalwarp -t_srs EPSG:32628 tmp2.tif '+basename+'_'+vegindex+'.tif'
    os.system(cmd)
    cmd='rm -rf tmp2.tif'
    os.system(cmd)
    """


