#-*- coding: utf8 -*-
import os
import urllib2

def tomonthstr(month):
    if( month==1 ):
        return "jan"
    elif( month==2 ):
        return "fev"
    elif( month==3 ):
        return "mar"
    elif( month==4 ):
        return "apr"
    elif( month==5 ):
        return "may"
    elif( month==6 ):
        return "jun"
    elif( month==7 ):
        return "jul"
    elif( month==8 ):
        return "aug"
    elif( month==9 ):
        return "sep"
    elif( month==10 ):
        return "oct"
    elif( month==11 ):
        return "nov"
    elif( month==12 ):
        return "dec"
    else:
        return ""
        
def compose_url(year, month, first=True, satid=7):
    if( year<1981 or len(str(year))!=4 or (not isinstance( year, int )) or (not isinstance( month, int )) ):
        return "error",""
    url="https://ecocast.arc.nasa.gov/data/pub/gimms/3g.v0/"
    if( year<1981 or len(str(year))!=4 ):
        return "",""
    elif( year < 1990 ):
        url += "1980s_new/"
    elif ( year < 2000 ):
        url += "1990s_new/"
    elif ( year < 2010 ):
        url += "2000s_new/"
    elif ( year < 2020 ):
        url += "2010s_new/"
    
    if first:
        url += "geo"+str(year)[2:]+tomonthstr(month)+"15a.n%02d-VI3g"%satid
        fout = "geo"+str(year)[2:]+tomonthstr(month)+"15a-VI3g"
    else:
        url += "geo"+str(year)[2:]+tomonthstr(month)+"15b.n%02d-VI3g"%satid
        fout = "geo"+str(year)[2:]+tomonthstr(month)+"15b-VI3g"
        
    return url,fout


def load_and_save(year, month, f=True, outputdir="images"):
    if not os.path.exists(outputdir):
        os.mkdir(outputdir)
    
    satids = [7,9,11,14,16,17,18,19]
    ii=0
    found=False
    while ii<len(satids):
        url,fin = compose_url(year, month, first=f, satid=satids[ii])
        print 'try '+ url+": ",
        try:
            #launch an exception if the ressource does not exists
            filehandler=urllib2.urlopen( url )
            print "ok!"
            found=True
            break
        except urllib2.HTTPError:
            print "oops!"
        ii += 1

    if found:
        fout =  open(outputdir+"/"+fin, "wb")
        fout.write( filehandler.read() )
        fout.close()

def batch_load(year, rep="images"):
    outputdir=rep
    for d in range(1,13):
        load_and_save(year, d, outputdir=rep)

#load all image of july from 1981 to 2002
for year in range(1981,2002):
    load_and_save(year, 7, outputdir="images")

