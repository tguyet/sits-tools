/**
- brief: NDVI3g to GTiff converter, convert the original data into a Float32 GTiff format with correct georeferencing (WGS84 SRS)
- author: T. Guyet
- date: 11/2014

Compilation:
g++ main.cpp -o converter -lgdal -I/usr/include/gdal -std=c++11
*/

#include <iostream>
#include <fstream>
#include <stdint.h>
#include <math.h>
#include <stdlib.h>
#include <string>

#include "gdal_priv.h"
#include "cpl_conv.h"
#include "cpl_string.h"
#include "ogr_spatialref.h"

using namespace std;

void usage(const char *comm);

int main(int argc, char **argv) {
    int16_t ndvi3g=0;
    short flagW=0;
    float ndvi=0.0;
    int i=0;
    
    const char *pszFormat = "GTiff";
    GDALDriver *poDriver;
    char **papszMetadata;
    
    std::string fin, fout;
    
    if(argc<2) {
        std::cerr << "Parameter error: not enough arguments." << endl;
		usage( argv[0] );
		return( EXIT_FAILURE );
    }
    
    for(int i=1;i<argc;i++) {
		if( !strcmp(argv[i],"-h") ) {
			usage(argv[0]);
			return EXIT_SUCCESS;
		} else if(!strcmp(argv[i],"-o")) {
			i++;
			fout = string(argv[i]);
	    } else {
			if( argv[i][0]=='-' ) {
				std::cerr << "Parameter error: Unknown parameter '" << argv[i] << "'." << endl;
				usage( argv[0] );
				return( EXIT_FAILURE );
			}
			fin = argv[i];
        }
    }
    
    if( fin.empty() ) {
        std::cerr << "Parameter error: no input file provided." << endl;
		usage( argv[0] );
		return( EXIT_FAILURE );
    }
    if( fout.empty() ) {
        fout = fin + ".tif";
    }
    
    // Read the input stream !
    float *abyRaster=nullptr;
    abyRaster = new float[4320*2160];
    if( abyRaster==nullptr) {
        cerr << "Not enough memory for the image size requested." <<endl;
        exit(1);
    }
    
    std::ifstream ifs(fin, ios::binary);
    if(ifs.fail()){
        std::cerr << "File '"<< fin <<"' does not exist." << endl;
		usage( argv[0] );
		return( EXIT_FAILURE );
    }

    while( ifs.good() ) {
        ndvi3g=0;
        ndvi3g |= ifs.get() << 8;
        ndvi3g |= ifs.get();
        flagW = ndvi3g-floor(ndvi3g/10)*10 + 1;
        ndvi = floor(ndvi3g/10)/1000;
        abyRaster[i]=ndvi;
        if( ndvi>1 || ndvi<-1 ) cout << ndvi3g << "," << flagW << "," << ndvi << endl;
        i++;
    }
    ifs.close();
    
    //save abyRaster as GTiff
    GDALAllRegister();

    poDriver = GetGDALDriverManager()->GetDriverByName(pszFormat);
    if( poDriver == NULL ) {
        cerr << "Invalid data format: the system can not read/write geotiff (missing GDAL Provider)" <<endl;
        exit( 1 );
    }
    
    papszMetadata = poDriver->GetMetadata();
    if( !CSLFetchBoolean( papszMetadata, GDAL_DCAP_CREATE, FALSE ) ) {
        cerr << "Driver "<<pszFormat<< " does not supports Create() method."<<endl;
        exit( 1 );
    }
    
    GDALDataset *poDstDS; 
    GDALRasterBand *poBand;
    char **papszOptions = NULL;
    
    poDstDS = poDriver->Create(fout.c_str(), 2160, 4320, 1, GDT_Float32, papszOptions );
    
    // geoTransform
    double adfGeoTransform[6] = { -180.0+1.0/24.0, 0, 0.0833, 90.0-1.0/24.0, -0.0833,0 };
    poDstDS->SetGeoTransform( adfGeoTransform );
    
    // SRS
    OGRSpatialReference oSRS;
    char *pszSRS_WKT = NULL;
    //oSRS.SetUTM( 11, TRUE );
    oSRS.SetWellKnownGeogCS( "WGS84" );
    oSRS.exportToWkt( &pszSRS_WKT );
    poDstDS->SetProjection( pszSRS_WKT );
    CPLFree( pszSRS_WKT );
    
    poBand = poDstDS->GetRasterBand(1);
    poBand->RasterIO( GF_Write, 0, 0, 2160, 4320 , abyRaster, 2160, 4320, GDT_Float32, 0, 0 );    

    // Once we're done, close properly the dataset
    GDALClose( (GDALDatasetH) poDstDS );
    delete[](abyRaster);
}


void usage(const char *comm) {
  printf("*******************\n");
  printf("NDVI3g to GTiff converter\n\n");
  printf("Brief : Convert the AVHRR NDVI3g files into GTiff format with standard\ngeoreferencing in WGS84. For more information about NDVI3g format, see\n https://nex.nasa.gov/nex/projects/1349/.\n");
  printf("Author : Guyet Thomas, AGROCAMPUS-OUEST/IRISA\n");
  printf("Year : 2014\n");
  printf("*******************\n\n");
  printf("command : %s [-o %%s] datafile\n", comm);
  printf("Options :\n");
  printf("\t -o : name of the output file (default add the '.tif' to the input file)\n");
  printf("\t datafile : name of the input file (NDVI3g format)\n");
}


