#-*- coding: utf8 -*-
#!/bin/python

import stacker

################################
##     PROGRAMME PRINCIPAL    ##
################################
if __name__=="__main__":
	year=2014
	homedir="/home/tguyet/Recherche/Donnees/Bretagne2014/"
	srcwin=[3100, 100, 1700, 1400] #[xoff, yoff, xsize, ysize] en nombre de pixels.
	vegindex='evi'

    ##Pour tests
	#stacker.singledate_stacker("~/Recherche/Donnees/Bretagne2014", "MOD13Q1.A2014001.h17v04.005.2014018104155", srcwin, bands=['ndvi'])
	
	stacker.annual_stacker(homedir, "sub"+str(year), str(year), srcwin, bands=["ndvi", "evi", "721"])


