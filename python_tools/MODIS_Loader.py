#-*- coding: utf8 -*-
#!/bin/python

"""
file: staker.py
author: T. Guyet
year: 2013
"""

#exemple http://e4ftl01.cr.usgs.gov/MOLT/MOD13Q1.005/2005.02.18/

#tile of the Senegal
H=16
V=7
#tile of the French Britany
H=17
V=4

from datetime import date
import urllib2

year=2014

urlbase="http://e4ftl01.cr.usgs.gov/MOLT/MOD13Q1.005/"
newyeardate=date(year, 1,1)
for i in range(1, 365, 16):
    url=urlbase
    d=date.fromordinal(newyeardate.toordinal()+i-1)
    url=url+d.strftime("%Y.%m.%d")+"/MOD13Q1.A"+str(year)+str(i).zfill(3)+".h%02dv%02d.005.2008201124358.hdf" % (H,V)
    print url
    ##NB: can not be automatized considering that the end of the file name changes !!
