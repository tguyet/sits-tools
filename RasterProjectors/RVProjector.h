/*
 * RVProjector.h
 *
 *  Created on: 26 oct. 2013
 *      Author: tguyet
 */

#ifndef RVPROJECTOR_H_
#define RVPROJECTOR_H_

#include "RasterProjector.h"
#include "ogrsf_frmts.h" //Pour les shapefiles
#include "gdal_priv.h"
#include "cpl_conv.h"
#include "FeatureStat.h"
#include <list>
#include <set>
#include <string>

class RVProjector : public RasterProjector {
protected:
	// constraint to add a limit to the size of the envelope of the geometry to process
	int _regionsizelimit;
	double _compacity_threshold;

	std::list<OGRFeature *> unprocessed_features;
public:
	RVProjector();
	RVProjector(std::string, std::string, int, std::string);
	virtual ~RVProjector();

	virtual int project();

	void setRegionSizeLimit(int val) {_regionsizelimit=val;};
	void setCompacityThreshold(double val) {_compacity_threshold=val;};
	int regionSizeLimit() const {return _regionsizelimit;};
	double compacityThreshold() const {return _compacity_threshold;};

	const std::list<OGRFeature *>& RemainingFeatures() const {return unprocessed_features;} ;

protected:
	FeatureStat* getclass(OGRFeature &feature, bool * =NULL);
};

#endif /* RVPROJECTOR_H_ */
