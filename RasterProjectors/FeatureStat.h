/*
 * FeatureStat.h
 *
 *  Created on: 26 oct. 2013
 *      Author: tguyet
 */

#ifndef FEATURESTAT_H_
#define FEATURESTAT_H_

#include "gdal_priv.h"
#include <map>
#include <string>
#include "ogrsf_frmts.h" //Pour les shapefiles

class variant {
public:
	int type;
	int ival; //type 1
	std::string sval; //type 2

	variant() {
		type=0;
		ival=0;
		sval=std::string("");
	}

	bool operator<(const variant& v) const
	{
		if( type==1 ) {
			return ival<v.ival;
		} else if(type==2) {
			return sval<v.sval;
		} else {
			return false;
		}
	}
};

class FeatureStat {
public:
	OGRGeometry *f;
	std::map<int8_t, float> stats;
	int featureid;

	static std::map<variant, int> attributesmap;

public:
	FeatureStat();
	virtual ~FeatureStat();

	static int getfeatureid(int);
	static int getfeatureid(std::string);
};

#endif /* FEATURESTAT_H_ */
