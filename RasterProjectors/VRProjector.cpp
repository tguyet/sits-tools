/*
 * VRProjector.cpp
 *
 *  Created on: 26 oct. 2013
 *      Author: tguyet
 */

#include "VRProjector.h"
#include <iostream>
#include <sstream>

VRProjector::VRProjector() {
	covshape = new OGRPolygon();
}

VRProjector::~VRProjector() {
	delete(covshape);
}

bool QueryResultCallback(FeatureStat* a_data, void* context) {
	VRProjector::callback_context *lcontext=(VRProjector::callback_context*)context;
	OGRPoint point(lcontext->x, lcontext->y);
	if ( a_data->f->Contains(&point) ) {
		lcontext->result=a_data;
		return false; //stop la recherche dans le R-Tree !
	} else {
		return true; //continue la recherche
	}
	return true;
}

/**
 * La fonction créé la recopie de la geometie
 */
void VRProjector::addGeometry(OGRFeature *feature, int value)
{
	OGRGeometry *poGeometry = feature->GetGeometryRef();
	if( poGeometry != NULL  && wkbFlatten(poGeometry->getGeometryType()) == wkbPolygon ) {
		if(poCT){
			//Transformation de la geometry dans le referenciel de l'image
			poGeometry->transform(poCT);
		}

		OGRPolygon *poPoly = (OGRPolygon *) poGeometry;
		OGREnvelope psEnvelope;
		poPoly->getEnvelope(&psEnvelope);

		//mise à jour de la forme covshape par merges successifs
		OGRPolygon* pTemp =(OGRPolygon*)(covshape->Union( poPoly ) );
		if (pTemp != NULL) {
			OGRPolygon *old=covshape;
			covshape=pTemp;
			delete(old);
		} else {
			cerr << "geometry union error" << endl;
		}

		//Construction d'un élément permettant de retenir des statistiques de couche
		FeatureStat *fs=new FeatureStat();
		fs->f=poPoly->clone();

		if( !cl_attribute.empty() ) {
			fieldindex=feature->GetFieldIndex(cl_attribute.c_str());
			if( fieldindex==-1) {
				cerr << "field "<< cl_attribute << " does not exists"<<endl;
			} else {
				if( feature->GetFieldDefnRef(fieldindex)->GetType()==OFTInteger ) {
					int val=feature->GetFieldAsInteger(cl_attribute.c_str());
					fs->featureid = FeatureStat::getfeatureid(val);
				} else if (feature->GetFieldDefnRef(fieldindex)->GetType()==OFTString) {
					std::string s=feature->GetFieldAsString(cl_attribute.c_str());
					fs->featureid = FeatureStat::getfeatureid(s);
				} else {
					cerr << "unknown field type"<<endl;
				}
			}
		}

		//Indexation de l'élément dans le R-Tree
#if RTREE == LOCAL_RTREE
		float dmin[2];dmin[0]=psEnvelope.MinX;dmin[1]=psEnvelope.MinY;
		float dmax[2];dmax[0]=psEnvelope.MaxX;dmax[1]=psEnvelope.MaxY;
		tree.Insert(dmin, dmax, fs);
#elif RTREE == GEOM_RTREE
		Envelope *envelope = new Envelope(psEnvelope.MinX, psEnvelope.MaxX, psEnvelope.MinY, psEnvelope.MaxY);
		tree.insert(envelope, (void*)fs);
#elif RTREE == BOOST_RTREE
#endif
		_stats.push_back(fs);
	} else {
		cerr << "no polygon geometry" << endl;
	}
}

/**
 *
 */
void VRProjector::openshp(string filename) {
	OGRDataSource *poDS;

	//Ouverture du fichier en lecture
	poDS = OGRSFDriverRegistrar::Open( filename.c_str(), FALSE );
	if( poDS == NULL ) {
		printf( "Open failed.\n" );
		exit( 1 );
	}
	OGRLayer  *poLayer;
	poLayer = poDS->GetLayer(0);

	OGRFeature *poFeature;
	poLayer->ResetReading();
	while( (poFeature = poLayer->GetNextFeature()) != NULL ) {
		addGeometry(poFeature);
		OGRFeature::DestroyFeature( poFeature );
	}
	OGRDataSource::DestroyDataSource( poDS );
}

/**
 * \param bandid : numéro de bande (entre 1 et infty)
 * \param poDataset : image raster (supposé codé en Byte !!
 * \param tree : contient toutes les FeatureStat qui vont être "remplies" au fur et à mesure
 * \param coveringshape : polygone englobant les FeaturesStats
 * \param _values : (out) est
 */
int VRProjector::project()
{
	if( poBand == NULL ) return 1;

	/////////// Chargement du fichier  ////////////////
	int sx=poDataset->GetRasterXSize();
	int sy=poDataset->GetRasterYSize();
	double MinX,MinY,MaxX,MaxY;

	MinX = geotransform[0] + 0*geotransform[1] + sy*geotransform[2];
	MinY = geotransform[3] + 0*geotransform[4] + sy*geotransform[5];
	MaxX = geotransform[0] + sx*geotransform[1] + 0*geotransform[2];
	MaxY = geotransform[3] + sx*geotransform[4] + 0*geotransform[5];

	double xstep = (MaxX - MinX) / sx;
	double ystep = (MaxY - MinY) / sy;

	//ligne correspondant aux FeatureStat associé aux pixels de la ligne courante
	FeatureStat** featurerow = (FeatureStat**)calloc( sizeof(FeatureStat*), sx);
	//ligne correspondant aux FeatureStat associé aux pixels de la ligne au dessus
	FeatureStat** featurerow_old = (FeatureStat**)calloc( sizeof(FeatureStat*), sx);

	//Allocation mémoire
	memset(featurerow, 0, sizeof(FeatureStat*)*sx);
	memset(featurerow_old, 0, sizeof(FeatureStat*)*sx);

	//Chargement de l'image en mémoire
	char *pafScanline = (char *)CPLMalloc(sizeof(char)* sx * sy);
	if(pafScanline==NULL) {
		std::cerr << "memory allocation error" << std::endl;
		GDALClose(poDataset);
		return 1;
	}
	if( _datatype== GDT_Byte) {
		poBand->RasterIO( GF_Read, 0, 0, sx, sy, pafScanline, sx, sy, GDT_Byte, 0, 0 );
	} else if(_datatype==GDT_Int32) {
		int32_t *intScanline = (int32_t *)CPLMalloc(sizeof(int32_t)* sx * sy);
		if(intScanline==NULL) {
			cerr << "memory allocation error" << endl;
			return 1;
		}
		poBand->RasterIO( GF_Read, 0, 0, sx, sy, intScanline, sx, sy, GDT_Int32, 0, 0 );
		for(int i=0;i<sx*sy;i++){
			if( INT8_MAX<intScanline[i] ) {
				cerr << "warning : int8 conversion error"<<endl;
			}
			pafScanline[i] = (int8_t)intScanline[i];
		}
		CPLFree(intScanline);
	} else {
		cerr << "VRProjector::project : band type issue"<<endl;
		return 1;
	}

	callback_context context; //objet utilisé pour la recherche dans le RTree (contient des coordonnées et le résultat)
	context.result=NULL;

	int lx=0, ly=sy;
	float x[2], y[2];
	for(int i=0; i<sx*sy; i++ ) { //Pour chaque pixel ...
		lx=i%sx;

		if( !lx ) {
			// On change de ligne
			// Debut de ligne : on inverse les deux lignes
			FeatureStat** frow_tmp = featurerow;
			featurerow = featurerow_old;
			featurerow_old=frow_tmp;

			// On remet le résultat à NULL pour éviter de faire la vérification d'inclusion d'un bout de ligne à l'autre
			context.result=NULL;

			// Affichage de la progression à chaque ligne traitée
			printf("%.2lf%%", 100*(float)i/(float)(sx*sy));
			fflush(stdout);
			printf("\r");
			ly=sy-(i-i%sx)/sx;
		}

		//On récupère les coordonnées dans un point
		x[0] = MinX + lx*xstep; x[1]=x[0];
		y[0] = MinY + ly*ystep; y[1]=y[0];
		OGRPoint point(x[0], y[0]);

		//Si le pixel n'est pas dans la forme englobante alors, on ne le traite pas ...
		if( covshape && !covshape->Contains(&point)) {
			continue;
		}

		//Si on avait un résultat précédent, on regarde en priorité si le polygone précédent ne contient pas la forme
		if(!context.result || !context.result->f->Contains(&point) ) {
			//ICI, le résultat précédent (juste à gauche) n'a rien donné
			// On regarde donc si il y a une correspondance juste au dessus
			if( featurerow_old[lx] && featurerow_old[lx]->f->Contains(&point) ) {
				context.result=featurerow_old[lx];
			} else {
				// ICI, il n'y avait pas de resultat précédent (en x ou y)
				// ou il faut changer de polygone car il ne contient pas le nouveau point

				context.x=x[0];
				context.y=y[0];
				context.result=NULL;
#if RTREE == LOCAL_RTREE
				tree.Search(x, y, &QueryResultCallback, (void*)(&context));
#elif RTREE == GEOM_RTREE
				Envelope *envelope=new Envelope(x[0],x[1],y[0],y[1]);
				vector<void *> results;
				tree.query(envelope, results);
				for (unsigned int i = 0; i < results.size(); i++) {
					FeatureStat *fs=(FeatureStat *) results[i];
					if ( fs->f->Contains(&point) ) {
						context.result=fs;
						break;
					}
				}
#elif RTREE == BOOST_RTREE
#endif
			}
		}
		if( context.result!=NULL ) {
			//ICI. on a un résultat de la recherche dans le RTree
			_values.insert((int)(pafScanline[i]));
			context.result->stats[(int)(pafScanline[i])] ++;
		}

		//On retient le FeatureStat pour ce pixel!
		if(context.result) featurerow[lx]=context.result;
	}

	//Libération mémoire
	free(pafScanline);
	free(featurerow);
	free(featurerow_old);

	std::cout << "\t\tPROJECTION FINISHED" << std::endl;
	GDALClose(poDataset);
	return 0;
}
