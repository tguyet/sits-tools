/*
 * FeatureStat.cpp
 *
 *  Created on: 26 oct. 2013
 *      Author: tguyet
 */

#include "FeatureStat.h"
using namespace std;

FeatureStat::FeatureStat() {
	f=NULL;
	featureid=-1;
}

FeatureStat::~FeatureStat() {
	if(f) delete(f);
}

map<variant,int> FeatureStat::attributesmap = map<variant,int>();

int FeatureStat::getfeatureid(int val) {
	map<variant,int>::iterator it=attributesmap.begin();
	while(it!=attributesmap.end()) {
		if((*it).first.ival==val) {
			return (*it).second;
		}
		it++;
	}
	int id=attributesmap.size();
	variant v;
	v.type=1;
	v.ival=val;
	attributesmap[v]=id;
	return id;
}

int FeatureStat::getfeatureid(std::string val) {
	map<variant,int>::iterator it=attributesmap.begin();
	while(it!=attributesmap.end()) {
		if((*it).first.sval==val) {
			return (*it).second;
		}
		it++;
	}
	int id=attributesmap.size();
	variant v;
	v.type=2;
	v.sval=val;
	attributesmap[v]=id;
	return id;
}
