/*
 * RVProjector.cpp
 *
 *  Created on: 26 oct. 2013
 *      Author: tguyet
 */

#include "RVProjector.h"
#include <iostream>
#include <string>
#include <sstream>
#include <cstdint>


using namespace std;

RVProjector::RVProjector() {
	_regionsizelimit=0;
	_compacity_threshold=0.0;
}

RVProjector::RVProjector(std::string rfn, std::string vfn, int bid, std::string att) : RasterProjector(rfn, vfn, bid, att) {
	_regionsizelimit=0;
	_compacity_threshold=0.0;
}

RVProjector::~RVProjector() {
}

FeatureStat* RVProjector::getclass(OGRFeature &feature, bool *remaining) {
	if(poBand==NULL) {
		cerr << "no raster band defined"<<endl;
		return NULL;
	}
	if(remaining) (*remaining)=0;

	OGRGeometry *thegeom = feature.GetGeometryRef();
	if(poCT){
		//Transformation de la geometry dans le referenciel de l'image
		thegeom->transform(poCT);
	}
	OGREnvelope envelope;
	thegeom->getEnvelope(&envelope);
	//printf("%lf %lf %lf %lf\n", envelope.MinX,envelope.MaxX ,envelope.MinY,envelope.MaxY);

	double den = (geotransform[1]*geotransform[5] - geotransform[4]*geotransform[2]);
	int i1= (int)( ((envelope.MinX - geotransform[0])*geotransform[5] - (envelope.MinY - geotransform[3])*geotransform[2])/den );
	int j1= (int)( ((envelope.MinY - geotransform[3])*geotransform[1] - (envelope.MinX - geotransform[0])*geotransform[4])/den );

	int i2= (int)( ((envelope.MaxX - geotransform[0])*geotransform[5] - (envelope.MaxY - geotransform[3])*geotransform[2])/den );
	int j2= (int)( ((envelope.MaxY - geotransform[3])*geotransform[1] - (envelope.MaxX - geotransform[0])*geotransform[4])/den );

	int imin= std::min(i1, i2);
	int jmin= std::min(j1, j2);
	int jmax= std::max(j1, j2);
	int imax= std::max(i1, i2);

	int sx = imax-imin;
	int sy = jmax-jmin;

	//keep only shapes inside the raster extent
	if( imin<0 || jmin<0 || imax>poDataset->GetRasterXSize() || jmax>poDataset->GetRasterYSize() ) {
		return NULL;
	}
	//printf("taille : %d %d\npos: %d %d\n", sx,sy, imin, jmin);

	double xstep = (envelope.MaxX - envelope.MinX) / sx;
	double ystep = (envelope.MaxY - envelope.MinY) / sy;

	//Si sx*sy est trop grand, on zappe !!
	if(_regionsizelimit && sx*sy>_regionsizelimit) {
		if(remaining) (*remaining)=1;
		return NULL;
	}

	double env_area=(envelope.MaxX - envelope.MinX)*(envelope.MaxY - envelope.MinY);
	double rapport = ((OGRPolygon*)thegeom)->get_Area()/env_area;

	if( rapport<_compacity_threshold ) {
		if(remaining) (*remaining)=1;
		return NULL;
	}

	//On travaille uniquement avec des shorts !!
	int8_t *pafScanline = (int8_t *)CPLMalloc(sizeof(int8_t)* sx * sy);
	if(pafScanline==NULL) {
		cerr << "RVProjector::getclass Error : memory allocation error for int8_t matrix of size "<<sx <<"x"<<sy << endl;
		return NULL;
	}
	//Chargement de la partie de l'image en mémoire
	if( _datatype== GDT_Byte) {
		poBand->RasterIO( GF_Read, imin, jmin, sx, sy, pafScanline, sx, sy, GDT_Byte, 0, 0 );
	} else if ( _datatype==GDT_Int32 ) {
		int32_t *intScanline = (int32_t *)CPLMalloc(sizeof(int32_t)* sx * sy);
		if(intScanline==NULL) {
			cerr << "RVProjector::getclass Error : memory allocation error for int32_t matrix of size "<<sx <<"x"<<sy << endl;
			return NULL;
		}
		poBand->RasterIO( GF_Read, imin, jmin, sx, sy, intScanline, sx, sy, GDT_Int32, 0, 0 );
		for(int i=0;i<sx*sy;i++){
			if( INT8_MAX<intScanline[i] ) {
				cerr << "RVProjector::getclass Warning : int8 conversion error"<<endl;
			}
			pafScanline[i] = (int8_t)intScanline[i];
		}
		CPLFree(intScanline);

	} else {
		cerr << "RVProjector::getclass Error : band type issue"<<endl;
		return NULL;
	}

	FeatureStat * statfeat = new FeatureStat();

	int lx=0, ly=sy;
	double x, y;
	for(int i=0; i<sx*sy; i++ ) { //Pour chaque pixel ...
		lx=i%sx;
		if( !lx ) { // Affichage de la progression à chaque ligne traitée
			printf("%.2lf%%", 100*(float)i/(float)(sx*sy));
			fflush(stdout);
			printf("\r");
			ly=sy-(i-i%sx)/sx;
		}

		//On récupère les coordonnées dans un point
		x = envelope.MinX + lx*xstep;
		y = envelope.MinY + ly*ystep;

		OGRPoint point(x, y);

		//On regarde l'envelope en premier
		//Si le pixel n'est pas dans la boite englobante alors, on ne le traite pas ...
		if( x<envelope.MinX || x>envelope.MaxX || y<envelope.MinY || y>envelope.MaxY ) {
			continue;
		}

		if( thegeom->Contains(&point) ) {
			//On met à jour les _stats
			_values.insert((int)(pafScanline[i]));
			statfeat->stats[(int)(pafScanline[i])] ++;
		}
	}

	statfeat->f=feature.GetGeometryRef()->clone();
	if( !cl_attribute.empty() ) {
		fieldindex=feature.GetFieldIndex(cl_attribute.c_str());
		if( fieldindex==-1) {
			cerr << "field "<< cl_attribute << " does not exists"<<endl;
		} else {
			if( feature.GetFieldDefnRef(fieldindex)->GetType()==OFTInteger ) {
				int val=feature.GetFieldAsInteger(cl_attribute.c_str());
				statfeat->featureid = FeatureStat::getfeatureid(val);
			} else if (feature.GetFieldDefnRef(fieldindex)->GetType()==OFTString) {
				std::string s=feature.GetFieldAsString(cl_attribute.c_str());
				statfeat->featureid = FeatureStat::getfeatureid(s);
			} else {
				cerr << "unknown field type"<<endl;
			}
		}
	}

	//Libération mémoire
	CPLFree(pafScanline);
	return statfeat;
}


/**
 * NB: les pixels non-couverts par une forme geometrique sont ignorés
 */
int RVProjector::project() {
	cout << "Project features of size <" << _regionsizelimit << ", compacity min : "<< _compacity_threshold<<endl;
	if(!poBand) {
		cerr << "RVProjector::project error : define the raster layer and the band before projecting"<<endl;
		return 1;
	}

	OGRDataSource *poDS;

	//Ouverture du fichier en lecture
	poDS = OGRSFDriverRegistrar::Open( _vfn.c_str(), FALSE );
	if( poDS == NULL ) {
		printf( "Open failed.\n" );
		return 1;
	}
	OGRLayer  *poLayer;
	poLayer = poDS->GetLayer(0);

	int nbf = poLayer->GetFeatureCount();
	int i=0;
	OGRFeature *poFeature;
	poLayer->ResetReading();
	while( (poFeature = poLayer->GetNextFeature()) != NULL ) {
		// Affichage de la progression toutes les 100 features
		if((i++)%100==0) printf("feature %d/%d\n", i, nbf);
		bool remaining;
		FeatureStat *fstat = getclass(*poFeature, &remaining);
		if(fstat) {
			_stats.push_back(fstat);
			OGRFeature::DestroyFeature( poFeature );
		} else {
			if(remaining) {
				unprocessed_features.push_back(poFeature);
			}
		}
	}
	OGRDataSource::DestroyDataSource( poDS );
	return 0;
}
