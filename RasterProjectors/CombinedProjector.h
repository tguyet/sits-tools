/*
 * CombinedProjector.h
 *
 *  Created on: 26 oct. 2013
 *      Author: tguyet
 */

#ifndef COMBINEDPROJECTOR_H_
#define COMBINEDPROJECTOR_H_

#include "RasterProjector.h"
#include "RVProjector.h"
#include "VRProjector.h"
#include "FeatureStat.h"
#include <string>
#include <list>
#include <set>

class CombinedProjector : public RasterProjector {
protected:
	VRProjector VRP;
	RVProjector RVP;
public:
	CombinedProjector(std::string rasterfilename, std::string vectorfilename, int bid=0, std::string att = std::string(""));
	virtual ~CombinedProjector();

	void setRegionLimit(int val) {RVP.setRegionSizeLimit(val);};
	void setCompacityThreshold(double val) {RVP.setCompacityThreshold(val);};

	virtual int project();
};


#endif /* COMBINEDPROJECTOR_H_ */
