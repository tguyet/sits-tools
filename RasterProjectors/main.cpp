/*
 * main.cpp
 *
 *  Created on: 22 juillet 2012
 *      Author: guyet
 *
 *      Outil permettant de projeter une image raster sur les géométries d'une couche vecteur.
 *      Le résultat des traitements est un shapefile contenant toute les géométrie du shapefile d'entrée.
 *      Les attributs des géométries sont :
 *      	- la classe majoritaire : ie la classe dont le nombre de pixel apparait le plus de fois dans la forme
 *      	- le nombre de pixel dans la forme
 *      	- la proportion des pixels pour chaque classe
 *
 *      Les prérequis :
 *      	- images vecteurs et rasters en entrée doivent être dans le même CRS
 *      	- la bande sélectionnée de l'image raster doit être une couche en Byte (entiers sur 8 bits)
 *
 *      La partie calculatoire du code concerne l'identification de la forme correspondant spatialement à un pixel.
 *      Pour améliorer les performances de requetage, les geometries sont sauvegardées dans un R-Tree.
 *      Lors de la recherche, on regarde d'abord sur les pixels adjacents déjà construits (à gauche et en bas) si leurs geometrie ne conviendrait pas (on limite ainsi les recherches).
 *      Néanmoins, le temps de calcul restent importants. Notez que le début de la recherche est très lent mais va continuellement en s'accélérant. Il ne faut donc pas se décourager trop tôt. Les 50 derniers % du traitement se font aussi vite que les 3 premiers.
 *
 *
 *      Compilation :
 *          requirements:
 *              libgeos++-dev: pour les index spatiaux
 *              libgdal-dev: pour les formats de fichiers
 *
 *          Commande:
 *              g++ -O3 *.cpp -I/usr/include/gdal/ -I/usr/include/geos/ -std=c++11 -lgdal -lgeos -o RasterProjector
 *
 *      TODO :
 *      - Assurer la projection entre couche de CRS différents (actuellemment, ce n'est pas fait)
 *      - Généraliser à différents couches d'entrées (en raster)
 *      - l'utilisation d'une librairie "artisannale" pour le R-Tree conduit à des performances un peu limitée. Étrangement la recherche est efficace pour les premiers éléments insérés dans l'arbre mes plutôt lente pour les derniers. Le passage à des R*-Tree serait certainement pertinent à la vue des données
 *      	-> utilisation de la librairie spatialindex-src-1.7.1 pourrait améliorer les performances (semble implémenter des R*-Tree, mais facilite l'expérimentation de ses différentes structures)
 *      - une analyse des performances reste à faire (gprof etc.)
 *      - Transfert en python pour portabilité (?)
 */



#include "gdal_priv.h"
#include <list>
#include <set>
#include <map>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "cpl_conv.h"
#include "ogrsf_frmts.h" //Pour les shapefiles

#include "FeatureStat.h"
#include "VRProjector.h"
#include "RVProjector.h"
#include "CombinedProjector.h"

using namespace std;


/**
 * TODO gestion des SRS !! Actuellement en dur !
 */

void usage(const char *comm);

int main(int argc, char **argv)
{
	int method=1;
	int bandid;
	string att;
	int SizeLimit=0;
	float CompacityThreashold=0;
	string hdrfilename;
	string shpfilename;
	string outputfilename;
	string outputcsv;

	for(int i=1;i<argc;i++) {
		if( !strcmp(argv[i],"-h") ) {
					usage(argv[0]);
					return EXIT_SUCCESS;
		} else if( !strcmp(argv[i],"-rv") ) {
			method=1;
		} else if( !strcmp(argv[i],"-vr") ) {
			method=2;
		} else if( !strcmp(argv[i],"-c") ) {
			method=0;
		} else if(!strcmp(argv[i],"-s")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&SizeLimit) || SizeLimit<0 ) {
				fprintf(stderr, "Parameter error: Expected positive integer after -s\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-b")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&bandid) || bandid<0 ) {
				fprintf(stderr, "Parameter error: Expected positive integer after -b\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-ct")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%f",&CompacityThreashold) || CompacityThreashold<0 ) {
				fprintf(stderr, "Parameter error: Expected positive value after -ct\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-o")) {
			i++;
			outputfilename=argv[i];
		} else if( !strcmp(argv[i],"-shp") ) {
			i++;
			shpfilename=argv[i];
		} else if( !strcmp(argv[i],"-csv") ) {
			i++;
			outputcsv=argv[i];
		} else if( !strcmp(argv[i],"-a") ) {
			i++;
			att=argv[i];
		} else {
			//C'est un fichier à charger
			hdrfilename = argv[i];
		}
	}

	if( shpfilename.length()==0 || hdrfilename.length()==0 ){
		cerr << "Raster file AND Shapefile are both required."<<endl;
		usage( argv[0] );
		exit( EXIT_FAILURE );
	}

	if( outputfilename.length()==0 ) {
		outputfilename = hdrfilename+".shp";
	}

	//suppression du fichier si necessaire
	ifstream ifile(outputfilename);
	if (ifile) {
		ifile.close();
		cout << "file " << outputfilename << " exists. Are you sure to overwrite it ? [yn] ";
		string s;
		cin >> s;
		if( s =="Y" || s=="yes" || s=="Yes" ) {
			if( remove( outputfilename.c_str() ) )
				cerr << "Error deleting file " << outputfilename << endl;
		}
	}

	if( outputcsv.length()!=0 && att.length()==0) {
		cerr << "An attribute name (-a) is required for the construction of the confusion matrix." <<endl;
		exit(EXIT_SUCCESS);
	}

	/*
	string hdrfilename("/home/guyet/Recherche/Donnees/PayTal/test_classif_decoupe3.tif");
	string shpfilename("/home/guyet/Recherche/Donnees/PayTal/Cadastre2.shp");
	string ouputfilename("/home/guyet/Recherche/Donnees/PayTal/Cadastre2_output.shp");
	*/

	/*
	string hdrfilename("/home/tguyet/Recherche/Donnees/PayTal/Zone56166/DecoupeClassif.tif");
	string shpfilename("/home/tguyet/Recherche/Donnees/PayTal/Cadastre56166/PARCELLE.shp");
	string ouputfilename("/home/tguyet/Recherche/Donnees/PayTal/Zone56166/proj.shp");
	*/

	/*
	string hdrfilename("/home/tguyet/Recherche/Donnees/Senegal/ClassifsResults/kelkome/dec_2001_2004_c7_c15");
	string shpfilename("/home/tguyet/Recherche/Donnees/Senegal/Vegetation/sen_lc_dd_agg.shp");
	string outputfilename("/home/tguyet/Recherche/Donnees/Senegal/ClassifsResults/kelkome/proj.shp");
	string outputcsv("/home/tguyet/Recherche/Donnees/Senegal/ClassifsResults/kelkome/proj.csv");
	*/

	/*
	string hdrfilename("/home/tguyet/Recherche/Donnees/Senegal/ClassifsResults/output_249_1827/dec_2001_2005_c10_c50");
	//string shpfilename("/home/tguyet/Recherche/Donnees/Senegal/Vegetation/sen_lc_dd_agg.shp");
	string shpfilename("/home/tguyet/Recherche/Donnees/Senegal/Vegetation/decoupe/vegetationsenegal_decoupe.shp");
	string outputfilename("/home/tguyet/Recherche/Donnees/Senegal/ClassifsResults/output_249_1827/proj.shp");
	string outputcsv("/home/tguyet/Recherche/Donnees/Senegal/ClassifsResults/output_249_1827/proj.csv");
	bandid=1;
	att="USERLABEL"
	*/

	GDALAllRegister();
	OGRRegisterAll();

	RasterProjector *projector;
	if(method==2) {
		cout << "*** VR projector ***"<<endl;
		projector = new VRProjector();
		/*
		VRProjector projector;
		cout << "Construct tree"<<endl;
		projector.openshp(shpfilename);
		cout << "Project on features"<<endl;
		projector.project();
		cout << "Write shapefile"<<endl;
		projector.write(outputfilename);
		*/
	} else if (method==1) {
		cout << "*** RV projector ***"<<endl;
		projector = new RVProjector(hdrfilename, shpfilename, bandid, att);
		cout << "Open raster file"<<endl;
		if(!projector->setDataset(hdrfilename,1)) {
			((RVProjector*)projector)->setRegionSizeLimit(0);
			((RVProjector*)projector)->setCompacityThreshold(0);
			cout << "Project on features"<<endl;
			projector->project();

			cout << "#unprocessed features : " << ((RVProjector*)projector)->RemainingFeatures().size()<<endl;
		}
	} else if (method==0) {
		cout << "*** Combined projector ***"<<endl;
		projector=new CombinedProjector(hdrfilename, shpfilename, bandid, att);
		((CombinedProjector*)projector)->setRegionLimit(100000);
		((CombinedProjector*)projector)->setCompacityThreshold(0.1);
		projector->project();
	}
	if( outputcsv.length()!=0 && att.length()!=0 ) {
		projector->confusion(outputcsv);
	}
	projector->write(outputfilename);

	return EXIT_SUCCESS;
}

/**
* \brief Fonction d'information sur l'usage du programme
*/
void usage(const char *comm)
{
  printf("*******************\n");
  printf("Raster on vector projector\n\n");
  printf("Brief : Project raster on a vector layer : for each feature of the vector layer the majority class of the raster is attributed to the feature class (the layer store the distribution of pixels classes).\n");
  printf("Author : Guyet Thomas, AGROCAMPUS-OUEST\n");
  printf("Year : 2013\n");
  printf("*******************\n\n");
  printf("command : %s [-s %%d] [-ct %%f] [-c|-rf|-vr] [-csv [csvfile]] -shp shpfile rasterfile\n\n", comm);
  printf("Options :\n");
  printf("\t -h : help\n");
  printf("\t -s : maximum size of a shape to process (default 0)\n");
  printf("\t -ct : compacity threshold for processing a shape (default 0)\n");
  printf("\t -o : output file (default: postfix the input with '.shp') \n");
  printf("\t -csv : output file  for confusion matrix (default None)\n");
  printf("\t -b : band id of the raster file to process (default 1)\n");
  printf("\t -a : attribute for confusion matrix (default None)\n");
  printf("\t -c, -rv, -vr : combined, RasterVector, VectorRaster method  (default -rv)\n");
  printf("\t -shp : input vector file  for confusion matrix (required)\n");
  printf("\t rasterfile : HDR/TIFF datafile (required)\n");
}
