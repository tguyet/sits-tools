/*
 * RasterProjector.h
 *
 *  Created on: 26 oct. 2013
 *      Author: tguyet
 */

#ifndef RASTERPROJECTOR_H_
#define RASTERPROJECTOR_H_

#include <set>
#include <list>
#include <string>
#include "FeatureStat.h"
#include "ogrsf_frmts.h"

class RasterProjector {
protected:
	std::string _rfn, _vfn;
	std::set<int> _values; //rempli avec la liste des valeurs qui peuvent être prises par la bande raster
	std::list<FeatureStat*> _stats;

	// dataset characteristics
	GDALDataset *poDataset;
	GDALDataType _datatype;
	GDALRasterBand *poBand;
	double geotransform[6];
	int bandid;
	std::string cl_attribute;
	int fieldindex;
	OGRCoordinateTransformation *poCT;
public:
	RasterProjector();
	RasterProjector(std::string rasterfilename, std::string vectorfilename, int bid, std::string att);
	virtual ~RasterProjector();

	virtual int project() =0;

	void setVectorDataset(std::string vectorfilename, std::string att =std::string(""));
	void setAttribute(std::string s){
		cl_attribute=s;
	};
	int setDataset(std::string hdrfilename, int bid=0);
	int setBandId(int id);

	const std::list<FeatureStat *>& Stats() const {return _stats;};
	const std::set<int>& Values() const {return _values;};

	int write(std::string output);

	/**
	 * NB: pour le calcul d'une matrice de confusion, il est préférable de passer
	 * par une rasterization du vecteur
	 */
	void confusion(std::string output);

	void clear();
};


#endif /* RASTERPROJECTOR_H_ */
