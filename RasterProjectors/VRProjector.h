/*
 * VRProjector.h
 *
 *  Created on: 26 oct. 2013
 *      Author: tguyet
 */

#ifndef VRPROJECTOR_H_
#define VRPROJECTOR_H_

#include <set>
#include <list>
#include <string>

#include "RasterProjector.h"
#include "ogrsf_frmts.h" //Pour les shapefiles

#include "FeatureStat.h"

using namespace std;

#define LOCAL_RTREE 1
#define GEOM_RTREE 2
#define BOOST_RTREE 3

#define RTREE GEOM_RTREE

#if RTREE == LOCAL_RTREE
#include "RTree.h"
#elif RTREE == GEOM_RTREE
#include <spatialIndex.h>
#include <geos/index/strtree/STRtree.h>
using geos::geom::Envelope;
using geos::index::strtree::STRtree;
#elif RTREE == BOOST_RTREE

#endif



class VRProjector : public RasterProjector {
public:
	struct callback_context {
		float x, y; //thequery
		FeatureStat* result;
	};
protected:
#if RTREE == LOCAL_RTREE
	RTree<FeatureStat*,float,2> tree; //R-tree to index features, contient toutes les géometries à traiter
#elif RTREE == GEOM_RTREE
	STRtree tree;
#elif RTREE == BOOST_RTREE
#endif
	OGRPolygon *covshape; // covshape est un double-pointeur servant à récupérer un polygone englobant tous les polygones

public:
	VRProjector();
	virtual ~VRProjector();

	virtual int project();

	/**
	 * Add a geometry into the RTree
	 */
	void addGeometry(OGRFeature *feature, int value =-1);
protected:
	void openshp(string filename);
};

#endif /* VRPROJECTOR_H_ */
