/*
 * CombinedProjector.cpp
 *
 *  Created on: 26 oct. 2013
 *      Author: tguyet
 */

#include "CombinedProjector.h"
#include <iostream>
#include "ogrsf_frmts.h"
#include <list>
#include <sstream>


using namespace std;

CombinedProjector::CombinedProjector(string rasterfilename, string vectorfilename, int bid, string att):RasterProjector(rasterfilename,vectorfilename, bid, att) {
	RVP.setDataset(_rfn, bandid);
	RVP.setVectorDataset(_vfn, cl_attribute);
	VRP.setDataset(_rfn,bandid);
	VRP.setVectorDataset(_vfn, cl_attribute);
	RVP.setCompacityThreshold(0.1);
	RVP.setRegionSizeLimit(100000);
}

CombinedProjector::~CombinedProjector() {
}

int CombinedProjector::project() {
	cout << "Project features of size <" << RVP.regionSizeLimit() << ", compacity min : "<< RVP.compacityThreshold()<<endl;
	RVP.project();

	int remaining=RVP.RemainingFeatures().size();
	cout << "#processed features : " << RVP.Stats().size()<< endl;
	cout << "#remaining features : " << remaining <<endl;

	//On recupère ce qu'on a déjà
	//!!Attention, on recopie des pointeurs pour les stats !!
	_stats.insert(_stats.end(), RVP.Stats().begin(), RVP.Stats().end());
	_values.insert(RVP.Values().begin(), RVP.Values().end());

	if(remaining) {
		//On ajoute les formes restantes aux RVProjector !
		cout << "Construct the R-Tree with remaining features" <<endl;
		list<OGRFeature *>::const_iterator it =RVP.RemainingFeatures().begin() ;
		int i=0;
		int nb=RVP.RemainingFeatures().size();
		while( it!= RVP.RemainingFeatures().end() ) {
			printf("%.2lf%%", 100*(float)i++/(float)(nb));
			fflush(stdout);
			printf("\r");
			VRP.addGeometry( (*it) );
			OGRFeature::DestroyFeature(*it);
			it++;
		}
		cout << "Project the remaining features" <<endl;
		VRP.project();
		_values.insert(VRP.Values().begin(), VRP.Values().end());
		//!!Attention, on recopie des pointeurs pour les stats !!
		list<FeatureStat*> lstats=VRP.Stats();
		_stats.insert(_stats.end(), lstats.begin(), lstats.end());
	}
	return 0;
}
