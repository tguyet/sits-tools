/*
 * RasterProjector.cpp
 *
 *  Created on: 26 oct. 2013
 *      Author: tguyet
 */

#include "RasterProjector.h"
#include "ogrsf_frmts.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>

using namespace std;


RasterProjector::RasterProjector():bandid(0) {

	poDataset = NULL;
	bandid=-1;
	poBand=NULL;
	_datatype=GDT_Unknown;
	fieldindex=-1;

	OGRSpatialReference oSourceSRS, oTargetSRS;
	//TODO rajouter des options !!
	oSourceSRS.importFromEPSG( 4326 );
	oTargetSRS.importFromEPSG( 32628 );
	poCT = OGRCreateCoordinateTransformation( &oSourceSRS, &oTargetSRS );
}


RasterProjector::RasterProjector(std::string rasterfilename, std::string vectorfilename, int bid, string att):_rfn(rasterfilename),_vfn(vectorfilename),bandid(bid),cl_attribute(att) {

	_datatype=GDT_Unknown;

	setDataset(_rfn, bid);

	OGRSpatialReference oSourceSRS, oTargetSRS;
	//TODO rajouter des options !!
	oSourceSRS.importFromEPSG( 4326 );
	oTargetSRS.importFromEPSG( 32628 );
	poCT = OGRCreateCoordinateTransformation( &oSourceSRS, &oTargetSRS );
}

void RasterProjector::clear() {
	_values.clear();
	list<FeatureStat*>::iterator it=_stats.begin();
	while( it!=_stats.end() ) {
		delete(*it);
		it++;
	}
	_stats.clear();
}

/**
 * \return 0 is succeeded, 1 otherwise
 */
int RasterProjector::setBandId(int id) {
	if( id<=0) {
		cerr << "RVProjector::setBandId error : band id must be a strictly positive integer" <<endl;
		return 1;
	}
	if (!poDataset) {
		cerr << "RVProjector::setBandId error : define the band before the band id " << id <<endl;
		return 1;
	}
	if( poDataset->GetRasterCount() < id) {
		cerr << "RVProjector::setBandId error : band id " << id << " does not exists (not enough band)"<<endl;
		return 1;
	}
	bandid=id;

	poBand = poDataset->GetRasterBand( bandid );
	if( poBand == NULL ) {
		return 1;
	}

	_datatype =  poBand->GetRasterDataType();
	if( _datatype!=GDT_Byte && _datatype!=GDT_Int32 ) {
		cerr << "mauvais type de bande : Byte ou Int32 attendus, reçu " << _datatype << " (see GDALDataType)."<< endl;
		return 1;
	}

	return 0;
}

void RasterProjector::setVectorDataset(string vectorfilename,string att) {
	_vfn=vectorfilename;
	cl_attribute=att;
}

/**
 * \return 0 is succeeded, 1 otherwise
 */
int RasterProjector::setDataset(string hdrfilename, int bid) {
	poDataset = (GDALDataset *) GDALOpen( hdrfilename.c_str(), GA_ReadOnly );
	if( poDataset == NULL ) {
		cerr << "RVProjector::setDataset error : impossible to open " << hdrfilename << endl;
		return 1;
	}
	if(CE_Failure==poDataset->GetGeoTransform(geotransform)){
		cerr << "no geotransform"<<endl;
	}

	if( bid ) {
		return setBandId(bid);
	} else {
		return 0;
	}
}

RasterProjector::~RasterProjector() {

}

int RasterProjector::write(string output ) {
	OGRDataSource *poSource;
	OGRSFDriver *poDriver = OGRSFDriverRegistrar::GetRegistrar()->GetDriverByName("ESRI Shapefile");
	if( poDriver == NULL ) {
		cerr << "FATAL ERROR : ESRI Shapefile driver not available.\n";
		return 1 ;
	}

	poSource = poDriver->CreateDataSource( output.c_str(), NULL );
	if( poSource == NULL ) {
		cerr << "Creation of "<< output <<" file failed.\n";
		return 1;
	}
	OGRLayer *newlayer;

	newlayer = poSource->CreateLayer( "output", NULL, wkbPolygon, NULL );
	if( newlayer == NULL ) {
		cerr << "FATAL ERROR : Layer creation failed.\n";
		return 1;
	}
	OGRFieldDefn oFieldC( "Class", OFTInteger );
	if( newlayer->CreateField( &oFieldC ) != OGRERR_NONE ) {
		cerr << "FATAL ERROR : Creating Name field failed.\n";
		return 1;
	}
	OGRFieldDefn oFieldNb( "NbPixel", OFTInteger );
	if( newlayer->CreateField( &oFieldNb ) != OGRERR_NONE ) {
		cerr << "FATAL ERROR : Creating Name field failed.\n";
		return 1;
	}
	set<int>::iterator itv = _values.begin();
	while( itv!=_values.end() ) {
		stringstream strs;
		strs << "Prop" << *itv;
		OGRFieldDefn oField( strs.str().c_str(), OFTReal );
		if( newlayer->CreateField( &oField ) != OGRERR_NONE ) {
			cerr << "FATAL ERROR : Creating "<< strs.str() <<" field failed.\n";
			return 1;
		}
		itv++;
	}

	list<FeatureStat*>::iterator it=_stats.begin();
	while( it!=_stats.end() ) {
		FeatureStat *fs = *it;
		//Création d'une nouvelle feature pour la couche de sortie
		OGRFeature *f;
		f = OGRFeature::CreateFeature( newlayer->GetLayerDefn() );
		int8_t argmax=0;
		float max=fs->stats[0];
		float nb=0; //nombre total de pixels
		map<int8_t,float>::iterator itm=fs->stats.begin();
		while(itm!=fs->stats.end()) {
			if( (*itm).second>max ) {
				max = (*itm).second;
				argmax = (*itm).first;
			}
			nb+=(*itm).second;
			itm++;
		}
		f->SetField( "Class", argmax);
		f->SetField( "NbPixel", (int)nb);

		set<int>::iterator itv = _values.begin();
		while( itv!=_values.end() ) {
			stringstream strs;
			strs << "Prop" << *itv;
			f->SetField( strs.str().c_str(), (float)fs->stats[*itv]/nb);
			itv++;
		}

		f->SetGeometryDirectly(fs->f); //!! Attribut la responsabilité de la géometrie !
		if( newlayer->CreateFeature( f ) != OGRERR_NONE ) {
			cerr << "FATAL ERROR : Failed to create feature in shapefile.\n";
			return 1;
		}
		OGRFeature::DestroyFeature( f );
		it++;
	}

	OGRDataSource::DestroyDataSource( poSource );

	return 0;
}

void RasterProjector::confusion(std::string output) {
	std::ofstream ofs;
	ofs.open(output.c_str(), std::ofstream::out);


	set<int>::iterator itv = _values.begin();
	int max=(*itv);
	while( itv!=_values.end() ) {
		if( *itv>max ) {
			max=*itv;
		}
		itv++;
	}

	map<int, vector<int> > confmat;
	itv = _values.begin();
	while( itv!=_values.end() ) {
		confmat[*itv] = vector<int>(FeatureStat::attributesmap.size(), 0);
		itv++;
	}
	//confmat[vraster][vshp]

	list<FeatureStat*>::iterator it=_stats.begin();
	while( it!=_stats.end() ) {
		FeatureStat *fs = *it;
		map<int8_t,float>::iterator jt=fs->stats.begin();
		while( jt!=fs->stats.end() ) {
			confmat[ (*jt).first ][fs->featureid]+=(*jt).second;
			jt++;
		}
		it++;
	}

	//construction de la ligne d'entête
	map<variant,int>::const_iterator itatt=FeatureStat::attributesmap.begin();
	while(itatt!=FeatureStat::attributesmap.end()) {
		if( (*itatt).first.type==2 ) {
			ofs << ","<<(*itatt).first.sval;
		} else {
			ofs << ","<<(*itatt).first.ival;
		}
		itatt++;
	}
	ofs<<endl;

	//construction du reste de la matrice
	map<int, vector<int> >::iterator itc = confmat.begin();
	while( itc!=confmat.end() ) {
		ofs << (*itc).first;
		vector<int>::iterator itcc=(*itc).second.begin();
		while(itcc!=(*itc).second.end()) {
			ofs <<","<< *itcc;
			itcc++;
		}
		ofs<<endl;
		itc++;
	}

	ofs.close();
}

