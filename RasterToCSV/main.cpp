/*
 * main.cpp
 *
 *  Created on: 8 avr. 2011
 *      Author: guyet
 */



#include "gdal_priv.h"
#include <list>
#include <assert.h>
#include <iostream>
#include <fstream>
#include "cpl_conv.h"
//#include "ogrsf_frmts.h"
#include <cstdlib>
#include <ctime>

int get_class(double x, double y) {

}


float **alloc_matrix(int nrows, int ncols) {
	int i;
	float **matrix=(float **)malloc( sizeof(float*)*nrows );
	if( matrix == NULL ) {
		fprintf(stderr, "alloc_matrix_float : matrix allocation error\n");
		return NULL;
	}

	for(i=0; i<nrows; i++) {
		matrix[i]=(float *)malloc( sizeof(float)*ncols );
		if(matrix[i]==NULL) {
			fprintf(stderr, "alloc_matrix_float : memory allocation error\n");
			return NULL;
		}
	}
	return matrix;
}

void free_matrix(float **mat, int nclusters)
{
	int i;
	for(i=0; i<nclusters; i++) {
		free(mat[i]);
	}
	free(mat);
}


int main(int argc, char **argv)
{
	char *hdrfilename="Kelkome";

	std::ofstream fout;
	fout.open("raster_kelkome.csv", std::ios::out);
	if( fout.bad() ) {
		return EXIT_FAILURE;
	}


	/////////// Chargement du fichier  ////////////////

	GDALDataset *poDataset;
	GDALRasterBand *poBand;
	float *pafScanline;
	float **data;
	std::list<int>::iterator ipit;

	GDALAllRegister();

	poDataset = (GDALDataset *) GDALOpen( hdrfilename, GA_ReadOnly );
	if( poDataset == NULL ) {
		std::cerr << "error while openning " << hdrfilename << std::endl;
		return EXIT_FAILURE;
	}

	int sx=poDataset->GetRasterXSize();
	int sy=poDataset->GetRasterYSize();
	int nbbands=poDataset->GetRasterCount();

	pafScanline = (float *) CPLMalloc(sizeof(float)* sx * sy);
	if(pafScanline==NULL) {
		std::cerr << "memory allocation error" << std::endl;
		return EXIT_FAILURE;
	}

	//Allocation de l'ensemble des données
	std::cout << "\tMemory allocation"<< std::endl;
	data = alloc_matrix(sx*sy, nbbands);

	std::cout << "\tStart reading"<<std::endl;

	for(int bandid=1; bandid<=nbbands; bandid++) {

		poBand = poDataset->GetRasterBand( bandid );

		if( poBand == NULL )
			continue;

		// ATTENTION : GDT_Float64 pour des float
		// 			mais GDT_Float32 pour des float !!

		poBand->RasterIO( GF_Read, 0, 0, sx, sy, pafScanline, sx, sy, GDT_Float32, 0, 0 );

		for(int i=0; i<sx*sy; i++ ) {
			data[i][bandid]=pafScanline[i];
		}

		std::cout << "\t\tband "<< bandid << " done" << std::endl;
	}

	std::cout << "\t\tFINISHED" << std::endl;
	//free(pafScanline);
	//GDALClose(poDataset);

	/////////// Enregistrement en format CSV  ////////////////
	bool alea = true;
	
	if(alea) {
		for(int i=0; i<sx*sy; i++ ) {
			for(int j=0; j<(nbbands-1); j++) {
				fout << data[i][j] << ",";
			}
			fout << data[i][nbbands-1] << std::endl;
		}
	} else {
		srand(time(NULL)); // initialisation de rand
		
		for(int i=0; i<nbrand; i++ ) {
			int pos=(int)( ( rand()/(double)RAND_MAX ) * sx*sy );
			for(int j=0; j<(nbbands-1); j++) {
				fout << data[pos][j] << ",";
			}
		}
	}

	fout.close();
	free_matrix(data, sx*sy);
	return EXIT_SUCCESS;
}
