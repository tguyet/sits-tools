/*
 * main.cpp
 *
 *  Created on: 8 avr. 2011
 *      Author: guyet
 */

#include "gdal_priv.h"
#include <list>
#include <assert.h>
#include <iostream>
#include <fstream>
#include "cpl_conv.h"
#include "ogrsf_frmts.h"
#include "ogr_spatialref.h"
#include <cstdlib>
#include <ctime>
#include <string>
#include <sstream>
#include <vector>
#include <map>

std::string get_class(OGRLayer *poLayer, OGRPoint point, std::string attribute, bool *found) {
	OGRFeature *poFeature;
	poLayer->ResetReading();
	OGREnvelope penv;
	point.getEnvelope(&penv);
	while( (poFeature = poLayer->GetNextFeature()) != NULL ) {
		OGREnvelope envelope;
		poFeature->GetGeometryRef()->getEnvelope(&envelope);
		if( envelope.Contains(penv) ) { //si l'envelope de la feature contient le point :
			if( poFeature->GetGeometryRef()->Contains(&point)) { //si la feature contient le point :
				std::string cl = poFeature->GetFieldAsString( attribute.c_str() );
				*found=true;
				OGRFeature::DestroyFeature( poFeature );
				return cl;
			}
		}
		OGRFeature::DestroyFeature( poFeature );
	}
	*found=false;
	return std::string("none");
}


float **alloc_matrix(int nrows, int ncols) {
	int i;
	float **matrix=(float **)malloc( sizeof(float*)*nrows );
	if( matrix == NULL ) {
		fprintf(stderr, "alloc_matrix_float : matrix allocation error\n");
		return NULL;
	}

	for(i=0; i<nrows; i++) {
		matrix[i]=(float *)malloc( sizeof(float)*ncols );
		if(matrix[i]==NULL) {
			fprintf(stderr, "alloc_matrix_float : memory allocation error\n");
			return NULL;
		}
	}
	return matrix;
}

void free_matrix(float **mat, int nclusters)
{
	int i;
	for(i=0; i<nclusters; i++) {
		free(mat[i]);
	}
	free(mat);
}

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

/**
 * Fonction pour récupérer un ensemble de points à partir d'un fichiers
 */
float ** getpositions(std::string filename, int *id) {
	int nrows = 0;
	std::string line;
	std::ifstream myfile(filename.c_str());

	//On compte le nombre de lignes
	while (std::getline(myfile, line)) ++nrows;
	myfile.clear();
	myfile.seekg(std::ios::beg); //rewind

	float **points=alloc_matrix(nrows, 2);
	*id=0;
	while( std::getline(myfile, line) ) {
		if( (*id)>=nrows ) break;
		std::vector<std::string> s=split(line, ',');
		points[*id][0]=std::atof( s[0].c_str() );
		points[*id][1]=std::atof( s[1].c_str() );
		++(*id);
	}

	return points;
}


void usage(const char *comm);

int main(int argc, char **argv)
{
	// Qqs variables
	GDALDataset *poDataset=NULL;
	GDALRasterBand *poBand=NULL;
	OGRDataSource *poDS=NULL;
	OGRLayer  *poLayer=NULL;
	float *pafScanline=NULL;
	float **data=NULL;
	std::list<int>::iterator ipit;
	double geotransform[6];
	double MinX, MinY;
	std::string shpfilename, hdrfilename, outputfilename="output.csv", pointfilename;
	std::string attributename= "USERLABEL";
	enum MODE_TYPES {MODE_ALL, MODE_ALEA, MODE_FILE};
	enum LABEL_TYPES {C_LABEL_ONLY, C_INT_ONLY, C_INTLABEL};
	int mode=MODE_ALEA;
	bool retirage = false;
	int nbrand = 1000;
	bool has_label=false;
	bool coordinates=false;
	int step=1;
	int epsgtarget=32628,epsgsource=32628; //default value WGS84 EPSG

	char sep=',';
	int ctype= C_INT_ONLY;

	if(argc<2) {
		printf("Erreur : donner une image à traiter\n");
	}

	for(int i=1;i<argc;i++)
	{
		if( !strcmp(argv[i],"-h") ) {
			usage(argv[0]);
			return EXIT_SUCCESS;
		} else if(!strcmp(argv[i],"-o")) {
			i++;
			outputfilename=argv[i];
		} else if(!strcmp(argv[i],"-p")) {
			i++;
			if( !pointfilename.empty() ) {
				fprintf(stderr, "Parameter error: -co and -p are incompatible options\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
			pointfilename=argv[i];
			mode= MODE_FILE;
		} else if(!strcmp(argv[i],"-n")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&nbrand) || nbrand<0 ) {
				fprintf(stderr, "Parameter error: Expected positive integer after -n\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
			if( nbrand==0 ) { //not random
				mode= MODE_ALL;
			} else {
				mode= MODE_ALEA;
			}
		} else if(!strcmp(argv[i],"-step")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&step) || step<0 ) {
				fprintf(stderr, "Parameter error: Expected positive integer after -step\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-ct")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&epsgsource) || epsgsource<0 ) {
				fprintf(stderr, "Parameter error: Expected 2 strictly positive integer after -ct\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&epsgtarget) || epsgtarget<0 ) {
				fprintf(stderr, "Parameter error: Expected 2 strictly positive integer after -ct\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-l")) {
			i++;
			attributename = argv[i];
			i++;
			shpfilename=argv[i];//"/home/tguyet/Recherche/Donnees/Senegal/Vegetation/sen_lc_dd_agg.shp";
			has_label=true;
		}  else if( !strcmp(argv[i],"-r") ) {
			retirage=true;
		}  else if( !strcmp(argv[i],"-co") ) {
			coordinates=true;
			i++;
			if( !pointfilename.empty() ) {
				fprintf(stderr, "Parameter error: -co and -p are incompatible options\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
			pointfilename=argv[i];
		}  else {
			//C'est un fichier à charger
			hdrfilename = argv[i];//"/home/tguyet/Recherche/Stages/2013/Cherif/Final/Prog+Expe+tools/Tools/python_tools/subndvi/2001_2006_ndvi.tif";
		}
	}

	if( hdrfilename.empty() ) {
		usage(argv[0]);
		return EXIT_FAILURE;
	}

	GDALAllRegister();
	OGRRegisterAll();

	std::ofstream fout;
	fout.open(outputfilename.c_str(), std::ios::out);
	if( fout.bad() ) {
		printf( "Failed to open file %s.\n", outputfilename.c_str() );
		return EXIT_FAILURE;
	}
	
	std::ofstream pout;
	if(coordinates) {
		pout.open(pointfilename.c_str(), std::ios::out);
		if( pout.bad() ) {
			printf( "Failed to open file %s.\n", pointfilename.c_str() );
			return EXIT_FAILURE;
		}
	}

	/////////// Chargement du fichier shapefile  //////////
	
	if(has_label) {
		//Ouverture du fichier en lecture
		poDS = OGRSFDriverRegistrar::Open( shpfilename.c_str(), FALSE );
		if( poDS == NULL ) {
			printf( "Failed to open file %s.\n", shpfilename.c_str() );
			return EXIT_FAILURE;
		}
		poLayer = poDS->GetLayer(0);
	}

	OGRSpatialReference oSourceSRS, oTargetSRS;
	oSourceSRS.importFromEPSG( epsgsource );
	oTargetSRS.importFromEPSG( epsgtarget );
	OGRCoordinateTransformation *poCT = OGRCreateCoordinateTransformation( &oSourceSRS, &oTargetSRS );

	/////////// Chargement du fichier raster  ////////////////

	poDataset = (GDALDataset *) GDALOpen( hdrfilename.c_str(), GA_ReadOnly );
	if( poDataset == NULL ) {
		std::cerr << "error while opening " << hdrfilename << std::endl;
		return EXIT_FAILURE;
	}

	int sx=poDataset->GetRasterXSize();
	int sy=poDataset->GetRasterYSize();
	int nbbands=poDataset->GetRasterCount();

	if( poDataset->GetGeoTransform( geotransform ) != CE_None ) {
		std::cerr << "Error : no geotransform"<<std::endl;
		return EXIT_FAILURE;
	}
	/*
	if( poDataset->GetProjectionRef() != NULL ) {
		printf( "Projection is `%s'\n", poDataset->GetProjectionRef() );
	}*/

	MinX = geotransform[0] + 0*geotransform[1] + sy*geotransform[2];
	MinY = geotransform[3] + 0*geotransform[4] + sy*geotransform[5];

	double xstep = geotransform[1];
	double ystep = geotransform[5];

	//Allocation pour une couche
	pafScanline = (float *) CPLMalloc(sizeof(float)* sx * sy);
	if(pafScanline==NULL) {
		std::cerr << "memory allocation error" << std::endl;
		return EXIT_FAILURE;
	}

	//Allocation de l'ensemble des données
	std::cout << "\tMemory allocation"<< std::endl;
	data = alloc_matrix(sx*sy, nbbands);

	std::cout << "\tStart reading" << std::endl;

	for(int bandid=1; bandid<=nbbands; bandid++) {
		poBand = poDataset->GetRasterBand( bandid );

		if( poBand == NULL )
			continue;

		// ATTENTION : GDT_Float64 pour des double mais GDT_Float32 pour des float !!
		poBand->RasterIO( GF_Read, 0, 0, sx, sy, pafScanline, sx, sy, GDT_Float32, 0, 0 );

		for(int i=0; i<sx*sy; i++ ) {
			data[i][bandid-1]=pafScanline[i];
		}
		std::cout << "\t\tband "<< bandid << " done" << std::endl;
	}

	std::cout << "\tReading finished" << std::endl;
	free(pafScanline);
	GDALClose(poDataset);

	/////////// Enregistrement en format CSV  ////////////////
	
	if(mode==MODE_ALL) { //tous les points du raster (sans label possible !)
		std::cout << "writing CSV for pixels" << std::endl;
		std::cout << "\tstep: "<<step<<std::endl;
		if(has_label) {
			double x, y;
			for(int i=0; i<sx; i+=step ) {
				x = MinX + i*xstep;
				for(int j=0; j<sy; j+=step) {
					int pos=j*sx+i;
					//Recuperation des coordonnées du point
					y = MinY - j*ystep;
					double ox=x,oy=y;
					if(poCT){
						//Transformation du point dans le referenciel de l'image shp
						poCT->Transform(1, &x, &y);
					}

					OGRPoint point(x, y);
					bool found;
					std::string label=get_class(poLayer, point, attributename, &found);
					if(coordinates) pout << ox << sep << oy << std::endl;
					for(int t=0; t<nbbands; t++) {
						fout << data[pos][t] << sep;
					}
					fout << label << std::endl;

					//barre de progression
					printf("%.2lf%%", 100*(float)((i+1)*(j+1))/(float)(sx*sy));
					fflush(stdout);
					printf("\r");
				}
			}
		} else {
			double x, y;
			for(int i=0; i<sx; i+=step ) {
				x = MinX + i*xstep;
				for(int j=0; j<sy; j+=step) {
					y = MinY - j*ystep;
					int pos = j*sx+i;
					if(coordinates) pout << x << sep << y << std::endl;
					for(int t=0; t<(nbbands-1); t++ ) {
						fout << data[pos][t] << sep;
					}
					fout << data[pos][nbbands-1] << std::endl;
				}
			}
		}
	} else if(mode==MODE_ALEA) { //Points générés aléatoirements
		std::cout << "writing CSV for " << nbrand << " random pixels" << std::endl;
		srand(time(NULL)); // initialisation de rand
		std::map<std::string, int> labelsid;
		for(int i=0; i<nbrand; i++ ) {
			int pos=(int)( ( rand()/(double)RAND_MAX ) * sx*sy );

			//Recuperation des coordonnées du point
			int lx=pos%sx, ly=pos/sx;
			double x, y;
			x = MinX + lx*xstep;
			y = MinY - ly*ystep;
			
			if(has_label) {
				double ox=x,oy=y;
				if(poCT){
					//Transformation du point dans le referenciel de l'image shp
					poCT->Transform(1, &x, &y);
				}

				OGRPoint point(x, y);
				bool found;
				std::string label=get_class(poLayer, point, attributename, &found);

				//ecriture dans le fichier, si on a trouve qqc
				if(!retirage || found) {
					if(coordinates) pout << ox << sep << oy << std::endl;

					//Recuperation de l'id pour le label
					std::map<std::string,int>::iterator sit;
					int id=0;
					if( (sit=labelsid.find(label))==labelsid.end()) {
						id = labelsid.size();
						labelsid[label] = id;
					} else {
						id = (*sit).second;
					}

					for(int j=0; j<nbbands; j++) {
						fout << data[pos][j] << sep;
					}

					//fout << id << std::endl;
					//fout << label << std::endl;
					if( ctype==C_LABEL_ONLY ) {
						fout << label;
					} else if( ctype==C_INT_ONLY ) {
						fout << id;
					} else {
						fout << label << sep <<id;
					}
					fout << std::endl;
				} else if (retirage) {
					i--;
				}
			} else {
				if(coordinates) pout << x << sep << y << std::endl;
				for(int j=0; j<nbbands; j++) {
					fout << data[pos][j];
					if(j==(nbbands-1))
						break;
					fout << sep;
				}
				fout << std::endl;
			}

			//barre de progression
			printf("%.2lf%%", 100*(float)(i+1)/(float)(nbrand));
			fflush(stdout);
			printf("\r");
		}

		std::cout << "==== label table ====" << std::endl;
		std::map<std::string,int>::iterator sit;
		for(sit=labelsid.begin(); sit!=labelsid.end(); sit++) {
			std::cout << (sit)->first <<": "<< (sit)->second <<std::endl;
		}
		std::cout << "=====================" << std::endl;
	} else if(mode==MODE_FILE) { //mode dans lequel est points sont donnés dans un fichier !
		int nbp=0;
		float **points=getpositions(pointfilename, &nbp);
		for(int i=0; i<nbp; i++ ) {
			double x = points[i][0], y=points[i][1];
			int lx = (x-MinX)/xstep;
			int ly = (y-MinY)/xstep;
			int pos = sx*ly+lx;

			if(has_label) {
				if(poCT){
					//Transformation du point dans le referentiel de l'image shp
					poCT->Transform(1, &x, &y);
				}

				OGRPoint point(x, y);
				bool found;
				std::string label=get_class(poLayer, point, attributename, &found);

				//ecriture dans le fichier
				for(int j=0; j<nbbands; j++) {
					fout << data[pos][j] << sep;
				}
				fout << label << std::endl;
			} else {
				for(int j=0; j<nbbands; j++) {
					fout << data[pos][j];
					if(j==(nbbands-1))
						break;
					fout << sep;
				}
				fout << std::endl;
			}

			//barre de progression
			printf("%.2lf%%", 100*(float)(i+1)/(float)(nbp));
			fflush(stdout);
			printf("\r");
		}
	} else {
		printf("Error: unknown mode file ...");
	}


	fout.close();
	if(coordinates) pout.close();
	free_matrix(data, sx*sy);
	return EXIT_SUCCESS;
}


/**
* \brief Fonction d'information sur l'usage du programme
*/
void usage(const char *comm)
{
  printf("*******************\n");
  printf("Satellite images time series analysis CSV extractor\n\n");
  printf("Brief : ...\n");
  printf("Author : Guyet Thomas, AGROCAMPUS-OUEST\n");
  printf("Year : 2013\n");
  printf("*******************\n\n");
  printf("command : %s [-n %%d] [-p %%s] [-l %%s %%s] [-o %%s] [-co %%s] [-step %%d] [-r] [-ct %%d %%d] datafile\n\n", comm);
  printf("Options :\n");
  printf("\t -h : show this help \n");
  printf("\t -o file : output file name (default: output.csv) \n");
  printf("\t -l attribute file : shapefile with labels given in attribute name, both attribute and filename have to be provided\n");
  printf("\t -n %%d : number of random points to extract, all pixels if 0 (default: 1000)\n");
  printf("\t -r : force to have the exact number of random points with labels\n");
  printf("\t -p file : use the position in the file (1 coordinate/line, comma separated, same SRS as the datafile one)\n");
  printf("\t -co file: generate the coordinate of the pixels in the file\n");
  printf("\t -step %%d: sampling step (in x and y)\n");
  printf("\t -ct %%d %%d: coordinate transform from the raster file to the shapefile. Require two EPSG numbers (integers) (no transformation by default).\n");
  printf("\t datafile : raster datafile name (main data file)\n");
  printf("\n *** WARNING: SRS are hard coded !!! ***\n");
}
