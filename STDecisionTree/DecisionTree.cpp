/*
 * DecisionTree.cpp
 *
 *  Created on: 11 août 2014
 *      Author: tguyet
 */

#include "DecisionTree.h"
#include <cassert>
#include <utility>
#include <limits>
#include <iomanip>
#include <algorithm>
#include <cstring>
#include <iostream>

#ifdef VPARA
#include <future>
#endif


using namespace std;

DecisionTreeNode::DecisionTreeNode(DecisionTree &t, DecisionTreeNode *_p):_parent(_p),_tree(t) {
	_I={0,_tree.ds().length()};
}

DecisionTreeNode::DecisionTreeNode(DecisionTree &t, const std::list<unsigned int> &ts, DecisionTreeNode *_p):_tsset(ts), _parent(_p), _tree(t) {
	_I={0,_tree.ds().length()};
}

/**
 * La destruction d'un noeud entraine la description de tout le sous-arbre.
 */
DecisionTreeNode::~DecisionTreeNode() {
	if(_l) delete(_l);
	if(_r) delete(_r);
}

void DecisionTreeNode::computeMajClass() {
	_majclass=0;
	_relclasspourcentage=0.0;
	_classpourcentage=0.0;
	double *f = new double[_tree.ds().nc()];
	memset ( f, 0, _tree.ds().nc()*sizeof(double) );
	for(auto i : _tsset) {
		f[_tree.ds().Sequences()[i].cl()]++;
		if( f[_tree.ds().Sequences()[i].cl()]>f[_majclass] ) {
			_majclass=_tree.ds().Sequences()[i].cl();
		}
	}
	_relclasspourcentage = f[_majclass]/(double)_tsset.size();
	_classpourcentage = f[_majclass]/(double)_tree.ds().nobs();
	delete[] f;
}

void DecisionTreeNode::print(unsigned int level) const {
	for(unsigned int i=0; i<level; i++) cout <<" ";
	cout << _TS;
#if DO_DIM_EXPLO
	if( var>0 ) {
		cout << " d"<<var<< " - " ;
	} else {
		cout << " d-all - " ;
	}
#endif
	cout << "[" << _I.l << "," << _I.r << "](" << _tsset.size() << ")";
	if(_l==nullptr) {
		cout << "(" << _tsset.size() << ", " << setprecision(2) << _classpourcentage << ", "  << setprecision(2) << _relclasspourcentage<< " ) => " << _majclass;
	}
	cout  << endl;
	if( _l ) _l->print(level+1);
	if( _r ) _r->print(level+1);
}

double DecisionTreeNode::gini(const split &s) const {
	double GiniL = 0, GiniR=0;

	double *f = new double[_tree.ds().nc()];
	memset ( f, 0, _tree.ds().nc()*sizeof(double) );
	for(auto i : s.tsl) {
		f[_tree.ds().Sequences()[i].cl()]++;
	}
	for(unsigned int k=0; k<_tree.ds().nc();k++) {
		GiniL += f[k]*f[k];
		f[k]=0; //on remet à 0 pour le calcul suivant !
	}
	GiniL=1-GiniL/(double)(s.tsl.size()*s.tsl.size());
	//cout << "GiniL:" << GiniL << "," << s.tsl.size() <<endl;

	for(auto i : s.tsr) {
		f[_tree.ds().Sequences()[i].cl()]++;
	}
	for(unsigned int k=0; k<_tree.ds().nc();k++) {
		GiniR += f[k]*f[k];
	}
	GiniR=1-GiniR/(double)(s.tsr.size()*s.tsr.size());
	//cout << "GiniR:" << GiniR << "," << s.tsr.size() << endl;

	delete[] f;
	return GiniL*s.tsl.size() + GiniR*s.tsr.size();
}

double DecisionTreeNode::informationgain(const split &ss) const {
	double ig = entropy(_tsset) - (ss.tsl.size()*entropy(ss.tsl) + ss.tsr.size()*entropy(ss.tsr))/(double)(_tsset.size());
	return ig;
}

double DecisionTreeNode::entropy(const std::list<unsigned int> &L) const {
	double d=0;
	double *f = new double[_tree.ds().nc()];
	memset ( f, 0, _tree.ds().nc()*sizeof(double) );
	for(auto i : L) {
		f[_tree.ds().Sequences()[i].cl()]++;
	}
	for(unsigned int i=0; i<_tree.ds().nc(); i++) {
		if(f[i]>0) {
			double pi=f[i]/(double)L.size();
			d+= pi*log(pi);
		}
	}
	delete[] f;
	return -d;
}

void DecisionTreeNode::TSSplit(const Interval I) {
	//cout << "TSSplit" <<endl;
#ifdef VPARA
	std::future<void> task1, task2;
#endif
	split ss=AdaptSplit(I); //! copy of ss : may be improved by moving !
	ss = DichoSplit(ss, _tree.alpha());
	_l = new DecisionTreeNode(_tree, this);
	_l->_tsset = move( ss.tsl );
	_l->_TS = ss.l;
	_l->_I = ss.I;
	_l->var= ss.var;
	_l->computeMajClass();
	//si _l->_relclasspourcentage==1 : il n'y a rien à décomposer !
	if( _l->_relclasspourcentage!=1 && _l->_tsset.size()> _tree.mininal_node_number ) {
#ifdef VPARA
		task1=std::async(launch::async, &DecisionTreeNode::TSSplit, _l, I);
#else
		_l->TSSplit(I);
#endif
	}
	_r = new DecisionTreeNode(_tree, this);
	_r->_TS = ss.r;
	_r->_tsset = move( ss.tsr );
	_r->_I = ss.I;
	_r->var= ss.var;
	_r->computeMajClass();
	if( _r->_relclasspourcentage!=1 && _r->_tsset.size()> _tree.mininal_node_number ) {
#ifdef VPARA
		task2=std::async(launch::async, &DecisionTreeNode::TSSplit, _r, I);
#else
		_r->TSSplit(I);
#endif
	}

#ifdef VPARA
	//wait for results
	if( task1.valid() ) task1.wait();
	if( task2.valid() ) task2.wait();
#endif
}

DecisionTreeNode::split DecisionTreeNode::DichoSplit(const split &ss, double alpha) const {
	//cout << "DichoSplit(" << _TS << "," << ss.I.l << "," << ss.I.r << ") <<"<< endl;
	if( alpha*(ss.I.r-ss.I.l)<_tree._minimal_interval_length ) return ss;
	if( ss.e==0 ) return ss;

	Interval Il, Ir;
	Il.l=ss.I.l;
	Il.r=ss.I.l+alpha*(ss.I.r-ss.I.l);
	Ir.l=ss.I.r-alpha*(ss.I.r-ss.I.l);
	Ir.r=ss.I.r;

#ifdef VPARA
	future<split> task_l, task_r;
	task_l = async(launch::async, &DecisionTreeNode::AdaptSplit, this, Il);
	task_r = async(launch::async, &DecisionTreeNode::AdaptSplit, this, Ir);
	split ssl=task_l.get();
	split ssr=task_r.get();
#else
	split ssl=AdaptSplit(Il); //HACK copy ssl and ssr!
	split ssr=AdaptSplit(Ir);
#endif

	//cout << "DichoSplit(" << _TS << "," << ss.I.l << "," << ss.I.r << "): ss:" << ss.e << ", ssl:" << ssl.e << ", ssr:" << ssr.e << "!" << endl;

	if ( ss.e < min(ssl.e ,ssr.e) ) {
		//_tree.addNbClassified(_tsset.size(), 3);
		return ss;
	} else if(ssl.e < ssr.e) {
		//ssl est mieux a priori, mais il faut regarder les contraintes structurelles de l'arbre !
		if( ( ssl.tsl.size()<_tree.mininal_node_number || ssl.tsr.size()<_tree.mininal_node_number ) && ssr.tsl.size()>=_tree.mininal_node_number && ssr.tsr.size()>=_tree.mininal_node_number ) {
			//à cause de la contrainte des tailles de noeud, ssl ne peut être utilisé
			return DichoSplit(ssr, alpha);
		} else {
			return DichoSplit(ssl, alpha);
		}
	} else {
		// ssr est a priori le mieux, mais il faut regarder les contraintes structurelles de l'arbre !
		if( (ssr.tsr.size()<_tree.mininal_node_number || ssr.tsr.size()<_tree.mininal_node_number) && ssl.tsl.size()>=_tree.mininal_node_number && ssl.tsr.size()>=_tree.mininal_node_number  ) {
			return DichoSplit(ssl, alpha);
		} else {
			return DichoSplit(ssr, alpha);
		}
	}
}


DecisionTreeNode::split DecisionTreeNode::AdaptSplit(const Interval I) const {
	//cout << "\tAdaptSplit(" << _TS << "," << I.l << "," << I.r << ") <<"<< endl;

	split bestsplit;
	bestsplit.e=std::numeric_limits<double>::max();
#if DO_DIM_EXPLO
	for(int v=(_tree._ds.dim()>1?-1:0); v<(int)_tree._ds.dim();v++) {
#else
	unsigned int v=0;
#endif
		for(list<unsigned int>::const_iterator it=_tsset.begin(); it!=_tsset.end(); it++) {
			list<unsigned int>::const_iterator jt=it;
			jt++;
			for(; jt!=_tsset.end(); jt++) {
				split s;
				s.l=*it;
				s.r=*jt;
				s.var=v;
				for(auto k : _tsset) {
					double dit=_tree.ds().dist(*it, k, I, v);
					double djt=_tree.ds().dist(*jt, k, I, v);
					if( dit < djt ) {
						s.tsl.push_back(k);
					} else {
						s.tsr.push_back(k);
					}
				}
				//cout << "\tgini:" << s.tsl.size() << " vs " << s.tsr.size() << endl;
				if(s.tsl.size()==0 || s.tsr.size()==0) continue;

				//Compute the GiniIndex for the split
				if( _tree._Gini ) {
					s.e=gini(s);
				} else {
					s.e=1-informationgain(s); //on utilise un '1-' parce qu'on minimise ensuite !
				}

				//cout << "\tgini:" << s.e << " vs " << bestsplit.e<<  endl;
				if(s.e < bestsplit.e ) {
					bestsplit = move( s );
					if(bestsplit.e==0) {
						//can not do better, we cut the research
						bestsplit.var = var;
						bestsplit.I=I;
						return bestsplit;
					}
				}
			}
		}
#if DO_DIM_EXPLO
	}
#endif
	//cout << "\tAdaptSplit(" << _TS << "," << I.l << "," << I.r << ") >> bestsplit:" << bestsplit.e <<endl;
	bestsplit.I=I;
	return bestsplit;
}

unsigned int DecisionTreeNode::predict(const TimeSeries &S) const {
	//cout << "Predict - Node(" << _TS << "," << _I.l << "," << _I.r << ") <<"<< endl;
	if( _l==nullptr && _r==nullptr ) {
		return _majclass;
	}
	double dl=_tree.ds().Sequences()[_l->_TS].cmp(S, _l->_I, _l->var);
	double dr=_tree.ds().Sequences()[_r->_TS].cmp(S, _r->_I, _r->var);
	//cout << "\tl(" << _l->_TS <<"): " << dl << ", r(" << _r->_TS <<"): " << dr <<endl;
	if( dl<dr ) {
		return _l->predict(S);
	} else {
		return _r->predict(S);
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////

DecisionTree::DecisionTree(Dataset &d):_root(nullptr), _ds(d) {}
DecisionTree::~DecisionTree() {
	if(_root) delete(_root);
}

void DecisionTree::buildTree() {
	mininal_node_number=(mininal_node_number<2?2:mininal_node_number);
	cout << "learning sequences:" << _ds.learn().size() << endl;
	if(_root) delete(_root);
	_root = new DecisionTreeNode(*this,_ds.learn());
	Interval I={0, _ds.length()};
	_root->TSSplit(I);
}

void DecisionTree::print() const {
	if(_root) {
		_root->print(0);
	}
}

unsigned int DecisionTree::predict(const TimeSeries &S) const {
	//cout << "===== Predict =====" << endl;
	unsigned int cl=_root->predict(S);
	//cout << "real:" << S.cl() <<", predicted: " << cl <<endl;
	return cl;
}
