/*
 * dataset.h
 *
 *  Created on: 8 août 2014
 *      Author: tguyet
 */

#ifndef DATASET_H_
#define DATASET_H_

#include <vector>
#include <string>
#include <map>
#include <unordered_map>
#include <functional>
#include <mutex>
#include <list>

class TimeSeries;

/**
 * Structure de données représentant un intervalle
 */
typedef struct {
	unsigned int l,r;
} Interval;

/**
 * Structure de données utilisée comme clé pour l'accès au
 * cache des distances.
 * Ici, on accède aux distances à l'aide des indices des séries temporelles et de l'intervalle
 * sur lequel est calculé la distance.
 */
typedef struct {
	unsigned int S1;
	unsigned int S2;
	int d; // variable to use (if -1), multi-dimensionnal distance
	unsigned int Il,Ir;
	//
} subspace;

/**
 * Définition de l'égalité entre subspace
 */
inline bool operator==(subspace const& p1, subspace const& p2);

/**
 * définition d'une fonction (extraite de boost) pour
 * la combinaison d'attributs de hashage
 */
template <class T>
inline void hash_combine(std::size_t & seed, const T & v)
{
  std::hash<T> hasher;
  seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
};

//definition d'une fonction de hashage
namespace std {
template<> struct hash<subspace> {
	std::size_t operator()(subspace const& p) const {
        std::size_t seed = 0;
        hash_combine(seed, p.S1);
        hash_combine(seed, p.S2);
        hash_combine(seed, p.d);
        hash_combine(seed, p.Il);
        hash_combine(seed, p.Ir);
        return seed;
	}
};
}

class DecisionTree;

/**
 * \class Dataset
 * \brief Classe de gestion d'un jeu de données de séries temporelles
 *
 * Toutes les séries temporelles sont de même longueur et même dimension
 * (pour toutes les dimensions, les longueurs sont toutes les mêmes également !)
 *
 * La classe offre principalement les fonctionnalités suivantes:
 * <ul>
 * <li> lecture d'un jeu de données
 * <li> gestion des distances entre pair de séries temporelles
 * <li> generation d'un ensemble de test et d'évaluation
 * </ul>
 *
 * La classe gère les séries temporelles au moyen de TSId (Time Series identifier).
 * Pour des raisons d'efficacité, les séries temporelles chargées ne sont pas copiables (\see TimeSeries::TimeSeries(const TimesSeries&))
 */
class Dataset {
	std::vector<TimeSeries> _sequences; //!< Jeu de données de séries temporelles
	std::map<std::string,unsigned int> _classes; //!< liste des classes avec leurs descriptifs textuels
	unsigned int _length, 	//!< longueur des séries temporelles (nombre de points)
				_dim;		//!< dimension des séries temporelles (nombre de séries temporelles/exemples)
	std::list<unsigned int> _learn, //!< liste des TSId utilisées pour l'apprentissage de l'arbre
							_test;	//!< liste des TSid utilisées pour le test

	/**
	 * Structure de données servant de cache pour les distances entre séries temporelles
	 * Les distances sont indexés selon les attributs de subspace (dépend notamment des limites d'un intervalle)
	 * On utilise une hash_map pour les propriétés de sparcity de cette structure de données.
	 */
	std::unordered_map<subspace, double> distances_cache;

	std::mutex cachemtx; //< mutex servant à protéger l'accès concurrent au cache de distance

	friend TimeSeries; //< on déclare la classe Sequence comme friend pour faciliter l'accès aux membres protected de la classe


	//double (TimeSeries::*d)(const TimeSeries &S, Interval I) const; //< pointeur de fonction sur la distance à utiliser

public:
	Dataset(); //< Constructeur

	virtual ~Dataset(){}; //<Destructeur

	/**
	 * Chargement d'un jeu de données à partir d'un fichier CSV.
	 */
	void read(std::string filename);

	/**
	 * Fonction de calcul d'une distance entre séries temporelles
	 * \param S1, S2 TSId de deux séries temporelles
	 * \param I intervalle sur lequel calculer la distance
	 * \param var variable sur laquelle calculer la distance (distance multi-dimensionnelle si -1)
	 */
	double dist(int S1, int S2, Interval I, int var);

	/**
	 * Fonction d'évaluation d'un arbre de décision T sur le jeu de test interne
	 * \param T: Arbre de décision
	 * \return Un vecteur contenant, pour chaque classe du jeu de données, le nombre d'exemples bien
	 * classés (first) et le nombre d'exemples mals classés (second).
	 */
	std::vector< std::pair<unsigned int, unsigned int> > evaluate(const DecisionTree &T) const;

	/**
	 * Fonction de génération aléatoire d'une partition du jeu de données en jeu de test et d'apprentissage.
	 * \param n donne le nombre d'exemples dans le jeu d'apprentissage
	 *
	 * Le jeu de données est partitionné en deux.
	 */
	void cv_gen(unsigned int n);

	//Getters/Setters

	unsigned int nobs() const {return _sequences.size();}; // nombre d'observations
	unsigned int nc() const {return _classes.size();}; // nombre de classes
	unsigned int length() const {return _length;}; // nombre de points dans une séquence du jeu d'exemple
	unsigned int dim() const {return _dim;}; // nombre de variable (ie nombre de dimension pour les séries temporelles multivariees)

	void setDim(unsigned int d){_dim=d;};
	void setLength(unsigned int l){_length=l;};

	const std::vector<TimeSeries>& Sequences() const {return _sequences;};

	const std::list<unsigned int>& learn() const {return _learn;};
	const std::list<unsigned int>& test() const {return _test;};
};

/**
 * Classe de représentation d'une série temporelle multivariée (STM)
 *
 * Une STM est décrite par des séries temporelles univariées qui sont toutes de même longueur.
 * La dimension et la longueur d'une série temporelle sont définies au niveau du jeu de données.
 */
class TimeSeries {
	double *_X; //< tableau de taille _length*_dim,
				// _X[i+j*_length] designe la valeur à la position i de la dimension j !!
	Dataset &_ds;	//< reference au jeu de données auquel appartient la série temporelle
	unsigned int _cl; 			//< classe d'appartenance d'une série temporelle dans le jeu d'exemple
	double *_scaling =nullptr;	//< vecteur de dimension _ds._dim contenant le facteur de normalisation des séries temporelles
	double *_decay =nullptr;	//< vecteur de dimension _ds._dim contenant le décalage de la normalisation des séries temporelles
public:
	TimeSeries(Dataset &ds); //< constructor
	
	/**
	 * Copy constructor DELETED
	 * avoid the sequence copies: force the use of moving operators !
	 */
	TimeSeries(const TimeSeries &seq) =delete;
	
	/**
	 * Move constructor
	 * Utilisé de préférence pour la manipulation des séries temporelles
	 * L'utilisation de la sémantique des move permet de manipuler les séries temporelles
	 * en évitant d'avoir à faire des recopies des données internes (uniquement des déplacements des données)
	 */
	TimeSeries(TimeSeries&& seq);

	virtual ~TimeSeries(); //< destructeur (détruit les données)

	/**
	 * \brief construct a sequence from a string
	 * \see Dataset::read(string filename)
	 */
	int read(std::string str);

	/**
	 * Fonction de normalisation d'une série temporelle multivariée
	 */
	void normalize();

	/**
	 * Fonction de calcul de la distance entre deux séries temporelles.
	 * \param T time series to compare to
	 * \param I temporal interval on which compute the distance
	 * \param var variable on which compute unidimensional timeseries distances (-1 if multi-dimensional)
	 *
	 * Cette fonction est utilisée à fois lors de la construction de l'arbre et
	 * de la prédiction. Elle encapsule le choix de la distance à utiliser pour garantir la cohérence
	 * dans l'utilisation des distances entre ces deux phases !
	 *
	 * Il s'agit simplement d'un appel à une autre distance définie pour les séries
	 *
	 * HACK: definir ici la distance à utiliser
	 */
	inline double cmp(const TimeSeries &T, Interval I, int var) const {
		if(var<0) {
			return this->deuclid(T,I);
		} else {
			return this->deuclid(T, var, I);
		}
	};

	// Distances et dissimilarités
	// -> éviter de les utiliser directement: utiliser plutôt cmp qui
	//  définie une distance identifque pour toutes les étapes !)

	double deuclid(const TimeSeries &s, Interval I) const;
	double deuclid(const TimeSeries &s, unsigned int var, Interval I) const;
	double dunif(const TimeSeries &s, Interval I) const;
	double dtw(const TimeSeries &s, Interval I) const;
	double dtw(const TimeSeries &s, unsigned int var, Interval I) const;
	double mddtw(const TimeSeries &s, Interval I) const;

	double cort(const TimeSeries &s, Interval I) const;
	double corr(const TimeSeries &s, Interval I) const;

	double dissimilarity(const TimeSeries &s, Interval I, unsigned int k, bool usedtw, bool usecorr) const;

	//Getters et setters

	double *data() const {return _X;};
	unsigned int dim() const {return _ds.dim();};
	unsigned int length() const {return _ds.length();};
	unsigned int cl() const {return _cl;};
};

#endif /* DATASET_H_ */
