/*
 * DecisionTree.h
 *
 *  Created on: 11 août 2014
 *      Author: tguyet
 */

#ifndef DECISIONTREE_H_
#define DECISIONTREE_H_

#include <list>
#include "dataset.h"

#define DO_DIM_EXPLO 1

class DecisionTree;

/**
 * Noeud d'un arbre de décision
 *
 * Each node is defined by (see spbspace):
 * 	- a TSId which is the time series representing the set of time series _tsset (most central time series)
 * 	- an interval on which the distance are computed
 *
 * TODO define a new type for time series id and classes
 * TODO add some other quality functions
 */
class DecisionTreeNode {
	unsigned int _TS=0; //<! id of the time series that represent the node
	Interval _I;		//<! Interval on which the distance have been computed to cluster the time series of _tsset
	int var=-1; 		//<! variable to compute unidimensional timeseries distance, multi-dimensional distance if -1

	std::list<unsigned int> _tsset; //< list of time series identifier associated to the current node

	DecisionTreeNode *_l=nullptr, *_r=nullptr; //< children nodes
	DecisionTreeNode *_parent=nullptr;	//< parent node
	DecisionTree &_tree; //< reference to the tree

	unsigned int _majclass =0; //< majority class of the set of time series _tsset (predicted class)
	double _classpourcentage =0, _relclasspourcentage=0.0; //< some statistics

	/**
	 * Definition de la structure d'un split en cours d'évaluation
	 */
	typedef struct {
		unsigned int r,l; //id of the time series
		Interval I;
		std::list<unsigned int> tsl;
		std::list<unsigned int> tsr;
		double e; 	//< quantity to minimize (Gini index or Information Gain)
		int var; 	//< spliting variable
	} split;

public:
	DecisionTreeNode(DecisionTree &, DecisionTreeNode *_p=nullptr);
	DecisionTreeNode(DecisionTree &, const std::list<unsigned int> &ts, DecisionTreeNode *_p=nullptr);
	virtual ~DecisionTreeNode();

	/**
	 * Spliting function used to construct the tree.
	 * The function split the set _tsset into two subsets of time series (left and right child nodes).
	 */
	void TSSplit(const Interval I);

	/**
	 * printing the node at level l
	 */
	void print(unsigned int l) const;

	/**
	 * Function that give the most probable class according to the classification sub-tree from which the
	 * current node is the root.
	 */
	unsigned int predict(const TimeSeries &S) const;

protected:
	/**
	 * Function that compute the majority class of the current node.
	 */
	void computeMajClass();

	/* Fonction qui propose la meilleure décomposition en 2 noeuds
	 * \return the Gini Index of the new decomposition
	 */
	split AdaptSplit(const Interval I) const;
	split DichoSplit(const split &ss, double alpha) const;

	/**
	 * The function compute a Gini Index.
	 * The index inform about the quality of a split of _tsset
	 */
	double gini(const split &s) const;
	double informationgain(const split &s) const;
	double entropy(const std::list<unsigned int> &) const;
};


/**
 * Arbre de décision de séries temporelles s'appuyant sur des distances calculées par la
 * fonction Dataset::dist
 *
 * La classe offre les deux fonctionnalités usuelles d'un arbre de décision:
 * 		- la construction d'un arbre (apprentissage supervisé): buildTree()
 * 		- la prédiction de la classe d'appartenance d'une séries temporelles à partir d'un arbre: predict()
 *
 * Les paramètres influencant la construction de l'arbre sont (en plus du choix de la distance):
 * 		- _alpha et _minimal_interval_length pour les intervalles qui peuvent être explorés
 * 		- mininal_node_number pour la structure de l'arbre (limite le sur-apprentissage en l'état)
 */
class DecisionTree {
	DecisionTreeNode *_root; //< root of the tree
	Dataset &_ds; //< reference to the dataset that is used as a cache of distances between time series

	double _alpha =0.6; //plus alpha est proche de 1 et plus on laisse la possibilite d'overlap des sous-intervalles
	unsigned int _minimal_interval_length = 3; //limite minimal de taille d'un intervalle
	unsigned int mininal_node_number=20; //mettre a 0 pour supprimer la contrainte !! !! PB SI 0 !!
	bool _Gini = true;	//use GiniIndex (use Information Gain otherwise)

	friend DecisionTreeNode;
public:
	DecisionTree(Dataset &);
	virtual ~DecisionTree();

	/**
	 * Fonction de construction de l'arbre à partir du jeu de données du dataset reférencé
	 */
	void buildTree();

	/**
	 * Fonction de prédiction de la classe de la série temporelle S
	 * \param S série temporelle dont on cherche
	 * \return Retourne le numéro de la classe le plus probable selon l'arbre de décision
	 *
	 * S peut, ou non, appartenir au jeu de données de l'arbre de décision, mais elle doit impérativement avoir
	 * la même structure (même longueur et même dimension).
	 * Les classes prédites font nécessairement référence aux classes du jeu de données ayant servi à l'appprentissage.
	 */
	unsigned int predict(const TimeSeries &S) const;

	/**
	 * Fonction d'affichage de l'arbre
	 */
	void print() const;

	//Getters/Setters
	void useIG() {_Gini=false;};
	void useGini() {_Gini=true;};
	double alpha() const {return _alpha;};
	void set_alpha(double d) {_alpha=d;};
	void set_min_node_size(unsigned int i) {mininal_node_number=i;};
	Dataset &ds(){return _ds;};
};

#endif /* DECISIONTREE_H_ */
