/*
 * dataset.c
 *
 *  Created on: 8 août 2014
 *      Author: tguyet
 */

#include "dataset.h"
#include "DecisionTree.h"
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <utility>
#include <cassert>
#include <cmath>
#include <ctime>
#include <algorithm>
#include <cstring>

using namespace std;

Dataset::Dataset():_length(0),_dim(1){};


/**
 * empty the _TimeSeriess and _classes
 */
void Dataset::read(string filename) {
	if (_dim==0 || _length==0 ) {
	    std::cerr << "Error Dataset::read: undefined dim or length of the dataset\n";
	    return;
	}
	ifstream file ( filename );
	if ( (file.rdstate() & std::ifstream::failbit ) != 0 ) {
	    std::cerr << "Error Dataset::read: impossible to open "<< filename << "\n";
	    return;
	}

	_sequences.clear();
	_classes.clear();

	string str;
	getline(file, str);
	while ( file.good() && !file.eof() ) {
		TimeSeries seq(*this);
		if(!seq.read(str)) {
			//seq.normalize();
			_sequences.push_back( std::move(seq) ); //using the move avoid the copy (pure C++11)!
		}
		getline(file, str);
	}
	file.close();

	_learn.clear();
	_test.clear();
	for(unsigned int i=0; i<nobs();i++) {
		_learn.push_back(i);
	}
}

bool operator==(subspace const& p1, subspace const& p2) {
    return p1.S1 == p2.S1 &&  p1.S2 == p2.S2 && p1.d == p2.d && p1.Il == p2.Il &&  p1.Ir == p2.Ir;
};

/**
 * Efficient time-serie management
 *
 */
double Dataset::dist(int S1, int S2, Interval I, int var) {
	//1) get see the cache
	subspace ss;
	ss.S1 = min(S1,S2);
	ss.S2 = max(S1,S2);
	ss.Il=I.l;
	ss.Ir=I.r;
	ss.d=var;
	cachemtx.lock();
	unordered_map<subspace, double>::iterator it= distances_cache.find(ss);
	if( it != distances_cache.end() ) {
		double val=(*it).second;
		cachemtx.unlock();
		return val;
	} else {
		//		2) compute if not in the cache
		Interval I={ss.Il, ss.Ir};
		double val=_sequences[S1].cmp(_sequences[S2], I, var);

		//		3) add to the cache
		//distances_cache.insert( std::pair<subspace,double>(ss,d) );
		distances_cache[ss]= val;
		cachemtx.unlock();
		return val;
	}
	return 0.0;
}

/**
 * generate n random id of sequence
 */
void Dataset::cv_gen(unsigned int n){
	std::srand ( unsigned ( std::time(0) ) );
	unsigned int i;
	std::vector<unsigned int> val;
	for(i=0; i<_sequences.size(); ++i) val.push_back(i);
	std::random_shuffle( val.begin(), val.end() );
	_learn.clear();
	_test.clear();
	for(i=0; i<n; ++i) {
		_learn.push_back( val[i] );
	}
	for(; i<_sequences.size(); ++i) {
		_test.push_back( val[i] );
	}
}


vector< pair<unsigned int, unsigned int> > Dataset::evaluate(const DecisionTree &T) const {
	cout << "#test:" << _test.size() << endl;
	vector< pair<unsigned int, unsigned int> > eval(nc());
	unsigned int V=0, F=0;
	vector< vector<unsigned int> > contingencymatrix(nc());

	for(unsigned int i=0;i<nc();i++) {
		contingencymatrix[i]=vector<unsigned int>(nc());
		for(unsigned int j=0;j<nc();j++) {
			contingencymatrix[i][j]=0;
		}
	}
	for(unsigned int i : _test) {
		unsigned int pcl=T.predict( _sequences[i] );
		contingencymatrix[_sequences[i].cl()][pcl]++;
		if( pcl==_sequences[i].cl() ) {
			V++;
			eval[_sequences[i].cl()].first++;
		} else {
			F++;
			eval[_sequences[i].cl()].second++;
		}
	}
	cout << "#V: "<< V << ", #F:" << F << ", acc:" << (double)V/(double)(V+F) <<endl;
	cout << "=== Contingency matrix ===" <<endl;
	for(unsigned int i=0;i<nc();i++) {
		for(unsigned int j=0;j<nc();j++) {
			cout << setfill(' ') << setw(4) << contingencymatrix[i][j] <<", ";
		}
		cout << endl;
	}
	cout << "==========================" <<endl;
	return eval;
}

//////////////////////////////////////////////////////////////////////////////////


TimeSeries::TimeSeries(Dataset &ds):_X(nullptr),_ds(ds),_cl(0) {
	_scaling = new double[_ds.dim()];
	_decay = new double[_ds.dim()];
	memset ( _scaling, 0, _ds.dim()*sizeof(double) );
	memset ( _decay, 0, _ds.dim()*sizeof(double) );
}

TimeSeries::TimeSeries(TimeSeries &&s):_X(s._X),_ds(s._ds),_cl(s._cl),_scaling(s._scaling),_decay(s._decay) {
	s._X=nullptr;
	s._scaling=nullptr;
	s._decay=nullptr;
}

TimeSeries::~TimeSeries() {
	if(_X) delete[](_X);
	if(_scaling) delete[](_scaling);
	if(_decay) delete[](_decay);
}


/**
 * The function read the data from the string str.
 *
 * The expected number of values must be coherent with the dataset characteristics (dimension and length).
 *
 * If _X is not null, its content is destructed.
 *
 * \pre The _ds characteristics must have been set up before
 * \warning Warn on wrong number of values (but continue!)
 */
int TimeSeries::read(string str) {
	if(dim()==0 || length()==0) {
		cerr << "Error TimeSeries::read: error unknown dimensions and length."<<endl;
		return 1;
	}
	if(_X!=nullptr) {
		delete[](_X);
	}

	unsigned int d = dim()*length();
	_X = new double[d]();
	stringstream sstr(str);
	unsigned int i=0;
	double val=0.0;
	while( sstr.good() && i<d ) {
		/*
		if( i>=dim ) {
			cerr << "Warning TimeSeries::read: too many data (ignore last values)."<<endl;
			break;
		}*/
		sstr >> val;
		_X[i]=val;
		i++;
	}

	if( i<d ) {
		cerr << "Warning TimeSeries::read: not enough data (zero completion)."<<endl;
		while(i<d) {
			_X[i]=0.0;
			i++;
		}
		_cl=0;
		return 2;
	}

	//example class
	string clval;
	sstr >> clval;
	auto itcl=_ds._classes.find(clval);
	if( itcl==_ds._classes.end() ) {
		_cl = _ds._classes.size();
		_ds._classes.insert( std::pair<string, int>(clval, _cl) );
	} else {
		_cl = itcl->second;
	}
	return 0;
}

/**
 * Compute the Euclidian distance between the multi-dimensional time-series (norm L2)
 */
double TimeSeries::deuclid(const TimeSeries &S, Interval I) const {
	assert( I.r> I.l && I.r<=_ds.length() );
	double dst=0;
	unsigned int i=I.l;
	double *Sp=S._X+i;
	double *p=_X+i;
	for(unsigned int d=0; d<_ds.dim(); d++) {
		for(i=I.l; i<I.r; i++, Sp++, p++) {
			double r=(*Sp - *p);
			dst+= r*r;
		}
		Sp+=_ds.length()-I.r+I.l;
		p+=_ds.length()-I.r+I.l;
	}
	return dst;
}

double TimeSeries::deuclid(const TimeSeries &S, unsigned int var, Interval I) const {
	assert( I.r> I.l && I.r<=_ds.length() );
	double dst=0;
	unsigned int i=I.l;
	double *Sp=S._X+var*length();
	double *p=_X+var*length();
	for(i=I.l; i<I.r; i++, Sp++, p++) {
		double r=(*Sp - *p);
		dst+= r*r;
	}
	return dst;
}

/**
 * Distance en norme L1
 */
double TimeSeries::dunif(const TimeSeries &S, Interval I) const {
	assert( I.r> I.l && I.r<=_ds.length() );
	double dst=0;
	unsigned int i=I.l;
	double *Sp=S._X+i;
	double *p=_X+i;
	for(unsigned int d=0; d<_ds.dim(); d++) {
		for(i=I.l; i<I.r; i++, Sp++, p++) {
			dst+= abs(*Sp - *p);
		}
		Sp+=_ds.length()-I.r+I.l;
		p+=_ds.length()-I.r+I.l;
	}
	return dst;
}

double min(double a, double b, double c) {
	if( a<b ) {
		return (a<c?a:min(b,c));
	} else {
		return (b<c?b:min(a,c));
	}
}

/**
 * fonction de calcul d'une DTW à 1 dimension, en place !
 * \param X,Y: series temporelle
 * \param l: longueur de la série temporelle
 */
double dtw(double *X, double *Y, unsigned int l) {
	double *cost1 = new double[l];
	double *cost2 = new double[l];
	double **previous, **next, **tmp;
	double r;
	unsigned int i=0;
	//first line
	for(i=0;i<l;i++) {
		r=X[0]-Y[i];
		cost1[i]=r*r;
	}
	previous = &cost1;
	next= &cost2;
	for(unsigned int k=1;k<l;k++) {
		//first column
		r=X[k]-Y[0];
		(*next)[0]=r*r;
		//remaining part
		for(i=1;i<l;i++) {
			r=X[k]-Y[i];
			(*next)[i]=r*r + min( (*next)[i-1], (*previous)[i], (*previous)[i-1]);
		}
		//switch cost1 and cost2
		tmp=previous;
		previous=next;
		next=tmp;
	}
	return (*previous)[l-1];
}

/**
 * Compute the dtw between TS based on DTW between TS on each dimension.
 *
 * For each dimension, there is a dedicated mapping that minimize the dtw and then, the distances are additionned.
 */
double TimeSeries::dtw(const TimeSeries &S, Interval I) const {
	assert( I.r> I.l && I.r<=_ds.length() );
	double dst=0;
	double *Sp=S._X+I.l;
	double *p=_X+I.l;
	for(unsigned int d=0; d<_ds.dim(); d++) {
		dst+=::dtw(Sp, p, I.r-I.l+1);
		Sp+=_ds.length();
		p+=_ds.length();
	}
	return dst;
}

/**
 * Compute the dtw distance between 2 TS (one dimension, restricted to the interval I)
 * \param var dimension along which to compute the distance
 */
double TimeSeries::dtw(const TimeSeries &S, unsigned int var, Interval I) const {
	assert( I.r> I.l && I.r<=_ds.length() && var>=0 && var<_ds.dim() );
	double *Sp=S._X+var*length()+I.l;
	double *p=_X+var*length()+I.l;
	return ::dtw(Sp, p, I.r-I.l+1);
}

/**
 * fonction de calcul d'une DTW à nd dimensions, en place !
 * \param X,Y: series temporelle
 * \param l: longueur de la série temporelle
 * \param nd nombre de dimension
 * \param dd dimension decay dans les donnees
 *
 * La valeur à la dimension d et à la position temporelle t se situe à la position t+d*dd dans les vecteurs X et Y !
 */
double mddtw(double *X, double *Y, unsigned int l, unsigned int nd, unsigned int dd ) {
	double *cost1 = new double[l];
	double *cost2 = new double[l];
	double **previous, **next, **tmp;
	double r;
	unsigned int i=0;
	//first line
	for(i=0;i<l;i++) {
		r=X[0]-Y[i];
		cost1[i]=r*r;
	}
	previous = &cost1;
	next= &cost2;
	for(unsigned int k=1;k<l;k++) {
		//first column
		r=X[i]-Y[0];
		(*next)[0]=r*r;
		//remaining part
		for(i=1;i<l;i++) {
			double cost=0;
			for(unsigned int d=0;d<nd; d++){
				r=X[k+d*dd]-Y[i+d*dd];
				cost += r*r;
			}
			(*next)[i]=cost + min( (*next)[i-1], (*previous)[i], (*previous)[i-1]);
		}
		//switch cost1 and cost2
		tmp=previous;
		previous=next;
		next=tmp;
	}
	return (*previous)[l-1];
}

/**
 * Compute the multi-dimensional DTW between S and this, along the I interval
 * The fonction do the mapping by minimizing the overall distance between the MTS
 *
 * There is thus a single mapping for all the TS dimension.
 *
 * \see TimeSeries::dtw
 */
double TimeSeries::mddtw(const TimeSeries &S, Interval I) const {
	assert( I.r> I.l && I.r<=_ds.length() );
	return ::mddtw(S._X+I.l, _X+I.l, I.r-I.l+1, _ds.dim(), _ds.length());
}

/**
 * The function normalizes each dimension of the time series
 * 	-> the normalisation is based on mean/stddev: (X-mean)/stddev
 * 	-> the _scaling (stddev) and decay (_decay) are saved
 */
void TimeSeries::normalize() {
	unsigned int i=0;
	double *p=_X;
	for(unsigned int d=0; d<_ds.dim(); d++) {
		//p=_X+d*_ds.length();
		//double minv=*p, maxv=*p;
		double meanv=*p;
		p++;
		for(i=1; i<_ds.length(); i++, p++) {
			meanv+=*p;
			//minv=(minv<*p?minv:*p);
			//maxv=(maxv>*p?maxv:*p);
		}
		meanv /= (double) _ds.length();
		//compute stddev
		p -= _ds.length();
		double stddev=0;
		double r=0.0;
		for(i=0; i<_ds.length(); i++, p++) {
			r=(*p-meanv);
			stddev+=r*r;
		}
		//normalisation
		p -= _ds.length();
		for(i=0; i<_ds.length(); i++, p++) {
			*p = (*p-meanv)/stddev;
		}
		_scaling[d]=stddev;
		_decay[d]=meanv;
	}
}

/**
 * Temporal correlation coefficient
 * Computation time O(dim x |I|), where |I| = I.r-I.l
 */
double TimeSeries::cort(const TimeSeries &s, Interval I) const {
	double diffa=0,diffb=0,dena=0,denb=0,num=0;
	unsigned int d,j;

	double *p=_X+I.l;
	double *Sp=s._X+I.l;
	for(d=0;d<_ds.dim();d++){
		for (j=0; j < I.r-I.l; j++){
			diffa= (*p - *(p+1));
			diffb= (*Sp - *(Sp+1));
			num+= diffa * diffb;
			dena+= diffa * diffa;
			denb+= diffb * diffb;
		}
		*p += length()-I.r+I.l;
		*Sp += length()-I.r+I.l;
	}
	if(dena==0)dena=1;
	if(denb==0)denb=1;
	return num / ( sqrt(dena*denb) );
}

/**
 * Correlation coefficient
 *
 * Computation time O(dim x |I|^2), where |I| = I.r-I.l
 */
double TimeSeries::corr(const TimeSeries &s, Interval I) const {
	double diffa=0,diffb=0,dena=0,denb=0,num=0;
	unsigned int d,j,i;

	for(d=0;d<_ds.dim();d++){
		for (i=I.l; i < I.r; i++){
			for (j=I.l; j < I.r; j++){
				diffa= _X[i] - _X[j];
				diffb= s._X[i] - s._X[j];
				num+= diffa * diffb;
				dena+= diffa * diffa;
				denb+= diffb * diffb;
			}
		}
	}
	if(dena==0)dena=1;
	if(denb==0)denb=1;
	return num / ( sqrt(dena*denb) );
}

double TimeSeries::dissimilarity(const TimeSeries &s, Interval I, unsigned int k, bool usedtw, bool usecorr) const {
	if(usedtw) {
		/*
		if(usecorr) {
			return 2  * deuclid (s,I) / ( 1 + exp( k * corr(s,I)) ) ;
		} else {
			return 2  * deuclid (s,I) / ( 1 + exp( k * cort(s,I)) ) ;
		}*/
		return 0.0;
	} else {
		if(usecorr) {
			return 2  * deuclid (s,I) / ( 1 + exp( k * corr(s,I)) ) ;
		} else {
			return 2  * deuclid (s,I) / ( 1 + exp( k * cort(s,I)) ) ;
		}
	}
}


