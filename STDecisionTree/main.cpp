/*
 * main.cpp
 *
 *  Created on: 9 août 2014
 *      Author: tguyet
 */

#include "dataset.h"
#include "DecisionTree.h"
#include <iostream>
#include <ctime>

/*
 * Compilation:
 *
 * 		g++ -o Arbre_TG -DVPARA -O3 -std=c++11 -pthread ../dataset.cpp ../DecisionTree.cpp ../main.cpp
 *
 * -> Le code utilise des fonctions et syntaxes spécifiques à C++11, un compilateur compatible est nécessaire
 * -> L'utilisation de VPARA compile le programme en version multi-threadée
 */

using namespace std;

int main() {
	Dataset ds;
	/*
	//Jeu de données aléatoire Cylinder, Bell, Funnel
	ds.setDim(1);
	ds.setLength(128);
	ds.read("./data/cbf.csv");
	//*/

	/*
	// Senegal/Touba image 1000 examples NDVI/EVI
	ds.setDim(2);
	ds.setLength(23);
	ds.read("./data/images/output.csv");
	//*/

	/*
	// Senegal/ZAE, NDVI/EVI time series
	//  > toclassify.csv: 1283 examples, random positions in the Senegal
	//  > toclassify_sne.csv: 2400 examples, from selected regions in ZAE
	ds.setDim(2);
	ds.setLength(23);
	ds.read("./data/images/toclassify_sne.csv");
	//*/
	
	
	//*
	// Senegal/ZAE, NDVI/EVI/721 spectral bands time series
	//  > toclassify_5bands.csv: 1567 examples, random positions in the Senegal
	ds.setDim(5);
	ds.setLength(23);
	ds.read("./data/images/toclassify_5bands.csv");
	//*/

	cout << "read sequences: " << ds.Sequences().size() <<endl;



	//HACK: faire un precalcul des distances sur l'interval le plus général ?

	DecisionTree T(ds);
	T.useIG();
	T.set_min_node_size(10);
	T.set_alpha(.6);
	
	// HACK : pour réaliser un apprentissage "classique" avec distance euclidienne simple : 
	//T.set_alpha(0); //fix the alpha to 0: disable the TSSplit*/


	//exemple de cross-validation
	double mean_acc=0;
	int ncv=5;
	double learn_sampling_rate=0.3;
	time_t now, later;
	time(&now);
	for(int cv=0; cv<ncv; cv++) {
		cout << "******* " << cv+1 <<"/" << ncv <<" *******"<<endl;
		ds.cv_gen( learn_sampling_rate*ds.nobs() );
		T.buildTree();
		T.print();
		std::vector< std::pair<unsigned int,unsigned int> > eval = ds.evaluate(T);
		double P=0,N=0;
		for(auto p : eval) {
			P+=p.first; N+=p.second;
		}
		mean_acc+=P/(P+N);
	}
	time(&later);
	double seconds = difftime(later,now);
	cout << "time elapsed: "  << seconds  << endl;
	mean_acc/=ncv;
	cout << "mean accuracy: " << mean_acc << endl;

	flush(cout);
	return 0;
}

