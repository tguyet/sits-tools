/*
 * QFZSegmentation.h
 *
 *  Created on: 18 déc. 2013
 *      Author: tguyet
 */

#ifndef QFZSEGMENTATION_H_
#define QFZSEGMENTATION_H_

#include <list>
#include <vector>

using namespace std;

/**
 * ConnectedComponent
 */
typedef struct {
	float min, max; //min/max value
	float mean; //mean value of the CC
	int nb;
} CC;

typedef struct {
	float *min, *max; //min/max value
	float *mean; //mean value of the CC
	int nb;
} CCd;



int **QFZ(float **input_image, int height, int width, float alpha, vector<CC> &concomp, float alphastep =0.1);
int *QFZMD(float **input_image, int height, int width, int d, float alpha, vector<CCd> &concomp, float alphastep =0.1, int debuglevel =0);


void testQFZ(int alpha =4);
void testQFZMD();


#endif /* QFZSEGMENTATION_H_ */
