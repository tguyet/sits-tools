/*
 * Classification.h
 *
 *  Created on: 30 mai 2013
 *      Author: cherif
 */

#ifndef CLASSIFICATION_H_
#define CLASSIFICATION_H_

#include <vector>
#include <list>
#include "Baatz.h"

class string;


class Classification {
public:
	/* node of time_serie list */
	typedef struct
	{
		float *avg_color; 		/* average colors of pixels, one for each band interwined, eg for 3 bands: 123123123 */
		//float *std_color; 		/* std of pixel colors, one for each band */
		unsigned int dim; 		//< overall dimension
		unsigned int nbd;		//< nb of dates
		struct segment *seg;	//< keep a reference on the pixel concerned by the time serie
	} time_serie;

protected:
	std::vector<time_serie> _TS;

	typedef struct {
		time_serie means;
		std::list<unsigned int> ts;
		std::list<unsigned int> old;
		float error;
	} centroid;

	std::vector<centroid> _centroids;

	unsigned int max_iter;

	unsigned int _dba_maxiter;


	typedef enum {EUCLIDEAN, DTW} ClassifMethod;
	ClassifMethod _method;

public:
	Classification();
	virtual ~Classification();

	void write_class(unsigned char *output_image);

	/**
	 * d: total number of dimensions
	 * nbd: number of date
	 */
	void add_timeseries(struct segment *initial_segment, unsigned int d, unsigned int nbd);

	float classif(int k, int verbose);

	const std::vector<centroid>& centroids() const {return _centroids;};
	
	void save_centroids(const char *output_filename) const ;
	
	void save_ts(const char *output_filename) const ;

	void setDTW() {_method=DTW;};
	void setEuclidean() {_method=EUCLIDEAN;};

	/**
	 * Fonction à appliquer après une classification et qui modifie l'image utilisée en entrée
	 * (sous la forme de segments) en attribuant le profil du centroid
	 * à chacun des pixels de l'image !
	 */
	void propagate_centroids();

private:
	void add(struct segment *, unsigned int d, unsigned int nbd);

	float euclidean_distance(const time_serie &, const time_serie &) const;
	float dtw_distance(const time_serie &, const time_serie &) const;

	bool euclidian_mean(centroid &c);

	bool dtw_mean(centroid &c);
	//time_serie dba_one_iteration(centroid &c, time_serie &average);
	void dba_one_iteration(centroid &c, float *average, unsigned int dim, unsigned int nbd);

	void init_classif(unsigned int k);

	unsigned int best_cluster(time_serie ts, float *error) const;
};

#endif /* CLASSIFICATION_H_ */


