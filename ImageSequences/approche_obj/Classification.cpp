/*
 * Classification.cpp
 *
 *  Created on: 30 mai 2013
 *      Author: Mohand-Chérif BENYOUNÉS
 */

#define MAX_FLT 3.4e38 /* maximum float value */

// Internal includes
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <math.h>
#include <cmath>
#include <omp.h>
#include "Classification.h"
#include "Baatz.h"
#include <cstring>

using namespace std;
/* node of segment list */

segment **seg_ptr_vector;
segment *initial_seg;
segment *final_seg;

Classification::Classification() {
	max_iter=10;
	_dba_maxiter=15;
	_method=EUCLIDEAN;
	_method=DTW;
	srand(time(NULL));
}

Classification::~Classification() {
}


void Classification::add_timeseries(struct segment *initial_segment, unsigned int d, unsigned int nba) {
	struct segment *aux_segment;
	aux_segment=initial_segment;

	while (aux_segment!=NULL) {
		add(aux_segment, d, nba);
		aux_segment=aux_segment->next_segment;
	}
}

/**
 * nba: number of date
 */
void Classification::add(struct segment *s, unsigned int d, unsigned int nba) {
	time_serie ts;
	ts.avg_color=s->avg_color;   // pas de recopie des données
	//ts.std_color=s->std_color;   // idem
	ts.dim=d;
	ts.nbd=nba;
	ts.seg=s;

	_TS.push_back(ts);
}

void Classification::init_classif(unsigned int k) {
	k = (k<_TS.size()?k:_TS.size());

	_centroids = vector<centroid>(k);
	unsigned int i=0;
	unsigned int l;

	while( i<k ) {
		//Random generator : init centroids with random time series
		l=rand() % _TS.size();

		//Create a new centroid
		centroid c;
		c.means.avg_color = (float*)malloc( sizeof(float)*_TS[l].dim );
		if( c.means.avg_color==NULL ) {
			cout << "Allocation ERROR" <<endl;
		}
		memcpy( c.means.avg_color, _TS[l].avg_color, sizeof(float)*_TS[l].dim);
		//c.means.std_color = NULL;
		c.means.seg=NULL;
		c.means.dim=_TS[l].dim;
		c.means.nbd=_TS[l].nbd;
		c.ts.push_back(l);
		c.error=0.0;
		
		_centroids[i]=c;
		i++;
	}
}


float Classification::euclidean_distance(const time_serie & ts1, const time_serie & ts2) const {
	float dist=0;

	for(unsigned int i=0; i< ts1.dim; i++) {
		float r=ts1.avg_color[i]-ts2.avg_color[i];
		dist = dist + r*r;
	}
	
	//return sqrt(dist); //TG: supprimer le sqrt: ça ne sert pas à grand chose et c'est long à calculer !
	return dist;
}

/**
 * Retourne vrai si pour ce cluster n'a pas changée (c.ts==c.old)
 */
bool Classification::euclidian_mean(centroid &c) {
	c.ts.sort();
	c.old.sort();
	list<unsigned int>::iterator it_ts=c.ts.begin();
	list<unsigned int>::iterator it_old= c.old.begin();
	
	while( it_ts!= c.ts.end() && it_old!= c.old.end() && (*it_ts)==(*it_old) ) {
		it_ts++;
		it_old++;
	}
	
	if( (*it_ts)==(*it_old) ) {	 //ICI : bien comparer des nombres et non des itérateurs !
		return true;
	} else { //something changed : means are recomputed !
		for(unsigned int i=0; i< c.means.dim; i++) {
			c.means.avg_color[i]= 0;
		}
		
		list<unsigned int>::iterator it_ts=c.ts.begin();
		while( it_ts!= c.ts.end() ) {
			float *tab=_TS[*it_ts].avg_color;
			for(unsigned int i=0; i< c.means.dim; i++) {
				c.means.avg_color[i]=c.means.avg_color[i]+tab[i];
			}
			it_ts++;
		}

		if( c.ts.size()!=0 ) {
			for(unsigned int i=0; i<c.means.dim; i++) {
				c.means.avg_color[i]/=c.ts.size();
			}
		}
	
		return false;
	}
}


/////////////////////////  DBA //////////////////////////////

double min3(double a, double b, double c) {
	if( b<a ) {
		a=b;
	}
	if( c<a) {
		return c;
	} else {
		return a;
	}
}

short argmin3(double a, double b, double c) {
	short val=0;
	if( b<a ) {
		a=b;
		val=1;
	}
	if( c<a) {
		return 2;
	} else {
		return val;
	}
}

float mean(const std::list<float> &values) {
	std::list<float>::const_iterator it = values.begin();
	float m=0;
	while(it != values.end()) {
		m+=*it;
		it++;
	}
	return m/(float)(values.size());
}

/**
 * based on the DBA algorithm (Petitjean et al.)
 */
bool Classification::dtw_mean(centroid &c) {
	//cout << ">> DTW MEAN <<"<<endl;
	c.ts.sort();
	c.old.sort();
	list<unsigned int>::iterator it_ts=c.ts.begin();
	list<unsigned int>::iterator it_old= c.old.begin();

	while( it_ts!= c.ts.end() && it_old!= c.old.end() && (*it_ts)==(*it_old) ) {
		it_ts++;
		it_old++;
	}

	if( (*it_ts)==(*it_old) ) {	 //ICI : bien comparer des nombres et non des itérateurs !
		return true;
	}

	//something changed : means are recomputed !
	time_serie ts = _TS[ c.ts.front() ];
	float *average = (float*) calloc(sizeof(float),ts.dim);
	memcpy(average,ts.avg_color,sizeof(float)*c.means.dim);
	for(unsigned int i=0; i<_dba_maxiter; i++) {
		//cout << "DBA ONE ITERATION"<<endl;
		dba_one_iteration(c, average, c.means.dim, c.means.nbd);
	}

	memcpy(c.means.avg_color,average,sizeof(float)*c.means.dim);

	free(average);
	return false;
}

/**
 * multidimensional distances
 * \param l length
 * \param
 */

float dist(float *s, unsigned int x, float *t, unsigned int y, unsigned int l, unsigned int ndim) {
	float d=0;
	float *ps=s+x;
	float *pt=t+x;
	for(unsigned int i=0; i<ndim;i++) {
		d+=(*ps - *pt)*(*ps - *pt);
		ps+=l;
		pt+=l;
	}
	return d;
}



float dist(const Classification::time_serie &s, unsigned int x, const Classification::time_serie &t, unsigned int y) {
	float d=0;
	float *ps=s.avg_color+x;
	float *pt=t.avg_color+x;
	int nd=s.dim/s.nbd; //number of dimensions
	for(unsigned int i=0; i<nd; i++) {
		float r=(*ps - *pt);
		d+=r*r;
		ps+=s.nbd;
		pt+=t.nbd;
	}
	return d;
}

void Classification::dba_one_iteration(centroid &c, float *average, unsigned int dim, unsigned int nbd) {
	unsigned int l = nbd;
	std::vector< std::list<float> > tupleAssociation(l);

	//DTW matrices
	float **cost = (float**)calloc( sizeof(float*), l);
	short **path = (short**)calloc( sizeof(short*), l);
	for(unsigned int i=0;i<l;i++) {
		cost[i]=(float*)calloc( sizeof(float), l);
		path[i]=(short*)calloc( sizeof(short), l);
	}
	list<unsigned int>::iterator it = c.ts.begin();
	while( it != c.ts.end() ) { //for all sequences in the subset :
		float *seq=_TS[ *it ].avg_color;
		//compute the DTW

		cost[0][0] = dist(average, 0, seq, 0, l, dim/nbd);
		path[0][0] = -1;
		for(unsigned int j=1; j<l; j++) {
			cost[j][0] = cost[j-1][0] + dist(average, j, seq, 0, l, dim/nbd);
			path[j][0] = 2;
		}
		for(unsigned int j=1; j<l; j++) {
			cost[0][j] = cost[0][j-1] + dist(average, 0, seq, j, l, dim/nbd);
			path[0][j] = 1;
		}
		for(unsigned int i=1; i<l; i++) {
			for(unsigned int j=1; j<l; j++) {
				float res;
				short indiceRes = argmin3(cost[i-1][j-1],cost[i][j-1],cost[i-1][j]);

				if(indiceRes==0) {
					res = cost[i-1][j-1];
				} else if (indiceRes==1) {
					res = cost[i][j-1];
				} else {
					res = cost[i-1][j];
				}

				cost[i][j] = res + dist(average, i, seq, j, l, dim/nbd);
				path[i][j]=indiceRes;
			}
		}
		//DTW path
		int i=l-1;
		int j=l-1;

		while(true) {
			tupleAssociation[i].push_back(seq[j]);
			if(path[i][j]==0) {
				i=i-1;
				j=j-1;
			} else if (path[i][j]==1) {
				j=j-1;
			} else if (path[i][j]==2) {
				i=i-1;
			} else {
				break;
			}
		}
		it++;
	}

	//compute the average time serie
	for(unsigned int t=0; t<l; t++) {
	   average[t] = mean(tupleAssociation[t]);
	}

	//free allocated memory
	for(unsigned int i=0;i<l;i++) {
		free(cost[i]);
		free(path[i]);
	}
	free(cost);free(path);
}


// compute the DTW between two time series of different length
float Classification::dtw_distance(const time_serie &s, const time_serie &t) const {
	unsigned int ls=s.nbd;
	unsigned int lt=t.nbd;
	if( s.dim/s.nbd != t.dim/t.nbd ) {
		cerr << "Classification::dtw_distance: Warning! different dimension of time series" << endl;
	}

	//DTW matrices
	float **cost = (float**)calloc( sizeof(float*), ls);
	for(unsigned int i=0;i<ls;i++) {
		cost[i]=(float*)calloc( sizeof(float), lt);
	}

	cost[0][0] = dist(s, 0, t, 0);
	for(unsigned int j=1; j<ls; j++) {
		cost[j][0] = cost[j-1][0] + dist(s, j, t, 0);
	}
	for(unsigned int j=1; j<lt; j++) {
		cost[0][j] = cost[0][j-1] + dist(s, 0, t, j);
	}
	for(unsigned int i=1; i<ls; i++) {
		for(unsigned int j=1; j<lt; j++) {
			cost[i][j] = min3(cost[i-1][j-1],cost[i][j-1],cost[i-1][j]) + dist(s, i, t, j);
		}
	}
	float ret = cost[ls-1][lt-1];

	//free allocated memory
	for(unsigned int i=0;i<ls;i++) {
		free(cost[i]);
	}
	free(cost);
	return ret;
}

/////////////////////////  END DBA //////////////////////////////

unsigned int Classification::best_cluster(time_serie ts, float *error) const {
	float best = euclidean_distance(ts, _centroids[0].means);
	unsigned int argbest=0;
	unsigned int k=1;
	while( k<_centroids.size() ) {
		float d;
		if(_method==EUCLIDEAN) {
			d=euclidean_distance(ts, _centroids[k].means);
		} else {
			d=dtw_distance(ts, _centroids[k].means);
		}
		if( d<best ) {
			best=d;
			argbest=k;
		}
		k++;
	}

	if(error) *error=best;
	return argbest;
}


/**
* \return Return the squared error
*/
float Classification::classif(int k, int verbose) {
	init_classif(k);

	bool end = false;
	unsigned int nb_iter=0;
	float cerror, error=0.0;

	while( (nb_iter<max_iter) && !end) {
		if( verbose ) {
			printf ("\t\t step %d/%d : %f\n", nb_iter, max_iter, error);
		}
		error=0.0;

		//Assignement phase
		unsigned int i=0;
		for(i=0; i<_centroids.size(); i++) {
			_centroids[i].old=_centroids[i].ts;  //HACK très bourrin !
			_centroids[i].ts.clear();
			_centroids[i].error=0.0;
		}

		for(i=0; i<_TS.size(); i++) {
			//Find the closest cluster for _TS[i]
			unsigned int bc = best_cluster(_TS[i],&cerror);
			_centroids[bc].ts.push_back(i);
			_centroids[bc].error+=cerror;
			error+=cerror;
		}

		//Compute new centroids means
		end = true;
		for(i=0; i<_centroids.size(); i++) {
			if(_method==EUCLIDEAN) {
				end= euclidian_mean( _centroids[i] ) && end;
			} else {
				end= dtw_mean( _centroids[i] ) && end;
			}
		}

		nb_iter++;
	}
	
	return k*error;
}


/**
 *
 *
 **/
void Classification::propagate_centroids() {
	unsigned int i;
	struct segment *aux_segment;
	unsigned int nb=0;

	for(i=0; i<_centroids.size(); i++) {//for all centroids

		list<unsigned int>::iterator it_ts=_centroids[i].ts.begin();
		while( it_ts!=_centroids[i].ts.end()) { //for all segments in the centroids
			time_serie &ts =_TS[*it_ts];
			aux_segment=_TS[*it_ts].seg;

			for(unsigned int d=0; d<ts.dim; d++) {
				aux_segment->avg_color[d]=_centroids[i].means.avg_color[d];
				//aux_segment->avg_color[d]=0;
				aux_segment->avg_color_square[d] = aux_segment->avg_color[d]*aux_segment->avg_color[d];
			}
			nb++;
			it_ts++;
		}
	}
	cout << "\tnb pixels changed : "<< nb << endl;
}


void Classification::write_class(unsigned char *output_image)
{
	unsigned int i;
	time_serie ts;
	struct segment_pixel *aux_pixel;
	struct segment *aux_segment;

	for(i=0; i<_centroids.size(); i++) {
		//int color=(int) floor( ((float)((i+1)*256.0))/((float)_centroids.size()) );

		list<unsigned int>::iterator it_ts=_centroids[i].ts.begin();
		while( it_ts!=_centroids[i].ts.end()) {
			ts =_TS[*it_ts];
			aux_segment=ts.seg;

			// for each pixel of the segment
			aux_pixel = aux_segment->pixel_list;
			while (aux_pixel!=NULL) {
				output_image[aux_pixel->pixel_id] = (unsigned char) i;
				//cout << aux_pixel->pixel_id << ":"<< color <<endl;
				/*
				if (aux_pixel->borderline) {
					//marquer la bordure des segments ?
				}*/
				aux_pixel = aux_pixel->next_pixel;
			}

			it_ts++;
		}
	}
}

void Classification::save_centroids(const char *output_filename) const {
	if( _centroids.empty() ) return;

	ofstream fichier_centroids(output_filename, ios::out);

	if( _centroids[0].means.nbd==0 ) {
		cerr << "Classification::save_centroids error: #date=0 !"<<endl;
		return;
	}

	unsigned int nbdates= _centroids[0].means.nbd; //Nb de dates
	unsigned int dim= _centroids[0].means.dim/nbdates;
	if( fichier_centroids.good() ) {
		for(unsigned int i=0; i<_centroids.size(); i++) {
			for(unsigned int j=0;j<dim;j++) {
				fichier_centroids << i ;
				for(unsigned int b=0; b<nbdates; b++) {
					fichier_centroids<<"," << _centroids[i].means.avg_color[b*dim+j];
				}
				fichier_centroids << endl;
			}
		}
		fichier_centroids.close();
	} else {
			cerr << "Erreur à l'ouverture du fichier " << output_filename << endl;
	}
}

		/*
void Classification::save_centroids(const char *output_filename) const {
	if( _centroids.empty() ) return;

	ofstream fichier_centroids(output_filename, ios::out);
	
	if( _centroids[0].means.nbd==0 ) {
		cerr << "Classification::save_centroids error: #date=0 !"<<endl;
		return;
	}

	unsigned int nbbands= _centroids[0].means.nbd; //Nb de bandes par date! _centroids[0].means.dim/
	if( fichier_centroids.good() ) {
		for(unsigned int b=0; b<nbbands; b++)
			for(unsigned int i=0; i<_centroids.size(); i++) {
				fichier_centroids << i ;
				for(unsigned int j=b;j<_centroids[i].means.dim;j=j+nbbands) {
					fichier_centroids<<"," << _centroids[i].means.avg_color[j];
				}
				fichier_centroids << endl;
			}

		fichier_centroids.close();
	} else {
		cerr << "Erreur à l'ouverture !" << endl;
	}
}
*/

void Classification::save_ts(const char *output_filename) const {
	if( _centroids.empty() ) return;

	ofstream fichier_ts(output_filename, ios::out);
	
	if( _centroids[0].means.nbd==0 ) {
		cerr << "Classification::save_ts error: #date=0 !"<<endl;
		return;
	}
	unsigned int nbdates= _centroids[0].means.nbd; //Nb de dates
	unsigned int dim= _centroids[0].means.dim/nbdates; //Nb de variables

	if( fichier_ts.good() ) {
		for(unsigned int i=0; i<_centroids.size(); i++) {
			list<unsigned int>::const_iterator it_ts=_centroids[i].ts.begin();
			while( it_ts!= _centroids[i].ts.end() ) {
				fichier_ts << endl;
				for(unsigned int j=0;j<dim;j++) {
					fichier_ts << i << "," << j;
					for(unsigned int b=0; b<nbdates; b++) {
						fichier_ts<<"," << _TS[(*it_ts)].avg_color[b*dim+j];
					}
					fichier_ts << endl;
				}
				it_ts++;
			}

		}

		fichier_ts.close();
	} else {
		cerr << "Classification::save_ts error: Erreur à l'ouverture !" << endl;
	}
}


