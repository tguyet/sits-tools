/*
 * dba.cpp
 *
 *  Created on: 8 juil. 2015
 *      Author: tguyet
 */

#include <cstdlib>
#include <ctime>
#include <list>
#include <vector>
#include <iostream>

double min3(double a, double b, double c) {
	if( b<a ) {
		a=b;
	}
	if( c<a) {
		return c;
	} else {
		return a;
	}
}

short argmin3(double a, double b, double c) {
	short val=0;
	if( b<a ) {
		a=b;
		val=1;
	}
	if( c<a) {
		return 2;
	} else {
		return val;
	}
}

double mean(const std::list<double> &values) {
	std::list<double>::const_iterator it = values.begin();
	double m=0;
	while(it != values.end()) {
		m+=*it;
		it++;
	}
	return m/(double)(values.size());
}

#define dist(x,y) ( (x-y)*(x-y) )

double dtw(double *s, unsigned int ls, double *t, unsigned int lt, double **cost =NULL, short **path =NULL) {
	bool matricesdefined=true;
	if( (path==NULL && cost!=NULL) || (path!=NULL && cost==NULL) ) {
		std::cerr << "dtw error "<< std::endl;
		return 0;
	}

	if( cost == NULL && path==NULL) {
		//DTW matrices
		matricesdefined=false;
		cost = (double**)calloc( sizeof(double*), ls);
		path = (short**)calloc( sizeof(short*), ls);
		for(unsigned int i=0;i<ls;i++) {
			cost[i]=(double*)calloc( sizeof(double), lt);
			path[i]=(short*)calloc( sizeof(short), lt);
		}
	}

	//compute the DTW
	cost[0][0] = dist(s[0],t[0]);
	path[0][0] = -1;
	for(unsigned int j=1; j<ls; j++) {
		cost[j][0] = cost[j-1][0] + dist(s[j],t[0]);
		path[j][0] = 2;
	}
	for(unsigned int j=1; j<lt; j++) {
		cost[0][j] = cost[0][j-1] + dist(s[0],t[j]);
		path[0][j] = 1;
	}
	for(unsigned int i=1; i<ls; i++) {
		for(unsigned int j=1; j<lt; j++) {
			double res;
			short indiceRes = argmin3(cost[i-1][j-1],cost[i][j-1],cost[i-1][j]);

			if(indiceRes==0) {
				res = cost[i-1][j-1];
			} else if (indiceRes==1) {
				res = cost[i][j-1];
			} else {
				res = cost[i-1][j];
			}

			cost[i][j] = res + dist(s[i],t[j]);
			path[i][j]=indiceRes;
		}
	}

	if( !matricesdefined) {
		//free allocated memory
		for(unsigned int i=0;i<ls;i++) {
			free(cost[i]);
			free(path[i]);
		}
		free(cost);free(path);
	}
	return cost[ls-1][lt-1];
}

/**
 * The returned vector has been allocated by the function. The user becomes the owner of the vector and must delete it.
 *
 * HACK: memory usage to optimize !
 */
double *dba_one_iteration(double *average, double **sequences, unsigned int l, unsigned int n) {
	double *average_rt = new double(l);
	std::vector< std::list<double> > tupleAssociation(l);

	//DTW matrices
	double **cost = (double**)calloc( sizeof(double*), l);
	short **path = (short**)calloc( sizeof(short*), l);
	for(unsigned int i=0;i<l;i++) {
		cost[i]=(double*)calloc( sizeof(double), l);
		path[i]=(short*)calloc( sizeof(short), l);
	}


	for(unsigned int k=0; k<n; k++) { //for all sequences in the dataset
		double *seq=sequences[k];
		//compute the DTW
		cost[0][0] = dist(average[0],seq[0]);
		path[0][0] = -1;
		for(unsigned int j=1; j<l; j++) {
			cost[j][0] = cost[j-1][0] + dist(average[j],seq[0]);
			path[j][0] = 2;
		}
		for(unsigned int j=1; j<l; j++) {
			cost[0][j] = cost[0][j-1] + dist(average[0],seq[j]);
			path[0][j] = 1;
		}
		for(unsigned int i=1; i<l; i++) {
			for(unsigned int j=1; j<l; j++) {
				double res;
				short indiceRes = argmin3(cost[i-1][j-1],cost[i][j-1],cost[i-1][j]);

				if(indiceRes==0) {
					res = cost[i-1][j-1];
				} else if (indiceRes==1) {
					res = cost[i][j-1];
				} else {
					res = cost[i-1][j];
				}

				cost[i][j] = res + dist(average[i],seq[j]);
				path[i][j]=indiceRes;
			}
		}
		//DTW path
		int i=l-1;
		int j=l-1;

		while(true) {
			tupleAssociation[i].push_back(seq[j]);
			if(path[i][j]==0) {
				i=i-1;
				j=j-1;
			} else if (path[i][j]==1) {
				j=j-1;
			} else if (path[i][j]==2) {
				i=i-1;
			} else {
				break;
			}
		}
	}

	//compute the average time serie
	for(unsigned int t=1; t<l; t++) {
	   average_rt[t] = mean(tupleAssociation[t]);
	}

	//free allocated memory
	for(unsigned int i=0;i<l;i++) {
		free(cost[i]);
		free(path[i]);
	}
	free(cost);free(path);

	return average_rt;
}

double *dba(double **sequences, unsigned int l, unsigned int n, unsigned int it) {
	//start with a random sequence
	srand (time(NULL));
	int index = rand() % n;
	double *average_rt=sequences[index];
	for(unsigned int i=0; i<it; i++) {
		average_rt=dba_one_iteration(average_rt,sequences,l,n);
	}
	return average_rt;
}


