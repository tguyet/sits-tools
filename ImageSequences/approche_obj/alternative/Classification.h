/*
 * Classification.h
 *
 *  Created on: 30 mai 2013
 *      Author: cherif
 */

#ifndef CLASSIFICATION_H_
#define CLASSIFICATION_H_

#include <vector>
#include <list>
#include "Baatz.h"

class string;


class Classification {
public:

	/* node of time_serie list */
	typedef struct
	{
		float *avg_color; 		/* average colors of pixels, one for each band */
		float *std_color; 		/* std of pixel colors, one for each band */
		unsigned int dim; 		//< overall dimension
		unsigned int nbd;		//< nb of date
		struct segment *seg;	//< keep a reference on the pixel concerned by the time serie
	} time_serie;

	typedef struct {
		time_serie means;
		std::list<unsigned int> ts;
		std::list<unsigned int> old;
		float error;
	} centroid;
	
protected:
	std::vector<time_serie> _TS;
	std::vector<centroid> _centroids;
	unsigned int max_iter;

public:
	Classification();
	virtual ~Classification();

	void write_class(unsigned char *output_image);

	void add_timeseries(struct segment *initial_segment, unsigned int d, unsigned int nbd);

	float classif(int k, int verbose);

	const std::vector<centroid>& centroids() const {return _centroids;};
	const time_serie& TS(unsigned int id) const {return _TS[id];};
	
	void save_centroids(const char *output_filename) const ;
	
	void save_ts(const char *output_filename) const ;

private:
	void add(struct segment *, unsigned int d, unsigned int nbd);

	float euclidean_distance(const time_serie &, const time_serie &) const;
	float dtw_distance(const time_serie &, const time_serie &) const;

	bool euclidian_mean(centroid &c);

	void dtw_mean(centroid c);

	void init_classif(unsigned int k);

	unsigned int best_cluster(time_serie ts, float *error) const;
};

#endif /* CLASSIFICATION_H_ */


