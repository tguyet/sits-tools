/*
 * Classification.cpp
 *
 *  Created on: 30 mai 2013
 *      Author: Mohand-Chérif BENYOUNÉS
 */

#define MAX_FLT 3.4e38 /* maximum float value */

// Internal includes
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <math.h>
#include <cmath>
#include <omp.h>
#include "Classification.h"
#include "Baatz.h"
#include <cstring>

using namespace std;
/* node of segment list */

segment **seg_ptr_vector;
segment *initial_seg;
segment *final_seg;

Classification::Classification() {
	max_iter=100;
}

Classification::~Classification() {
}


void Classification::add_timeseries(struct segment *initial_segment, unsigned int d, unsigned int nba) {
	struct segment *aux_segment;
	aux_segment=initial_segment;

	int i=0;
	
	while (aux_segment!=NULL) {
		add(aux_segment, d, nba);
		i++;
		aux_segment=aux_segment->next_segment;
	}
}

void Classification::add(struct segment *s, unsigned int d, unsigned int nba) {
	time_serie ts;
	ts.avg_color=s->avg_color;   // pas de recopie des données
	ts.std_color=s->std_color;   // idem
	ts.dim=d;
	ts.nbd=nba;
	ts.seg=s;

	_TS.push_back(ts);
}

void Classification::init_classif(unsigned int k) {
	k = (k<_TS.size()?k:_TS.size());

	_centroids = vector<centroid>(k);
	unsigned int i=0;
	unsigned int l;

	while( i<k ) {
		//Random generator
		l=rand() % _TS.size();

		//Create a new centroid
		centroid c;
		c.means.avg_color = (float*)malloc( sizeof(float)*_TS[l].dim );
		if( c.means.avg_color==NULL ) {
			cout << "Allocation ERROR" <<endl;
		}
		memcpy( c.means.avg_color, _TS[l].avg_color, sizeof(float)*_TS[l].dim);
		c.means.std_color = NULL;
		c.means.seg=NULL;
		c.means.dim=_TS[l].dim;
		c.means.nbd=_TS[l].nbd;
		c.ts.push_back(l);
		c.error=0.0;
		
		_centroids[i]=c;
		i++;
	}
}


float Classification::euclidean_distance(const time_serie & ts1, const time_serie & ts2) const {
	float dist=0;

	for(unsigned int i=0; i< ts1.dim; i++) {
		float r=ts1.avg_color[i]-ts2.avg_color[i];
		dist = dist + r*r;
	}
	
	return sqrt(dist); //TG: supprimer le sqrt: ça ne sert pas à grand chose et c'est long à calculer !
}

float Classification::dtw_distance(const time_serie &, const time_serie &) const {
	float d=0;
	return d;
}

/**
 * Retourne vrai si pour ce cluster n'a pas changée (c.ts==c.old)
 */
bool Classification::euclidian_mean(centroid &c) {
	c.ts.sort();
	c.old.sort();
	list<unsigned int>::iterator it_ts=c.ts.begin();
	list<unsigned int>::iterator it_old= c.old.begin();
	
	while( it_ts!= c.ts.end() && it_old!= c.old.end() && (*it_ts)==(*it_old) ) {
		it_ts++;
		it_old++;
	}
	
	if( (*it_ts)==(*it_old) ) {	 //ICI : bien comparer des nombres et non des itérateurs !
		return true;
	} else { //something changed : means are recomputed !
		for(unsigned int i=0; i< c.means.dim; i++) {
			c.means.avg_color[i]= 0;
		}
		
		list<unsigned int>::iterator it_ts=c.ts.begin();
		while( it_ts!= c.ts.end() ) {
			float *tab=_TS[*it_ts].avg_color;
			for(unsigned int i=0; i< c.means.dim; i++) {
				c.means.avg_color[i]=c.means.avg_color[i]+tab[i];
			}
			it_ts++;
		}

		if( c.ts.size()!=0 ) {
			for(unsigned int i=0; i<c.means.dim; i++) {
				c.means.avg_color[i]/=c.ts.size();
			}
		}
	
		return false;
	}
}


void Classification::dtw_mean(centroid c) {

}

unsigned int Classification::best_cluster(time_serie ts, float *error) const {
	float best = euclidean_distance(ts, _centroids[0].means);
	unsigned int argbest=0;
	unsigned int k=1;
	while( k<_centroids.size() ) {
		float d=euclidean_distance(ts, _centroids[k].means);
		if( d<best ) {
			best=d;
			argbest=k;
		}
		k++;
	}

	if(error) *error=best;
	return argbest;
}


/**
* \return Return the squared error
*/
float Classification::classif(int k, int verbose) {
	init_classif(k);

	bool end = false;
	unsigned int nb_iter=0;
	float cerror, error=0.0;

	while( (nb_iter<max_iter) && !end) {
		if( verbose ) {
			printf ("\t\t step %d/%d : %f\n", nb_iter, max_iter, error);
		}
		error=0.0;

		//Assignement phase
		unsigned int i=0;
		for(i=0; i<_centroids.size(); i++) {
			_centroids[i].old=_centroids[i].ts;  //HACK très bourrin !
			_centroids[i].ts.clear();
			_centroids[i].error=0.0;
		}

		for(i=0; i<_TS.size(); i++) {
			//Find the closest cluster for _TS[i]
			unsigned int bc = best_cluster(_TS[i],&cerror);
			_centroids[bc].ts.push_back(i);
			_centroids[bc].error+=cerror;
			error+=cerror;
		}

		//Compute new centroids means
		end = true;
		for(i=0; i<_centroids.size(); i++) {
			end= euclidian_mean( _centroids[i] ) && end;
		}

		nb_iter++;
	}
	
	return k*error;
}


void Classification::write_class(unsigned char *output_image)
{
	unsigned int i;
	time_serie ts;
	struct segment_pixel *aux_pixel;
	struct segment *aux_segment;

	for(i=0; i<_centroids.size(); i++) {
		//int color=(int) floor( ((float)((i+1)*256.0))/((float)_centroids.size()) );

		list<unsigned int>::iterator it_ts=_centroids[i].ts.begin();
		while( it_ts!=_centroids[i].ts.end()) {
			ts =_TS[*it_ts];
			aux_segment=ts.seg;

			// for each pixel of the segment
			aux_pixel = aux_segment->pixel_list;
			while (aux_pixel!=NULL) {
				output_image[aux_pixel->pixel_id] = (unsigned char) i;
				//cout << aux_pixel->pixel_id << ":"<< color <<endl;
				/*
				if (aux_pixel->borderline) {
					//marquer la bordure des segments ?
				}*/
				aux_pixel = aux_pixel->next_pixel;
			}

			it_ts++;
		}
	}
}


void Classification::save_centroids(const char *output_filename) const {
	if( _centroids.empty() ) return;

	ofstream fichier_centroids(output_filename, ios::out);
	
	if( _centroids[0].means.nbd==0 ) {
		cerr << "Classification::save_centroids error: #date=0 !"<<endl;
		return;
	}

	unsigned int nbdates= _centroids[0].means.nbd; //Nb de dates
	unsigned int dim= _centroids[0].means.dim/nbdates;
	if( fichier_centroids.good() ) {
		for(unsigned int i=0; i<_centroids.size(); i++) {
			for(unsigned int j=0;j<dim;j++) {
				fichier_centroids << i ;
				for(unsigned int b=0; b<nbdates; b++) {
					fichier_centroids<<"," << _centroids[i].means.avg_color[b*dim+j];
				}
				fichier_centroids << endl;
			}
		}
		fichier_centroids.close();
	} else {
		cerr << "Erreur à l'ouverture du fichier " << output_filename << endl;
	}
}


void Classification::save_ts(const char *output_filename) const {
	if( _centroids.empty() ) return;

	ofstream fichier_ts(output_filename, ios::out);
	
	if( _centroids[0].means.nbd==0 ) {
		cerr << "Classification::save_ts error: #date=0 !"<<endl;
		return;
	}

	unsigned int nbdates= _centroids[0].means.nbd; //Nb de dates
	unsigned int dim= _centroids[0].means.dim/nbdates; //Nb de variables
	if( fichier_ts.good() ) {
		for(unsigned int i=0; i<_centroids.size(); i++) {
		    list<unsigned int>::const_iterator it_ts=_centroids[i].ts.begin();
		    while( it_ts!= _centroids[i].ts.end() ) {
		    	for(unsigned int j=0;j<dim;j++) {
					fichier_ts << i << "," << j;
					for(unsigned int b=0; b<nbdates; b++) {
						fichier_ts<<"," << _TS[(*it_ts)].avg_color[b*dim+j];
					}
			    	fichier_ts << endl;
			    }
			    it_ts++;
		    }
			
		}

		fichier_ts.close();
	} else {
		cerr << "Classification::save_ts error: Erreur à l'ouverture !" << endl;
	}
}


