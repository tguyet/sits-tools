/*
 * main.cpp
 *
 *  Created on: 16 févr. 2013
 *      Author: tguyet
 */


#include "gdal.h"
#include "gdal_alg.h"
#include "ogr_api.h"
#include "cpl_conv.h" /* for CPLMalloc() */
#include "cpl_string.h"
//#include "omp.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <vector>
#include <string>

using std::cout;
using std::cerr;
using std::endl;

#include "Baatz.h"
#include "Classification.h"

/**
 * Lorsque REVERSE PROCESS est activé, alors le processus de traitement consiste
 * à faire d'abord une classification des pixels, puis de faire une segmentation
 * sur l'image issue de la classification.
 * L'image en sortie de classification est transformée pour avoir la série d'un
 * de représentant de classe pour chaque pixel (permet de faire une segmentation
 * classique par la suite).
 */
#define REVERSE_PROCESS 0

/**
* Definition of a data structure for the parameters
*/
typedef struct {
	//Object segmentation parameters
	float soo_scale;
	float soo_compactness;
	float soo_color;
	//Segmentation parameters
	int nb_class;
	//Input images parameters
	char *input_filename;
	int w,h,b; //image parameters, b is the total number of bands (nbd * nb_dates)
	
	int nbd; //number of date in the image
	
	//Output image parameters
	char *output_filename;
	double adfGeoTransform[6];
	
	bool do_polygonize;
	
	//verbose level
	int verbose;
	
	bool only_seg; //do only segmentation without classification
	bool normalize;
	
} params;


void set_default_params(params &p) {
	p.nb_class=15;
	p.soo_scale=200;
	p.soo_compactness=0.5;
	p.soo_color=0.5;
	p.input_filename = (char *)malloc(sizeof(char) * 500);
	strcpy(p.input_filename,"../Images/2004C.tif");
	p.w=0;
	p.h=0;
	p.b=0; // number of bands in the original image
	p.nbd=3; // number of dates
	p.output_filename = (char *)malloc(sizeof(char) * 500);
	p.output_filename[0]=0;
	p.do_polygonize=false;
	p.verbose=0;
	p.adfGeoTransform[0]=0.0;
	p.adfGeoTransform[1]=0.0;
	p.adfGeoTransform[2]=0.0;
	p.adfGeoTransform[3]=0.0;
	p.adfGeoTransform[4]=0.0;
	p.adfGeoTransform[5]=0.0;
	p.only_seg=false;
	p.normalize=false;
}

void free_params(params &p) {
	free(p.input_filename);
	free(p.output_filename);
}

GByte* process(params &p) ;
void usage(const char *comm);
void save_class_images(GByte *output_image_class, params &p);

int main(int argc, char **argv)
{
	//Lecture des parametres
	params p;
	
	set_default_params(p);
	
	if(argc<=1) {
		cerr << "An input file name is required\n";
		usage( argv[0] );
		return EXIT_FAILURE;
	}

	for(int i=1;i<argc;i++) {
		if( !strcmp(argv[i],"-h") ) {
			usage(argv[0]);
			return EXIT_SUCCESS;
		} else if( !strcmp(argv[i],"-v") ) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&p.verbose) ) {
				p.verbose=1;
				i--;
			}
		} else if(!strcmp(argv[i],"-c") || !strcmp(argv[i],"--nb-class")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&p.nb_class) || p.nb_class<=0 ) {
				cerr << "Parameter error: Expected strictly positive integer after --nb-class (-c)\n";
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-d") || !strcmp(argv[i],"--dates")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&p.nbd) || p.nbd<=0 ) {
				cerr << "Parameter error: Expected strictly positive integer after --dates (-d)\n";
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-s") || !strcmp(argv[i],"--scale")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%f",&p.soo_scale) || p.soo_scale<0 ) {
				cerr << "Parameter error: Expected positive real value after --scale (-s)\n";
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-co") || !strcmp(argv[i],"--compactness")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%f",&p.soo_compactness) || p.soo_compactness<0 ) {
				cerr << "Parameter error: Expected positive real value after --compactness (-co)\n";
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-un") || !strcmp(argv[i],"--uniformity")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%f",&p.soo_color) || p.soo_color<0 ) {
				cerr << "Parameter error: Expected positive real value after --uniformity (-un)\n";
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"--seg_only")) {
			p.only_seg=true;
		} else if(!strcmp(argv[i],"-o")) {
			i++;
			strcpy(p.output_filename, argv[i]);
		} else if( !strcmp(argv[i],"-p") ) {
			p.do_polygonize=true;
		} else if( !strcmp(argv[i],"-n") ) {
			p.normalize=true;
		} else {
			strcpy(p.input_filename, argv[i]);
		}
	}
	
	//Generate a output filename from input filename if it's not provided by the user
	if( strlen(p.output_filename)==0 ) {
		int last_dot_pos=strlen(p.input_filename);
		while( last_dot_pos!=0 && p.input_filename[last_dot_pos]!='.') last_dot_pos--;
		if( last_dot_pos==0 ) {
			cerr << "Parameter error: unexpected input filename (add the extension .tif to the filename)\n";
			usage( argv[0] );
			exit( EXIT_FAILURE );
		}
		strncpy(p.output_filename, p.input_filename,last_dot_pos);
		strcat (p.output_filename,"_out.tif");
	}
	
	//lancement des traitements
	GByte *output_image_class=process(p);
	if( !output_image_class ) {
		cerr << "Error while processing" <<endl;
		free_params(p);
		exit( EXIT_FAILURE );
	}
	
	save_class_images(output_image_class, p);
	
	free_params(p);
	
	return EXIT_SUCCESS;
}


GByte * process(params &p) {
	if( p.verbose ) {
		cout << "Start process " << p.input_filename <<endl;
		cout << "Processing params:" <<endl;
		cout << "======= PARAMS =======" <<endl;
		cout << "\tscale:" << p.soo_scale<<endl;
		cout << "\tcompactness:" << p.soo_compactness<<endl;
		cout << "\tcolor (alpha):" << p.soo_color<<endl;
		cout << "\t#class:" << p.nb_class<<endl;
		cout << "======================" <<endl;
		cout << "\t normalisation " << (p.normalize?"yes":"no") << endl;
		cout << "\t create polygons " << (p.do_polygonize?"yes":"no") << endl;
		cout << "======================" <<endl;
	}
	GDALDatasetH  hDataset;
	GDALAllRegister();	//register raster formats

	hDataset = GDALOpen( p.input_filename, GA_ReadOnly );
	if( hDataset == NULL ) {
		cerr << "Error: Invalid input file" <<endl;
		return NULL;
	}

	if(p.b==0) { //do not overwrite predifined number of bands
		p.b=GDALGetRasterCount( hDataset );
	}
	p.w=GDALGetRasterXSize( hDataset );
	p.h=GDALGetRasterYSize( hDataset );
	GDALGetGeoTransform(hDataset, p.adfGeoTransform);
	
	
	GDALRasterBandH lBand = GDALGetRasterBand( hDataset, 1 );
	if( p.verbose ) {
		cout << "======= IMAGE =======" <<endl;
		cout << "\tsize:" << p.w <<"x"<<p.h<<endl;
		cout << "\tb:" << p.b << endl;
		cout << "\t#date:" << p.nbd << endl;
		cout << "\t#time-series (dimensions):" << (p.b/p.nbd) << endl;
		cout << "\tdata type: " << GDALGetDataTypeName(GDALGetRasterDataType(lBand))<<endl;
		cout << "=====================" <<endl;
	}
	
	if( p.b%p.nbd != 0 ) {
		cerr << "ERROR : invalid number of dates (" << p.nbd <<"), didn't divide the number of bands in the image ("<< p.b<<").\n"<<endl;
		return NULL;
	}

	//Segmentation initialisation
	struct segment **segments_ptr_vector = NULL;
	struct segment *initial_segment = NULL;
	struct segment *final_segment = NULL;
	struct segmentation_parameters seg_parameters;

	seg_parameters.sp = p.soo_scale;
	seg_parameters.wcmpt = p.soo_compactness;
	seg_parameters.wcolor = p.soo_color;
	seg_parameters.bands = p.b;
	seg_parameters.wband = (float*)malloc(sizeof(float)*p.b);
	
	//weights
	for (int i=0; i<p.b; i++)
		seg_parameters.wband[i] = 1;
	
	/**
	 * Les données sont organisées par date, puis par numéro de bande.
	 * La couche n correspond donc à la date n/p.nbd et à la bande n%p.nbd.
	 */
	float **float_input_image = (float **) malloc( p.b*sizeof(float*) ); //autant de couches que de bandes
	if( float_input_image==NULL) {
		cerr << "Error: lake of memory (1)" <<endl;
		return NULL;
	}
	for (int i=0; i<p.b; i++) {
		float_input_image[i] = (float *) malloc( p.h*p.w*sizeof(float) );
		if( float_input_image[i]==NULL) {
			cerr << "Error: lake of memory (2), layer "<< i <<endl;
			return NULL;
		}
	}
	
	if( p.verbose ) {
		cout << "Memory allocated successfully" <<endl;
	}
	
	float *max_bands = (float*)  malloc( sizeof(float)*p.nbd );
	float *min_bands = (float*)  malloc( sizeof(float)*p.nbd );
	for(int k=0;k<p.nbd;k++) {
		max_bands[k]=-MAX_FLT;
		min_bands[k]=MAX_FLT;
	}
	//Check the data type of the first layer
	if( GDALGetRasterDataType(lBand)==GDT_Float32 ) {
		GDALRasterBandH hBand;
		float *pafScanline = (float *) CPLMalloc( sizeof(float)*p.w*p.h );
		for (int k=0; k<p.b; k++) { //k is the band number
			hBand = GDALGetRasterBand( hDataset, k+1 );
			GDALRasterIO( hBand, GF_Read, 0, 0, p.w, p.h, pafScanline, p.w, p.h, GDT_Float32, 0, 0 );
			for (int px=0; px<p.w*p.h; px++) { // px is the pixel id
				float_input_image[k][px] = (float) pafScanline[px];
				max_bands[ k%p.nbd ] = (max_bands[ k%p.nbd ]<float_input_image[k][px]?float_input_image[k][px]:max_bands[ k%p.nbd ]);
				min_bands[ k%p.nbd ] = (min_bands[ k%p.nbd ]>float_input_image[k][px]?float_input_image[k][px]:min_bands[ k%p.nbd ]);
			}
		}
		CPLFree(pafScanline);
	} else if( GDALGetRasterDataType(lBand)==GDT_Int16 ){
		GDALRasterBandH hBand;
		short *pafScanline = (short*) CPLMalloc( sizeof(short)*p.w*p.h );
		for (int k=0; k<p.b; k++) { //k is the band number
			hBand = GDALGetRasterBand( hDataset, k+1 );
			GDALRasterIO( hBand, GF_Read, 0, 0, p.w, p.h, pafScanline, p.w, p.h, GDT_Int16, 0, 0 );
			for (int px=0; px<p.w*p.h; px++) { // px is the pixel id
				float_input_image[k][px] = (float) pafScanline[px];
				max_bands[ k%p.nbd ] = (max_bands[ k%p.nbd ]<float_input_image[k][px]?float_input_image[k][px]:max_bands[ k%p.nbd ]);
				min_bands[ k%p.nbd ] = (min_bands[ k%p.nbd ]>float_input_image[k][px]?float_input_image[k][px]:min_bands[ k%p.nbd ]);
			}
		}
		CPLFree(pafScanline);
	} else if( GDALGetRasterDataType(lBand)==GDT_Byte ){
		GDALRasterBandH hBand;
		char *pafScanline = (char*) CPLMalloc( sizeof(char)*p.w*p.h );
		for (int k=0; k<p.b; k++) { //k is the band number
			hBand = GDALGetRasterBand( hDataset, k+1 );
			GDALRasterIO( hBand, GF_Read, 0, 0, p.w, p.h, pafScanline, p.w, p.h, GDT_Byte, 0, 0 );
			for (int px=0; px<p.w*p.h; px++) { // px is the pixel id
				float_input_image[k][px] = (float) pafScanline[px];
				max_bands[ k%p.nbd ] = (max_bands[ k%p.nbd ]<float_input_image[k][px]?float_input_image[k][px]:max_bands[ k%p.nbd ]);
				min_bands[ k%p.nbd ] = (min_bands[ k%p.nbd ]>float_input_image[k][px]?float_input_image[k][px]:min_bands[ k%p.nbd ]);
			}
		}
		CPLFree(pafScanline);
	} else {
		cerr << "Error: invalid file data format (must be Float32, Int16 or Byte)" <<endl;
		//free(float_input_image);
		return(NULL);
	}


	if( p.verbose ) {
		cout << "Image loaded succesfully" <<endl;
	}
	
	if( p.normalize ) {
		if( p.verbose ) {
			cout << "Start NORMALISATION" <<endl;
		}
		for (int k=0; k<p.b; k++) { //k is the band number
			float maxb=max_bands[ k%p.nbd ];
			float minb=min_bands[ k%p.nbd ];
			for (int px=0; px<p.w*p.h; px++) { // px is the pixel id
				float_input_image[k][px] = (float_input_image[k][px]-minb)/(maxb-minb);
			}
		}
	}
	

	if( p.verbose ) {
		cout << "Start SEGMENTATION" <<endl;
	}
	int  progress_enabled=0;
	initialize_segments(&segments_ptr_vector, &initial_segment, &final_segment, float_input_image, p.h, p.w, p.b, progress_enabled);

	if( p.verbose ) {
		cout << "\tinitial segments have been succesfully created" <<endl;
	}
	
#if REVERSE_PROCESS>0
	if( p.verbose ) {
		cout << "\tno segmentation here" <<endl;
	}
#else
	//segmentation(segments_ptr_vector, initial_segment, final_segment, p.h, p.w, seg_parameters, progress_enabled);

	if( p.verbose ) {
		cout << "\tfinished" <<endl;
	}
#endif
	
	if( p.only_seg ) {
		//output 
		GByte *output_image = (GByte *) CPLMalloc(sizeof(GByte)*p.w*p.h);
		write_segments(output_image, p.w, p.h, p.b, initial_segment, 0, progress_enabled);
		return output_image;
	}

	//Charger les segments dans le classifieur
	if( p.verbose ) {
		cout << "Start CLASSIFICATION" <<endl;
	}
	Classification classifieur;

	if( p.verbose ) {
		cout << "\tloading data time series ..." <<endl;
	}
	classifieur.add_timeseries(initial_segment, p.b, p.nbd);
	

	//Classer les segments !
	if( p.verbose ) {
		cout << "\tstarting classification ..." <<endl;
	}
	classifieur.classif(p.nb_class, p.verbose);
	if( p.verbose ) {
		cout << "\tfinished" <<endl;
	}
	
	classifieur.save_centroids("centroids.csv"); //TG ajouter un parametre !
	//classifieur.save_ts("ts.csv"); //TG ajouter un parametre !
	

#if REVERSE_PROCESS>0
	if( p.verbose ) {
		cout << "Start SEGMENTATION" <<endl;
		cout << "\tpropagate centroids on the image" <<endl;
	}
	classifieur.propagate_centroids();

	if( p.verbose ) {
		cout << "\tdo image segmentation" <<endl;
	}

	segmentation(segments_ptr_vector, initial_segment, final_segment, p.h, p.w, seg_parameters, progress_enabled);

	if( p.verbose ) {
		cout << "\tfinished" <<endl;
	}

	//output
	if( p.verbose ) {
		cout << "Output segmentation image" <<endl;
	}
	GByte *output_image = (GByte *) CPLMalloc(sizeof(GByte)*p.w*p.h);
	write_segments(output_image, p.w, p.h, p.b, initial_segment, 1/*mean value of the first dimension*/, progress_enabled);
	return output_image;

#else
	//output
	if( p.verbose ) {
		cout << "Output classification image" <<endl;
	}
	GByte *output_image_class = (GByte *) CPLMalloc(sizeof(GByte)*p.w*p.h);
	classifieur.write_class(output_image_class);

	free_segment_list(&initial_segment, &segments_ptr_vector);
	
	return output_image_class;
#endif

}


/**
* save a raster and a shapefile
*/
void save_class_images(GByte *output_image_class, params &p) {
	
	const char *pszFormat = "GTiff";
	GDALDriverH hDriver = GDALGetDriverByName( pszFormat );
	char **papszOptions = NULL;
	GDALDatasetH hDstDS = GDALCreate( hDriver, p.output_filename, p.w, p.h, 1, GDT_Byte, papszOptions );

	GDALSetGeoTransform(hDstDS, p.adfGeoTransform );
	//TODO set SRS !!

	if( hDstDS == NULL ) {
		GDALClose( hDstDS );
		exit(1);
	}

	GDALRasterBandH outputBand = GDALGetRasterBand( hDstDS, 1 );
	GDALRasterIO( outputBand, GF_Write, 0, 0, p.w, p.h, output_image_class, p.w, p.h, GDT_Byte, 0, 0 );


	/////////////// VECTORISATION ////////////////
	if( p.do_polygonize ) {
		OGRRegisterAll(); 	//register vector formats
		const char *pszDriverName = "ESRI Shapefile";
		OGRSFDriverH hVectorDriver;
		OGRDataSourceH hVectorDS;
		OGRLayerH hVectorLayer;
		OGRFieldDefnH hFieldDefn;
		
		hVectorDriver = OGRGetDriverByName( pszDriverName );
		if( hVectorDriver == NULL ) {
		    printf( "%s driver not available.\n", pszDriverName );
		    exit( 1 );
		}

		std::string str("");
		str+= p.output_filename;
		unsigned int found = str.find_last_of(".");
		if( found!=std::string::npos ) {
			//remove tif extension
			str=str.substr(0,found);
		}
		str+= ".shp";
		remove(str.c_str());
		hVectorDS = OGR_Dr_CreateDataSource( hVectorDriver, str.c_str(), NULL );
		if( hVectorDS == NULL ) {
		    printf( "Creation of output file failed.\n" );
		    exit( 1 );
		}

		hVectorLayer = OGR_DS_CreateLayer( hVectorDS, "output", NULL, wkbPolygon, NULL );
		if( hVectorLayer == NULL ) {
		    printf( "Layer creation failed.\n" );
		    exit( 1 );
		}
		
		//Create attribute to the layer
		hFieldDefn = OGR_Fld_Create( "Class", OFTInteger );
		if( OGR_L_CreateField( hVectorLayer, hFieldDefn, TRUE ) != OGRERR_NONE ) {
		    printf( "Creating Name field failed.\n" );
		    exit( 1 );
		}
		OGR_Fld_Destroy(hFieldDefn);
		
		//polygonize outputBand
		GDALPolygonize(outputBand, NULL/*mask*/, hVectorLayer, 0 /*iPixValField*/, papszOptions, NULL, NULL);

		OGR_DS_Destroy( hVectorDS );
	}
	///////////////////////////////////////////////////////

    /* Once we're done, close properly the dataset */
    GDALClose( hDstDS );
}


/**
 * \brief Fonction d'information sur l'usage du programme
 */
void usage(const char *comm)
{
	printf("*******************\n");
	printf("Segmentation de SITS basée sur des objets\n");
	printf("Year : 2013\n");
	printf("*******************\n");
	printf("*******************\n\n");
	printf("command : %s  [-c %%d] [-s %%f] [-co %%f] [-un %%f] [-o %%s] [-v] [-p] inputfile\n\n", comm);
	printf("Options :\n");
	printf("\t -c : # of class in the classification (default 15)\n");
	printf("\t -d : # of dates that characterize the time series (must divide the number of bands) (default 1)\n");
	printf("\t -n : normalize the data set per band\n");
	printf("\t -s : scale segmentation parameter (default 200)\n");
	printf("\t -co : compactness segmentation parameter (default 0.5)\n");
	printf("\t -un : uniformity segmentation parameter (default 0.5)\n");
	printf("\t -o : output file name (default adapt the input filename)\n");
	printf("\t -v : verbose (you may define a verbose level) (default none)\n");
	printf("\t -p : create a shapefile with regions (default none)\n");
	printf("\t --seg_only : perform only segmentation (default false)\n");
	printf("\t inputfile : GDAL standard raster image (HDR/GeoTiff). Dimensions are interwined with date. The layer are mainly organized by date: te layer at date d for band b (considering nb bands), the layer id is d*nb+b.\n");
}


