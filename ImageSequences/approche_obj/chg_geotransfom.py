from osgeo import gdal  
from osgeo.gdalconst import *  
import sys

ds = gdal.Open( "world.tif", GA_Update )
geotransform=[-180.0+1.0/24.0, 0, 0.0833, 90.0-1.0/24.0, -0.0833,0]
ds.SetGeoTransform(geotransform)
ds = None
 
