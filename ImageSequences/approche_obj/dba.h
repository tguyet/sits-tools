/*
 * dba.h
 *
 *  Created on: 8 juil. 2015
 *      Author: tguyet
 */

#ifndef DBA_H_
#define DBA_H_


/**
 * \param dataset time series dataset
 * \param l length of time series (same length in this implementation
 * \param n number of time series
 * \param nit number of iteration
 *
 * dataset[i][j] is the jth value in the ith time series
 */
double *dba(double **dataset, unsigned int l, unsigned int n, unsigned int nit =15);

#endif /* DBA_H_ */
