 /*
 * ImageSequences_DirectClustering.h
 *
 * This file is a part of the long-term SITS (Satellite Image Time Series) classification tool.
 * 
 * author: guyet
 * version: 2.0
 * last modification : 24/10/2013
 */

#ifndef IMAGESEQUENCES_DIRECT_H_
#define IMAGESEQUENCES_DIRECT_H_

#include "ImageSequences.h"
#include "HDR_Utilities.h"


int directclustering(images_params ip, params &pp);


#endif /* IMAGESEQUENCES_H_ */
