

#include "ImageSequences.h"
#include "HDR_Utilities.h"
#include "QFZSegmentation.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>


void usage(const char *comm);

int main(int argc, char **argv)
{
	//testQFZ(4);
	//exit(1);
	//testQFZMD();
	int *result=NULL;
	char outputname[50];
	outputname[0]=0;
	int datetag=0;

	//Paramètres de l'images
	images_params ip;
	init_images_params(&ip);

	//Paramètres de l'algorithme
	params p;
	init_params(&p);

	for(int i=1;i<argc;i++)
	{
		if( !strcmp(argv[i],"-h") ) {
			usage(argv[0]);
			return EXIT_SUCCESS;
		} else if(!strcmp(argv[i],"-geom")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%f",&ip.X0) || ip.X0<=0 ) {
				fprintf(stderr, "Parameter error: Expected 4 strictly positive integer after -geom\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%f",&ip.Y0) || ip.Y0<=0 ) {
				fprintf(stderr, "Parameter error: Expected 4 strictly positive integer after -geom\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%f",&ip.X1) || ip.X1<=0 ) {
				fprintf(stderr, "Parameter error: Expected 4 strictly positive integer after -geom\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%f",&ip.Y1) || ip.Y1<=0 ) {
				fprintf(stderr, "Parameter error: Expected 4 strictly positive integer after -geom\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
			if( ip.Y1 >ip.Y0) {
				float t=ip.Y1;
				ip.Y1=ip.Y0;
				ip.Y0=t;
			}
			if( ip.X0 >ip.X1) {
				float t=ip.X1;
				ip.X1=ip.X0;
				ip.X0=t;
			}

		} else if(!strcmp(argv[i],"-dt")) {
			datetag=1;
		}  else if(!strcmp(argv[i],"-o")) {
			i++;
			strcpy(outputname, argv[i]);
		} else {

			if( argv[i][0]=='-' ) {
				fprintf(stderr, "Parameter error: Unknown parameter %s\n", argv[i]);
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}

			//C'est un fichier à charger
			ip.hdrfilename = argv[i];

			//Récupération du chemin ...
			char *co=(char *)malloc( sizeof(char)*(strlen(ip.hdrfilename)+1) );
			if( co!=NULL ) {
				strcpy(co, argv[i]);
				char *lo=strrchr(co, '/');
				if( lo!=NULL ) {
				    *lo=0;
				    ip.input_basename=co;
			    }
			}
		}
	}

	//Construction d'un nom de fichier par défaut
	if( outputname[0]==0 ) {
		strcpy(outputname, "output");

		if(datetag) {
			time_t rawtime;
			struct tm * timeinfo;
			char strint[5];

			//Ajout d'un tag temporel sur le nom de fichier
			time ( &rawtime );
			timeinfo = localtime ( &rawtime );
			strcat(outputname, "_");
			sprintf(strint, "%d", timeinfo->tm_mday);
			strcat(outputname, strint );
			sprintf(strint, "%d", timeinfo->tm_mon);
			strcat(outputname, strint );
			strcat(outputname, "_");
			sprintf(strint, "%d", timeinfo->tm_hour);
			strcat(outputname, strint );
			sprintf(strint, "%d", timeinfo->tm_min);
			strcat(outputname, strint );
		}

		//Creation d'un repertoire de travail specifique
		char cmd[250];
		sprintf(cmd, "mkdir %s",outputname );
		int ret=system(cmd);
		if(ret) {
			fprintf(stderr, "Impossible de créer le répertoire %s\n", outputname);
			//exit(1);
		}
	}

	ip.output_basename=outputname;


	p.use_invalidpixels=false;
	ip.startyear=2001;
	ip.endyear=2002;
	ip.band_per_year=23;


	printf("********************\nLOAD IMAGE\n********************\n");
	float **image=load_data(ip, &p);
	printf("********************\n");

	float alpha=10;
	float alphastep=100;

	printf("********************\nSTART QFZ SEGMENTATION \n********************\n");
	printf("Params:\n");
	printf("\t alpha=%f\n", alpha);
	printf("\t alphastep=%f\n", alphastep);
	printf("********************\n");
	vector<CCd> concomp;
	result = QFZMD(image, p.sx, p.sy, ip.band_per_year, alpha, concomp, alphastep, 2);
	printf("********************\n");

	printf("********************\nSAUVEGARDE IMAGE FINALE\n********************\n");
	save_hdr_decade("output_qfz", p, result);

	if(result) free(result);
	return 0;
}

void usage(const char *comm)
{
  printf("*******************\n");
}
