/*
 * ImageSequences.cpp
 *
 * This file is a part of the long-term SITS (Satellite Image Time Series) classification tool.
 * 
 * author: guyet
 * version: 2.0
 * last modification : 24/10/2013
 */

#include "ImageSequences.h"
#include <stdlib.h>
#include <string.h>
#include "HDR_Utilities.h"
#include <list>
#include <map>
#include <vector>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <float.h>
#include <math.h>

extern "C" {
#include "cluster.h"
#include "apcluster.h"
#include "Matrix_Utils.h"
#include "train.h"
}


using namespace std;


// callback function; returning non-zero will abort apcluster

/*
int callback(float *curr_a, float *curr_r, int N, int *curr_idx, int I, float curr_netsim, int iter) {
	if(curr_netsim==-HUGE_VAL) printf("%d\t%s\n", iter, "NaN");
	else printf("%d\t%f\n", iter, curr_netsim);
	return(0);
}
*/

void init_params(params *p) {
	p->n_annualprofiles=30;
	p->n_decade=200;
	p->n_successions=6000;
	p->s=-2;

	p->nb_pixels=0;
	p->sample_rate=1;
	p->sample_rate_dec=1;
	p->reuse_clusteringid=false;
	p->nb_processing_parts=5;

	p->clustering_method = EM_CLUSTERING;

	p->kmeans_maxiter =50;
	p->kmedoid_maxiter =50;
	p->ap_convit = 20;
	p->ap_maxit = 200;
	p->succ_ok = 0;

	//Autres paramètres qui seront à définir plus tard
	p->sx=0;
	p->sy=0;
	memset(p->adfGeoTransform, 0, sizeof(double)*6);

	p->annee=NULL;
	p->ny=0;

	p->use_invalidpixels = true;

	p->attribution_error=0;
}

void free_images_params(images_params p) {
	if(p.hdrfilename) free(p.hdrfilename);
	if(p.input_basename) free(p.input_basename);
}

void init_images_params(images_params *p) {
	p->X0=0;
	p->Y0=0;
	p->X1=0;
	p->Y1=0;
	p->startyear=2001;
	p->endyear=2004;

	//Nom du fichier
	p->hdrfilename = (char*)malloc(sizeof(char)*200);

	//Paramètres spécifique à l'organisation des données d'Abdou :
/*
	p->firstyear=2001;
	p->band_per_year=23;
	p->start_band=230;
	p->band_order=-1;
	p->year_order=-1;
	strcpy(p->hdrfilename, "/home/guyet/Recherche/Donnees/SequenceImages/Layer_Staking_NDVI_UTM_250m_2000_2010");
*/
	
	//Paramètres spécifiques aux données de Cherif :
	//p->startyear=2009;
	//p->endyear=2010;
/*
	p->firstyear=2009;
	p->band_per_year=22;
	p->start_band=1;
	p->band_order=1;
	//
	p->firstyear=2005;
	p->band_per_year=21;
	p->start_band=1;
	p->band_order=1;
*/

	//Paramètres spécifiques aux données par pythontools NDIV/EVI
	p->firstyear=2001;
	p->band_per_year=23;
	p->start_band=1;
	p->band_order=1;
	p->year_order=1;
	
	strcpy(p->hdrfilename, "image.tif");
	

	p->output_basename = NULL;
	p->input_basename = NULL;
}


/*
 * \brief Détermine la meilleure classe pour des données data.
 * \param data vecteur de données
 * \param ip paramètres de l'image
 * \param p paramètres des algorithmes
 * \param profiles Définition des profils annuels associés aux classes
 * \param error (out) permet de récupérer l'erreur commise lors de l'attribution de la classe
 * \return Un numero de classe ou -1 si les données ne sons pas valides.
 *
 * La classe qui est sélectionnée est celle qui est la plus proche en distance (euclidienne)
 * des données.
 * La fonction retourne -1 si les données contiennent que des NaN. Pour les autres vecteurs, les NaN
 * sont des valeurs ignorés dans le calcul de la distance
 *
 * \pre data doit être de la taille du nombre de bande (ip.band_per_year)
 */
int get_class(float *data, images_params ip, params p, float** profiles, float *error = NULL)
{
	//Un pixel est invalide s'il ne contient que des NaN, sinon il est classé !
	int invalid=1;
	for(int k=0; k<ip.band_per_year; k++) {
		if( !isnan(data[k]) ) {
			invalid=0;
		}
	}
	if(invalid) return -1;

	float dist, r;
	float mindist=FLT_MAX;
	int argmin=-1;
	for(int i=0; i<p.n_annualprofiles;i++) {
		dist =0.0;
		for(int k=0; k<ip.band_per_year; k++) {
			if( isnan(data[k]) ) continue; //On passe les valeurs invalides !
			r=profiles[i][k]-data[k];
			dist+=r*r;
		}
		if( dist<mindist) {
			mindist=dist;
			argmin=i;
		}
	}

	assert(argmin!=-1);

	if(error!=NULL) *error = mindist;
	return argmin;
}


/**
 * \brief Fonction pour construire un vecteur des classes des profils annuelles
 * \param result (in/out) matrice dont il faut compléter des valeurs avec des classes.
 * result contient les informations de tous les pixels pour toutes les années (les unes
 * à la suite des autres).
 * \param ip (in) paramètres de l'image
 * \param p (in) paramètres
 * \param decal (in) valeur de décalage (doit être entre 0 et p>-sample_rate-1)
 * \param profiles[i][j] : profiles annuels, i est le numéro du profil, j correspond à la bande (entre 0 et ip->band_per_year)
 * \param globalerror (out) retourne l'erreur totale commise lors de l'attribution des classes
 *
 * La fonction prends en compte les pixels invalides (que des NaN) en remplissant result avec -1.
 * Les données manquantes (NaN dans un vecteur valide) sont égalements traités grâce à la fonction get_class().
 * La taille de result est donc nécessairement p.sx*p.sy*p.ny !!
 *
 */
void complete_annually(int *result, images_params ip, params p, int year, float** profiles, float *globalerror =NULL) {
	list<int>::iterator ipit;
	float error=0.0;
	float *pixel_profile;

	if( globalerror!=NULL ) *globalerror=0.0;

	// Chargement des données d'une année
	int nbparts=4;
	for(int part=0; part<nbparts-1; part++) {
		int nbli;
		float **data=load_annual_data_part(ip, p, year, part, nbparts, &nbli);

		if( !data ) {
			cerr << "error in complete : data as not been correctly loaded certainly due to a lack of memory" << endl;
			return;
		}

		// yresult indique le début de zone de result à remplir.
		// Il y a deux décalages :
		//		- le décalage pour faire correspondre à la bonne année,
		// 		- le décalage pour faire correspondre à la bonne partie de l'année !
		//	HACK !!ATTENTION!! au mode de calcul !! Il faut forcer la division par une parenthèse pour éviter un
		//	débordement qui induit des décalages dans l'image reconstruite !!
		int *yresult=result + (p.sx*p.sy)*year + part*p.sx*(p.sy/nbparts);

		int val=(p.sx*p.sy)*year + part*p.sx*(p.sy/nbparts);

		for(int i=0; i < p.sx*nbli; i++) {
			pixel_profile = data[i];

			//Récupération de la classe du pixel (et attribution)
			*yresult=get_class(pixel_profile, ip, p, profiles, &error);

			if( globalerror!=NULL ) (*globalerror)+=error;
			yresult++;
		}

		printf("done from %d to %d\n", val, val+p.sx*(p.sy/nbparts));

		//Libération de la mémoire
		free_matrix(data, nbli);
	}//for part

	//On finit par le dernier batch (de taille possiblement différente)
	{
		int part=nbparts-1;
		int nbli= p.sy - ( part*(p.sy/nbparts) ); //< nombre de ligne du dernier batch
		float **data=load_annual_data_part(ip, p, year, part, nbparts, &nbli);

		if( !data ) {
			cerr << "error in complete : data as not been correctly loaded certainly due to a lack of memory" << endl;
			return;
		}

		int *yresult=result + (p.sx*p.sy)*year + part*p.sx*(p.sy/nbparts);

		int val=(p.sx*p.sy)*year + part*p.sx*(p.sy/nbparts);

		for(int i=0; i < p.sx*nbli; i++) {
			pixel_profile = data[i];

			//Récupération de la classe du pixel (et attribution)
			*yresult=get_class(pixel_profile, ip, p, profiles, &error);

			if( globalerror!=NULL ) (*globalerror)+=error;
			yresult++;
		}

		printf("done last from %d to %d\n", val, val+p.sx*nbli);

		//Libération de la mémoire
		free_matrix(data, nbli);
	}
}


typedef struct {
	int *result;
	images_params ip;
	params p;
	int year;
	int part;
	int nbparts;
	gmm *model;
	profile *profiles;
	workers_mutex profile_mutex;
	float **data;
	int nbli;
} data_threadcompleteem;

void thread_complete_annually_em(void *tdata) {
	data_threadcompleteem *t=(data_threadcompleteem*)tdata;

	if( !t->data ) {
		cerr << "error in complete 1 : data as not been correctly loaded certainly due to a lack of memory" << endl;
		return;
	}

	int *cid= classify_em(t->model, t->data, t->ip.band_per_year, t->p.sx*t->nbli);

	// yresult indique le début de zone de result à remplir.
	// Il y a deux décalages :
	//		- le décalage pour faire correspondre à la bonne année,
	// 		- le décalage pour faire correspondre à la bonne partie de l'année !
	//	HACK !!ATTENTION!! au mode de calcul !! Il faut forcer la division par une parenthèse pour éviter un
	//	débordement qui induit des décalages dans l'image reconstruite !!
	int *yresult=t->result + (t->p.sx*t->p.sy)*t->year + t->part*t->p.sx*(t->p.sy/t->nbparts);
	int *cl=cid;
	for(int i=0; i < t->p.sx*t->nbli; i++) {
		*yresult=*cl;//Récupération de la classe du pixel


		pthread_mutex_lock(&t->profile_mutex);
		t->profiles[*yresult].nb++;

		//mise à jour du profil correspondant
		for(int d=0; d<t->ip.band_per_year; d++) {
			t->profiles[*yresult].mean[d] = t->profiles[*yresult].mean[d]+t->data[i][d];
			t->profiles[*yresult].envmin[d] = (t->data[i][d]<t->profiles[*yresult].envmin[d]?t->data[i][d]:t->profiles[*yresult].envmin[d]);
			t->profiles[*yresult].envmax[d] = (t->data[i][d]>t->profiles[*yresult].envmax[d]?t->data[i][d]:t->profiles[*yresult].envmax[d]);
		}
		pthread_mutex_unlock(&t->profile_mutex);

		cl++;
		yresult++;
	}
	//Libération de la mémoire
	free_matrix(t->data, t->ip.band_per_year);
}

//version en thread_launcher
void complete_annually_em_thl(int *result, images_params ip, params p, gmm *model, profile *profiles) {
	int nbparts=4;

	number t=sysconf(_SC_NPROCESSORS_ONLN);
	workers *pool = workers_create(t);

	data_threadcompleteem *tdata=(data_threadcompleteem *)calloc(sizeof(data_threadcompleteem), nbparts*p.ny);

	for(int year=0; year<p.ny; year++) {
		for(int part=0; part<nbparts; part++) {
			tdata[part+year*nbparts].result=result;
			tdata[part+year*nbparts].ip=ip;
			tdata[part+year*nbparts].p=p;
			tdata[part+year*nbparts].year=year;
			tdata[part+year*nbparts].part=part;
			tdata[part+year*nbparts].nbparts=nbparts;
			tdata[part+year*nbparts].model=model;
			tdata[part+year*nbparts].profiles=profiles;
			tdata[part+year*nbparts].data=load_annual_data_part(ip, p, year, part, nbparts, &(tdata[part+(p.ny-1)*nbparts].nbli));

			if( !tdata[part+year*nbparts].data ) {
				cerr << "error in complete 6 : data as not been correctly loaded certainly due to a lack of memory" << endl;
				return;
			}

			workers_addtask(pool,thread_complete_annually_em,(void*)(&tdata[part+year*nbparts]));
		}
	}
	workers_waitall(pool); /* Wait to the end of the parallel tasks. */
	workers_finish(pool);
}

void complete_annually_em(int *result, images_params ip, params p, int year, gmm *model, profile *profiles) {

	// Chargement des données d'une année
	int nbparts=4;
	for(int part=0; part<nbparts-1; part++) {
		int nbli;
		float **data=load_annual_data_part(ip, p, year, part, nbparts, &nbli);

		if( !data ) {
			cerr << "error in complete : data as not been correctly loaded certainly due to a lack of memory" << endl;
			return;
		}

		int *cid= classify_em(model, data, ip.band_per_year, p.sx*(p.sy/nbparts));

		// yresult indique le début de zone de result à remplir.
		// Il y a deux décalages :
		//		- le décalage pour faire correspondre à la bonne année,
		// 		- le décalage pour faire correspondre à la bonne partie de l'année !
		//	HACK !!ATTENTION!! au mode de calcul !! Il faut forcer la division par une parenthèse pour éviter un
		//	débordement qui induit des décalages dans l'image reconstruite !!
		int *yresult=result + (p.sx*p.sy)*year + part*p.sx*(p.sy/nbparts);
		int *cl=cid;
		for(int i=0; i < p.sx*nbli; i++) {
			*yresult=*cl;//Récupération de la classe du pixel
			profiles[*yresult].nb++;

			//mise à jour du profil correspondant
			for(int d=0; d<ip.band_per_year; d++) {
				profiles[*yresult].mean[d] = profiles[*yresult].mean[d]+data[i][d];
				profiles[*yresult].envmin[d] = (data[i][d]<profiles[*yresult].envmin[d]?data[i][d]:profiles[*yresult].envmin[d]);
				profiles[*yresult].envmax[d] = (data[i][d]>profiles[*yresult].envmax[d]?data[i][d]:profiles[*yresult].envmax[d]);
			}
			cl++;
			yresult++;
		}

		int val=(p.sx*p.sy)*year + part*p.sx*(p.sy/nbparts);
		printf("done from %d to %d\n", val, val+p.sx*(p.sy/nbparts));

		//Libération de la mémoire
		free_matrix(data, ip.band_per_year);
	}//for part

	//On finit par le dernier batch (de taille possiblement différente)
	{
		int part=nbparts-1;
		int nbli= p.sy - ( part*(p.sy/nbparts) ); //< nombre de ligne du dernier batch
		float **data=load_annual_data_part(ip, p, year, part, nbparts, &nbli);

		if( !data ) {
			cerr << "error in complete : data as not been correctly loaded certainly due to a lack of memory" << endl;
			return;
		}

		int *cid= classify_em(model, data, ip.band_per_year, p.sx*nbli);
		int *yresult=result + (p.sx*p.sy)*year + part*p.sx*(p.sy/nbparts);
		int *cl=cid;
		for(int i=0; i < p.sx*nbli; i++) {
			*yresult=*cl;//Récupération de la classe du pixel
			profiles[*yresult].nb++;

			//mise à jour du profil correspondant
			for(int d=0; d<ip.band_per_year; d++) {
				profiles[*yresult].mean[d] = profiles[*yresult].mean[d]+data[i][d];
				profiles[*yresult].envmin[d] = (data[i][d]<profiles[*yresult].envmin[d]?data[i][d]:profiles[*yresult].envmin[d]);
				profiles[*yresult].envmax[d] = (data[i][d]>profiles[*yresult].envmax[d]?data[i][d]:profiles[*yresult].envmax[d]);
			}
			cl++;
			yresult++;
		}

		int val=(p.sx*p.sy)*year + part*p.sx*(p.sy/nbparts);
		printf("done last from %d to %d\n", val, val+p.sx*nbli);

		//Libération de la mémoire
		free_matrix(data, ip.band_per_year);
	}
}


/**
 * \brief Prend 1 valeur de clusterid tous les ny à partir de start
 * \param clusterid vecteur
 * \param positions
 * \
 */
void transform(int *clusterid, vector< int > &positions, int sx, int sy, int *result, int ny, int start)
{
	vector< int >::iterator it=positions.begin();
	int currentpos=0;
	int *pcp=result;
	int currentid=start;
	while( it!=positions.end() ) {
		while(currentpos != *it) {
			*pcp=-1;
			currentpos++;
			pcp++;
		}
		*pcp=clusterid[currentid];
		currentpos++;
		currentid+=ny;
		pcp++;
		it++;
	}
	while(currentpos != sx*sy) {
		*pcp=-1;
		currentpos++;
		pcp++;
	}
}

/**
 * \param clusterid vecteur avec les identifiants de clusters
 * \param ncid taille du vecteur clusterid
 * \param p paramètres des algo
 * \param positions positions inverses dans l'image
 */
int *transform_clustersid(int *clusterid, int ncid, params p, vector< int > &positions) {

	//Allocation de toute la matrice
	int *result=(int *)malloc( sizeof(int)*p.sx*p.sy*p.ny );
#ifndef NDEBUG
	for(int i=0; i<p.sx*p.sy*p.ny; i++ ) {
		result[i]=0;
	}
#endif

	if(result==NULL) {
		cerr << "transform result allocation error" <<endl;
		return NULL;
	}

	for(int ny=0; ny<p.ny; ny++ ) {
		//int *ycid=clusterid + nycid*ny;
		int *yresult=result+(p.sx*p.sy)*ny;
		//On remplit les résultats à partir de yresult pour l'année ny
		transform(clusterid, positions, p.sx, p.sy, yresult, p.ny, ny);
	}
	return result;
}


/**
 * \brief Calcule la matrice des distances entre profils annuels (distance euclidienne)
 * \param annual_profiles profiles annuels annual_profiles[i] est le i-eme profile (vecteur de taille band_per_year)
 * \param ncluster nombre de profils annuels (taille de  annual_profiles)
 * \param band_per_year dimension d'un profil
 * \param cdist (out) matrice de distances
 */
void annualprofiles_distances_construction(float **annual_profiles, int nclusters, int band_per_year, float **cdist)
{
	float dist, d;

	for(int i=0; i<nclusters; i++) {
		cdist[i][i]=0.0;
		for(int j=i+1; j<nclusters; j++) {
			dist=0;
			for(int k=0; k<band_per_year; k++) {
				d= (annual_profiles[i][k]-annual_profiles[j][k]);
				dist+=d*d;
			}
			cdist[i][j]=dist;
			cdist[j][i]=dist;
		}
	}
}


float dist(int *decade1, int *decade2, int ny, float **cdist, int nmatrix)
{
	float d=0;
	if( decade1[0]==-1 || decade2[0]==-1) {
		for(int i=0; i<ny; i++) {
			if( decade1[i]!=-1 || decade2[i]!=-1) {
				//Cas d'un pixel avec -1 seul qq part!!
				return FLT_MAX;
			}
		}
		//Ils sont tous -1 : Cas d'un pixel vide
		return 0;
	} else {
		for(int i=0; i<ny; i++) {
			if( decade1[i]==-1 || decade2[i]==-1) {
				//Cas d'un pixel avec -1 seul qq part!!
				continue;
				//return FLT_MAX;
			}
			if( decade1[i]!=decade2[i] ) {
				d+=cdist[ decade1[i] ][ decade2[i] ];
			}
		}
	}
	return d;
}

void save_profiles(const char *filename, float **mat, int nrows, int ncols)
{
	FILE * fout=fopen(filename, "w+");
	if( fout == NULL)
		return;
	fprintf(fout, "%d %d\n", nrows, ncols);
	for(int i=0; i<nrows; i++) {
		for(int j=0; j<ncols; j++)
			fprintf(fout,"%lf ", mat[i][j]);
		fprintf(fout, "\n");
	}

	fclose(fout);
}

void save_profiles(const char *filename, profile *profiles, int nprof, int nband_per_year)
{
	FILE * fout=fopen(filename, "w+");
	if( fout == NULL)
		return;
	//fprintf(fout, "%d %d (with enveloppe)\n", nprof, nband_per_year);
	for(int i=0; i<nprof; i++) {
		for(int j=0; j<nband_per_year; j++)
			fprintf(fout,"%lf ", profiles[i].mean[j]);
		fprintf(fout, "\n");
		for(int j=0; j<nband_per_year; j++)
			fprintf(fout,"%f ", profiles[i].envmin[j]);
		fprintf(fout, "\n");
		for(int j=0; j<nband_per_year; j++)
			fprintf(fout,"%f ", profiles[i].envmax[j]);
		fprintf(fout, "\n");
		for(int j=0; j<nband_per_year; j++)
			fprintf(fout,"%lf ", profiles[i].std[j]);
		fprintf(fout, "\n");
	}

	fclose(fout);
}


void save_clustersdistances(const char *filename, float **mat, int nclusters)
{
	FILE * fout=fopen(filename, "w+");
	if( fout == NULL)
		return;
	fprintf(fout, "%d\n", nclusters);
	for(int i=0; i<nclusters; i++) {
		for(int j=0; j<nclusters; j++)
			fprintf(fout,"%lf ", mat[i][j]);
		fprintf(fout, "\n");
	}

	fclose(fout);
}

void save_successions(const char *filename, vector<int *> &successions, int *cor, int *idx, int *nbcl, int ny)
{
	FILE * fout=fopen(filename, "w+");
	if( fout == NULL)
		return;

	int s;
	for(int i=0; i<*nbcl; i++) {
		fprintf(fout, "Class %d :\n", i);
		s=0;
		for(vector<int *>::iterator it=successions.begin(); it!=successions.end(); it++, s++) {
			if( idx[s]==cor[i] ) {
				for(int j=0; j<ny; j++)
					fprintf(fout,"%d ", (*it)[j]);
				fprintf(fout,"\n");
			}
		}
	}

	fclose(fout);
}

void save_successions(const char *filename, int **successions, int *correspondance, int *idx, int nbelements, int nbcl, int ny)
{
	FILE * fout=fopen(filename, "w+");
	if( fout == NULL) return;

	//Construction d'une Hash_Map
	std::map<int, std::list<int*> > succession_hash;
	for(int i=0; i<nbelements; i++) {
		int found=false;
		//Suppression des doublons
		std::list<int*>::iterator it;
		for(it=succession_hash[ idx[i] ].begin();it!=succession_hash[ idx[i] ].end();it++) {
			if( !memcmp(*it, successions[i], sizeof(int)*ny ) ) {
				found=true;
			}
		}

		if(!found) {
			succession_hash[ idx[i] ].push_back( successions[i] );
		}
	}

	for(int i=0; i<nbcl; i++) {
		int proto_idx=correspondance[i];
		fprintf(fout, "Class %d (", i);
		for(int j=0; j<ny; j++) {
			if(proto_idx<0) {
				fprintf(fout,"-1 ");
			} else {
				fprintf(fout,"%d ", successions[proto_idx][j]);
			}
		}
		fprintf(fout, ") :\n");

		std::list<int*>::iterator it;
		for(it=succession_hash[ proto_idx ].begin();it!=succession_hash[ proto_idx ].end();it++) {
			int *succession=*it;
			for(int j=0; j<ny; j++)
				fprintf(fout,"%d ", succession[j]);
			fprintf(fout,"\n");
		}
	}
	fclose(fout);
}

float ** load_clustersdistances(const char *filename, int *nclusters)
{
	cout << "load clustersdistances file : "<< filename<<endl;
	FILE * fout=fopen(filename, "r");
	if( fout == NULL) {
		cerr << "load_clustersdistances : erreur d'ouverture du fichier " << filename <<  " !" << endl;
		return NULL;
	}
	if(!fscanf(fout, "%d", nclusters)) {
		cerr << "load_clusterdistances error !" <<endl;
		return NULL;
	}
	float **mat=alloc_matrix_float(*nclusters, *nclusters);
	for(int i=0; i<*nclusters; i++) {
		for(int j=0; j<*nclusters; j++) {
			if( !fscanf(fout,"%f ", &mat[i][j]) )
				cerr << "load_clusterdistances error !" <<endl;
			//cout << mat[i][j] << endl;
		}
	}

	fclose(fout);
	return mat;
}

float succ_dist(int *decade1, int *decade2, int dim, float **clustersdistances, int nbc )
{
	float dist=0.0;
	float max=0;
	for(int k=0; k<nbc; k++) {
		for(int l=0; l<nbc; l++) {
			max=(max<clustersdistances[k][l]? clustersdistances[k][l]: max);
		}
	}

	for(int h=0; h<dim; h++) {
		if( (*decade1)<0 || (*decade2)<0) {
			dist+=max;
			decade1++;
			decade2++;
		} else {
			dist+=clustersdistances[ *(decade1++) ][ *(decade2++) ];
		}
	}
	return dist;
}

float **local_clustersdistances;
int local_dim;
float local_max;
float local_min;

/**
 * \warning ne pas utiliser sur des vecteurs NaN (que des -1 !!)
 */
float local_succ_dist(int *decade1, int *decade2)
{
	float dist=0.0;

	/*
	int d1_empty=true;
	for(int h=0; h<local_dim; h++) {
		if( decade1[h]!=-1 ) {
			d1_empty=false;
			break;
		}
	}
	int d2_empty=true;
	for(int h=0; h<local_dim; h++) {
		if( decade2[h]!=-1 ) {
			d2_empty=false;
			break;
		}
	}
	if( d1_empty || d2_empty ) {
		if(d1_empty && d2_empty) {
			//Ce sont deux vecteurs empty
			return 0.0;
		} else {
			//Comparaison d'un vecteur empty avec un vecteur non-empty (on donne la distance max)
			return local_max;
		}
	}
	*/

	if( !local_clustersdistances ) {
		fprintf(stderr, "succ_dist : clustersdistances is NULL\n");
		return 0.0;
	}
	//Ici, on compare deux vecteurs, et les -1 ont une distance minimale (non nulle) !
	for(int h=0; h<local_dim; h++) {
		if( (*decade1)<0 || (*decade2)<0) {
			dist+=local_max;
			//dist+=local_min;
			decade1++;
			decade2++;
		} else {
			dist+=local_clustersdistances[ *(decade1++) ][ *(decade2++) ];
		}
	}
	return dist;
}

int *kmedoid_succession_clustering(vector<int *> &successions, float **clustersdistances, int nclusters, int dim, int nbcl, int maxit)
{
	int m=successions.size();
	int k, l;
	float error;
	int ifound;
	int *clusterid=(int *)malloc(sizeof(int)*m);
	if(clusterid==NULL) {
		cerr << "succession_clustering : clusterid memory allocation error" <<endl;
		return NULL;
	}

	float **dij=alloc_matrix_float(m,m);

	//Calcul de matrice de distance
	float dist=0;
	for(k=0;k<m;k++) {
		for(l=0;l<m;l++) {
			assert( k<m && l<m );
			dist=0.0;
			if(l!=k) {
				int *decade1=successions[k];
				int *decade2=successions[l];
				if( decade1[0]==-1 || decade2[0]==-1) {
					//Cas d'un pixel vide
					if(decade1[0]==decade2[0]) {
						dist=0;
					} else {
						dist=FLT_MAX/10;
					}
				} else {
					//Cas d'un pixel normal !

					for(int h=0; h<dim; h++) {
						//assert(decade1[h]!=-1 && decade2[h]!=-1); //Si c'est vrai pour la première valeur, ca doit être vrai pour toutes !
						if( decade1[h]==-1 || decade2[h]==-1) {
							//std::cerr << "decade[h] == -1 ("<< k<<" ou "<<l<<")" << std::endl;
							dist=FLT_MAX/10; //Si on met le max ... on repousse l'erreur !
							break;
						}

						dist+=clustersdistances[ decade1[h] ][ decade2[h] ];
					}
				}
			}
			dij[k][l]=dist;
		}
	}
	kmedoids(nbcl, m, dij, 1, clusterid, &error, &ifound, maxit);

	free_matrix(dij, m);
	return clusterid;
}

/**
 * \brief Lancement de la catégorisation par les KMedoid sans calcul préalable d'une matrice de distances
 * \param successions successions[i] est une succession de taille dim
 * \param dim dimension d'une succession (ie le nombre d'années)
 * \param nb_succ nombre de successions à classer
 * \param nbcl nombre de classe de succession
 *
 *  Utilise la fonction KMedoid perso utilisant un pointeur de fonction
 *  pour calculer la distance (plus long, mais moins de mémoire requise)
 *
 *  \warning local_clustersdistances est allouée mais pas libérée
 */
int *kmedoid_succession_clustering(int **successions, int nb_succ, float **clustersdistances, int nclusters, int dim, int nbcl, int maxit, int **tcentroids)
{
	float error;
	int ifound, k, l;
	int *clusterid=(int *)malloc(sizeof(int)*nb_succ);
	if(clusterid==NULL) {
		cerr << "succession_clustering : clusterid memory allocation error" <<endl;
		return NULL;
	}

	//Initialisation des variables globales servant à la fonction de distance !
	local_dim=dim;
	local_clustersdistances = alloc_matrix_float(nclusters, nclusters);
	local_max=0;
	local_min=FLT_MAX;
	for(k=0; k<nclusters; k++) {
		for(l=0; l<nclusters; l++) {
			local_clustersdistances[k][l]=clustersdistances[k][l];
			local_max=(local_max<local_clustersdistances[k][l]?local_clustersdistances[k][l]:local_max);
			if( local_clustersdistances[k][l] ) local_min=(local_min>local_clustersdistances[k][l]?local_clustersdistances[k][l]:local_min);
		}
	}
	printf("Matrice distance extremum : %lf %lf\n", local_max, local_min);

	kmedoids_perso(/*nrows*/nbcl, /*ncols*/nb_succ, /*data*/successions, local_succ_dist, 1, clusterid, &error, &ifound, maxit, tcentroids);

	return clusterid;
}

/**
 * \brief Lancement de la catégorisation par les classification hiérarchique
 * Cette fonction nécessite le calcul préalable d'une matrice de distances !! (lourd en mémoire)
 *
 * \param successions successions[i] est une succession de taille dim
 * \param dim dimension d'une succession (ie le nombre d'années)
 * \param nb_succ nombre de successions à classer
 * \param nbcl nombre de classe de succession
 *
 *  \see treecluster_perso Utilise la fonction treecluster_perso
 *
 * \return Vecteur des classes
 */
int *treecluster_succession_clustering(int **successions, int nb_succ, float **clustersdistances, int nclusters, int dim, int nbcl)
{
	int k,l,d;
	//Allocation de la mémoire pour le vecteur résultat
	int *clusterid=(int *)malloc(sizeof(int)*nb_succ);
	if(clusterid==NULL) {
		cerr << "treecluster_succession_clustering : clusterid memory allocation error" <<endl;
		return NULL;
	}

	float distmax=0;
	for(k=0; k<nclusters; k++) {
		for(l=0; l<nclusters; l++) {
			distmax=(distmax<clustersdistances[k][l]?clustersdistances[k][l]:distmax);
		}
	}

	printf("Construction de la matrice de distance pour le TreeCluster\n");
	float **distmatrix=alloc_matrix_float(nb_succ, nb_succ);
	if( distmatrix== NULL) {
		free(clusterid);
		cerr << "treecluster_succession_clustering : clusterid memory allocation error" <<endl;
		return NULL;
	}
	//Calcul des distances :
	for(k=0; k<nb_succ; k++) {
		for(l=0; l<nb_succ; l++) {
			distmatrix[k][l]=0;
			for(d=0; d<dim; d++) {
				int c1=successions[k][d];
				int c2=successions[l][d];
				if(c1==-1 && c2==-1) {
				} else if(c1==-1 || c2==-1) {
					distmatrix[k][l]+=distmax;
				} else {
					distmatrix[k][l]+=clustersdistances[ successions[k][d] ][ successions[l][d] ];
				}
			}
		}
	}

	printf("Lancement du clustering hiérarchique\n");
	Node* tree = treecluster_perso (/*nelements*/nb_succ, /*method*/'m', distmatrix);

	printf("Construction des classes à partir de l'arbre\n");
	cuttree (nb_succ, tree, /*nclusters*/nbcl, clusterid);

	free_matrix(distmatrix,nb_succ);

	return clusterid;
}

/**
 * Structure de données pour le calcul des successions les plus fréquentes
 */
typedef struct {
	int *succession;
	int frequence;
} succession;

/*
 * Fonction de comparaison de successions sur la base de leurs fréquences
 * L'ordre permet de classer les successions de la plus fréquentes à la moins fréquente
 */
bool compare_successions (succession first, succession second)
{
	return first.frequence>second.frequence;
}

/**
 * Fonction de test d'égalité de deux successions, basé sur la succession !
 */
bool succession_equals(succession first, succession second, int size)
{
	return !memcmp(first.succession, second.succession, sizeof(int)*size);
}


float **order_profiles(float **annualprofiles, int n_annualprofiles, int dim)
{
	float **result=(float **)malloc(sizeof(float*)*n_annualprofiles);
	float **cdist=alloc_matrix_float(n_annualprofiles, n_annualprofiles);
	annualprofiles_distances_construction(annualprofiles, n_annualprofiles, dim, cdist);

	int *done=(int*)malloc(sizeof(int)*n_annualprofiles);

	int argmax1=-1;
	int argmax2=-1;
	float max=0;
	for(int i=0; i<n_annualprofiles;i++) {
		for(int j=i+1; j<n_annualprofiles;j++) {
			if( cdist[i][j]>max ) {
				max=cdist[i][j];
				argmax1=i;
				argmax2=j;
			}
		}
	}
	assert(argmax1!=-1 && argmax2!=-1);

	float moy1=0.0, moy2=0.0;
	for(int j=0; j< dim; j++) {
		moy1+=annualprofiles[argmax1][j];
		moy2+=annualprofiles[argmax2][j];
	}
	if( moy1<moy2) {
		result[0]=annualprofiles[argmax1];
		done[0]=argmax1;
	} else {
		result[0]=annualprofiles[argmax2];
		done[0]=argmax2;
	}

	//on cherche le plus proche de proche en proche dans la matrice de distance !
	int current=1;
	while( current!=n_annualprofiles ) {

		//On cherche l'élément le plus proche du profil done[current-1]
		// parmis ceux qui restent !
		float min=FLT_MAX;
		int argmin=-1;
		for(int i=0; i<n_annualprofiles;i++) {
			int isdone=0;
			for(int j=0; j<current; j++) {
				if( i==done[j]) {
					isdone=1;
					break;
				}
			}
			if(isdone) continue; //le profil i a déjà été traité !

			float dist=cdist[ done[current-1] ][i];
			if( dist<min ) {
				min=dist;
				argmin=i;
			}
		}

		assert(argmin!=-1);
		//ICI, on a trouvé l'élément le pruls proche de done[current-1]

		result[ current ] = annualprofiles[ argmin ];
		done[ current ] = argmin;
		current++;
	}

	//Un peu de libération de mémoire :
	free_matrix(cdist, n_annualprofiles);
	free(done);
	free(annualprofiles); //supprime uniquement le vecteur de pointeurs sur les profils, pas les profils !

	return result;
}



/**
 * Fonction qui remappe le vecteur d'input en une matrice de taille (pp->sx*pp->sy)*pp->ny.
 * result[i][j] est le j-eme point de la decade du i-eme pixel.
 *
 * Peut contenir des valeurs -1 (pas de classe !)
 */
int ** compute_raw_successions(int *input, params *pp, unsigned short int **invalid, int *nb, int step) {
	*invalid=(unsigned short int *)malloc( sizeof(unsigned short int *)*pp->sx*pp->sy/step );
	int **result=alloc_matrix_int(pp->sx*pp->sy/step, pp->ny);
	*nb=0;
	if( result==NULL ) {
		cerr << "compute_raw_successions : allocation error" <<endl;
		return NULL;
	}

	int j=0;
	for(int i=0; i<pp->sx*pp->sy/step; i++) {
		int *decade = (int*)malloc(sizeof(int)*pp->ny);
		if( decade==NULL ) {
			cerr << "compute_raw_successions : decade allocation error" <<endl;
			return NULL;
		}

		int ok=0;
		for(int ny=0; ny<pp->ny; ny++) {
			decade[ny]=input[i*step+pp->sx*pp->sy*ny];
			if(!ok && decade[ny]!=-1) ok=1;
		}
		//ICI decade contient le vecteur du profil décadaire d'un pixel

		if( ok ) {
			(*invalid)[i]=0;
			result[j]=decade;
			(*nb)++;
			j++;
		} else {
			(*invalid)[i]=1;
			free(decade);
		}
	}
	return result;
}

/**
 * \brief Fonction qui récupère les successions à classer
 * \param result vecteur de taille pp->sx*pp->sy*pp->ny contenant les successions de profils annuels
 * \param pp paramètres des algorithmes, pp->n_successions définit le nombre maximum de succession à
 *  classer (taille maximum du vecteur retourné par la fonction)
 *
 * Si le nombre total de succession est supérieur à pp->n_successions, les successions utilisées sont,
 * en premier lieu, les successions les plus fréquentes dans l'image. Pour toutes les successions de
 */
vector<int *> compute_successions(int *result, params *pp) {
	int sx=pp->sx, sy=pp->sy;
	int nbmax = pp->n_successions;


	vector<int *> successions;
	vector<int> frequences;
	for(int i=0; i<sx*sy; i++) {
		if( !(i%1000) ) printf("%d/%d done\n", i, sx*sy);
		int *decade = (int*)malloc(sizeof(int)*pp->ny);
		if( decade==NULL ) {
			cerr << "compute_successions : allocation error" <<endl;
			return successions;
		}

		for(int ny=0; ny<pp->ny; ny++) {
			decade[ny]=result[i+sx*sy*ny];
		}
		//ICI decade contient le vecteur du profil décadaire d'un pixel

		bool skip=false;
		for(int ny=0; ny<pp->ny; ny++) {
			if(decade[ny]==-1) {
				skip=true;
			}
		}
		if(skip) {
			free(decade);
			continue;
		}

		bool found=false;
		int j=0;
		for(vector<int *>::iterator it=successions.begin(); it!=successions.end(); it++, j++) {
			if( !memcmp(*it, decade, sizeof(int)*pp->ny) ) {
				found=true;
				frequences[j]++;
				break;
			}
		}
		if( !found ) {
			successions.push_back( decade );
			frequences.push_back(1);
		} else {
			free(decade);
		}
	}

	if( nbmax>0 && (int)successions.size()>nbmax ) {
		printf("=> %d successions, %d sélectionnées\n", (int)successions.size(), nbmax);
		vector<int *> ss=vector<int*>(nbmax);
		vector<int> pos=vector<int>(nbmax);
		int i=0;
		vector<int *>::iterator it=successions.begin();
		int freqmin=frequences[0];
		//int freqargmin=0;
		int ssfreqargmin=0;
		for(; it!=successions.end() && i<nbmax; it++, i++) {
			ss[i]=*it;
			pos[i]=i;
			if( frequences[i]<freqmin ) {
				//freqargmin=i;
				ssfreqargmin=i;
				freqmin=frequences[i];
			}
		}

		for(; it!=successions.end(); it++, i++) {
			if( frequences[i] < freqmin ) {
				delete(*it);
				continue;
			}
			// ICI il faut remplacer le plus petit
			delete( ss[ssfreqargmin] );
			ss[ssfreqargmin]=*it;
			//On recherche le nouveau plus petit élément
			freqmin=frequences[ pos[0] ];
			//freqargmin=pos[0];
			ssfreqargmin=0;
			int j=0;
			for(vector<int>::iterator jt=pos.begin(); jt!=pos.end(); jt++, j++) {
				if( frequences[*jt]<freqmin ) {
					freqmin=frequences[*jt];
					//freqargmin=*jt;
					ssfreqargmin=j;
				}
			}
		}
		return ss;
	}
	return successions;
}

int *create_decades(images_params ip, params *pp, float **cdist)
{
	int ncols, npixels;
	float **data=NULL;
	float error;
	int ifound;
	float** annualprofiles=NULL;
	int *result=NULL;

	//Chargement du fichier de données
	printf("LOAD DATA\n");
	data= load_data(ip, pp);
	printf("\t-> ok !\n");

	npixels=pp->nb_pixels; //Nombre de pixels utiles (sans NaN, avec sauts éventuels)
	ncols= ip.band_per_year; //Nombre de couches par années

	//Préparation du K-Means

	int *clusterid = (int *)malloc(sizeof(int)*npixels*pp->ny);
	if( clusterid==NULL ) {
		cerr << "clusterid allocation error" <<endl;
		return NULL;
	}
	for(int i=0; i<npixels*pp->ny; i++) {
		clusterid[i]=0;
	}
	float *weight = (float *)malloc(sizeof(float)*ncols);
	for(int i=0; i<ncols; i++) {
		weight[i]=1;
	}

	//Lancement du K-Means
	printf("*********\nKMEANS %d classes, %d individus, %d dimensions\n*********\n", pp->n_annualprofiles, npixels*pp->ny, ncols);

	kcluster(pp->n_annualprofiles, npixels*pp->ny, ncols, data, NULL /*mask*/, weight, 0 /*transpose*/, 1 /*nbpass*/, pp->kmeans_maxiter /*maxiter*/, 'a' /*method*/, 'e' /*dist*/, clusterid, &annualprofiles, &error, &ifound);
	free(weight);

	free_matrix(data, npixels*pp->ny);

	//Réorganisation des profils annuels pour les mettre dans l'ordre (du plus sec au plus humide) !
	annualprofiles = order_profiles(annualprofiles, pp->n_annualprofiles, ncols);

	//Construction de la matrice de distances entre profils annuels :
	annualprofiles_distances_construction(annualprofiles, pp->n_annualprofiles, ncols, cdist);

	//Sauvegarde des profils :
	char filename[200];
	sprintf(filename, "%s/%s", ip.output_basename, PROFILES_FILE);
	save_profiles(filename, annualprofiles, pp->n_annualprofiles, ncols);

	//Suppression du travail déjà effectué !
	free(clusterid);

	//Allocation du vecteur de résultat
	result=(int *)malloc(sizeof(int)*pp->sx*pp->sy*pp->ny);
	if( result==NULL ) {
		cerr << "create_decades : memory allocation error !" << endl;
		return NULL;
	}
	pp->attribution_error=0;
	for(int k=0;k<pp->ny;k++) {
		printf("Complete %d/%d\n", k, pp->ny);
		float error;
		complete_annually(result, ip, *pp, k, annualprofiles, &error);

		pp->attribution_error+=error;
	}

	//On a plus besoin des représentants des clusters
	free_matrix(annualprofiles, pp->n_annualprofiles);

	return result;
}

int *create_decades_em(images_params ip, params *pp, float **cdist)
{
	int ncols, npixels;
	float **data=NULL;

	// initialisation des profils
	profile *profiles = (profile*)calloc(sizeof(profile), pp->n_annualprofiles);
	for(int i=0; i<pp->n_annualprofiles; i++) {
		profiles[i].length = ip.band_per_year;
		profiles[i].nb = 0;
		profiles[i].mean = (double*)calloc(sizeof(double), 2*ip.band_per_year);
		profiles[i].std = profiles[i].mean + ip.band_per_year;
		profiles[i].envmin = (float*)calloc(sizeof(float), 2*ip.band_per_year);
		profiles[i].envmax = profiles[i].envmin + ip.band_per_year;
		for(int j=0; j<ip.band_per_year; j++) {
			profiles[i].mean[j]=0.0;
			profiles[i].std[j]=0.0;
			profiles[i].envmin[j]=FLT_MAX;
			profiles[i].envmax[j]=FLT_MIN;
		}
	}

	//Chargement du fichier de données
	printf("LOAD DATA\n");
	data= load_data(ip, pp);
	printf("\t-> ok !\n");

	npixels=pp->nb_pixels; //Nombre de pixels utiles (sans NaN, avec sauts éventuels)
	ncols= ip.band_per_year; //Nombre de couches par années

	//Lancement de EM sur le jeu de données sous-échantillonné
	printf("EM clustering with %d models\n", pp->n_annualprofiles);
	gmm *model=train_em(data, ncols, npixels*pp->ny, pp->n_annualprofiles);

	//Attribution des classes à l'ensemble des pixels par le model appris
	int *result=(int *)malloc(sizeof(int)*pp->sx*pp->sy*pp->ny);
	if( result==NULL ) {
		cerr << "create_decades : memory allocation error !" << endl;
		return NULL;
	}
	/*
	for(int k=0; k<pp->ny; k++) {
		printf("Complete %d/%d\n", k, pp->ny);
		complete_annually_em(result, ip, *pp, k, model, profiles);
	}
	*/
	complete_annually_em_thl(result, ip, *pp, model, profiles);

	//calcul de la moyenne
	for(int i=0; i<pp->n_annualprofiles; i++) {
		for(int j=0; j<ip.band_per_year; j++) {
			profiles[i].mean[j]/=(float)profiles[i].nb;
		}
	}

	//Evaluation de la variance uniquement sur le jeu d'apprentissage !!
	for(int pixel=0; pixel<npixels*pp->ny; pixel++) {
		int cl = result[pixel];
		for(int j=0; j<ip.band_per_year; j++) {
			float r=(profiles[cl].mean[j]-data[pixel][j]);
			profiles[cl].std[j]+=r*r;
		}
	}
	for(int i=0; i<pp->n_annualprofiles; i++) {
		for(int j=0; j<ip.band_per_year; j++) {
			profiles[i].std[j]/=(float)profiles[i].nb;
		}
	}

	//Construction de la matrice de distances entre profils annuels :
	float** meanprofiles=alloc_matrix_float(pp->n_annualprofiles, ncols);
	for(int i=0; i<pp->n_annualprofiles; i++) {
		for(int j=0; j<ncols; j++) {
			meanprofiles[i][j]=profiles[i].mean[j];
		}
	}
	annualprofiles_distances_construction(meanprofiles, pp->n_annualprofiles, ncols, cdist);
	//Sauvegarde des profils
	char filename[200];
	sprintf(filename, "%s/%s", ip.output_basename, PROFILES_FILE);
	save_profiles(filename, meanprofiles, pp->n_annualprofiles, ip.band_per_year);
	free_matrix(meanprofiles, pp->n_annualprofiles);

	//Sauvegarde des informations étendues sur les profils
	sprintf(filename, "%s/%s", ip.output_basename, EXTEND_PROFILES_FILE);
	save_profiles(filename, profiles, pp->n_annualprofiles, ip.band_per_year);

	//liberation de la mémoire
	free_matrix(data, npixels*pp->ny);
	free(model);

	for(int i=0; i<pp->n_annualprofiles; i++) {
		free(profiles[i].mean);
		free(profiles[i].envmin);
	}

	return result;
}


int *create_successions(int *result, float **cdist, images_params *ip, params *pp)
{
	//Vecteur de correspondance entre le numero de classe et un
	// index entre 0 et nbcl à donner dans le fichier de résultat
	int nbcor=1;
	int *correspondance=(int*)malloc( sizeof(int) * (pp->n_decade+1) );
	for(int i=0; i<pp->n_decade+1; i++) {
		correspondance[i]=-1;
	}

	local_clustersdistances=NULL;
	local_max=0;
	local_min=0;

	//*
	// Clustering total, sans utilisation d'une matrice
	printf("*********\nCONSTRUCTION DES SUCCESSIONS\n*********\n");
	// Construction de la liste des successions
	int step=pp->sample_rate_dec;
	unsigned short int *invalids;
	int nb;
	int **successions=compute_raw_successions(result, pp, &invalids, &nb, step);
	int *centroids=NULL; //alloué par KMedoid !

	printf("*********\nSUCCESSION CLUSTERING %d successions, #profils %d, #classes=%d\n*********\n", nb, pp->n_annualprofiles, pp->n_decade);
	int *idx=kmedoid_succession_clustering(successions, nb, cdist, pp->n_annualprofiles, pp->ny, pp->n_decade, pp->kmedoid_maxiter, &centroids);
	//int *idx=treecluster_succession_clustering(successions, nb, cdist, pp->n_annualprofiles, pp->ny, pp->n_decade);


	printf("*********\nATTRIBUTION DES CLUSTERS\n*********\n");
	int *idximage=(int*)malloc( sizeof(int)*pp->sx*pp->sy );
	memset(idximage, 0, sizeof(int)*pp->sx*pp->sy );
	int *decade=(int*)malloc( sizeof(int)*pp->ny ); //Pour l'usage !
	int *p=idximage; //Pointeur sur le pixel courant à remplir
	int x=0; //Index du pixel courant à remplir
	int j=0; //Index de idx
	correspondance[0]=idx[0];
	for(int i=0; i<pp->sx*pp->sy/step; i++) {
	    printf("%.2lf%%", 100*(float)(i)/(float)(pp->sx*pp->sy/step));
		fflush(stdout);
		printf("\r");

		if( invalids[i] ) {
			*p=-1;
		} else {
			int val=-1;
			//chercher la valeur de correspondance entre 1 et nbcor
			bool found=false;
			for(int k=0; k<nbcor; k++) {
				if( correspondance[k]==idx[j] ) {
					val=k;
					found=true;
					break;
				}
			}
			if( !found ) { // Si rien n'a été trouvé, on ajoute une nouvelle entrée
				correspondance[nbcor]=idx[j];
				val=nbcor;
				nbcor++;
			}
			*p=val;
			j++;
		}
		p++;
		x++;
		for(int k=1;k<step; k++) {
			//Attribution pour les autres positions entre pos+1 et pos+step !

			// 1. on construit le vecteur
			int ok=0;
			for(int ny=0; ny<pp->ny; ny++) {
				decade[ny]=result[x+pp->sx*pp->sy*ny];
				if(!ok && decade[ny]!=-1) ok=1;
			}
			if( !ok ) {
				//Si c'est une vecteur NaN, on attribut -1 et on passe !
				*p=-1;
				p++;
				x++;
				continue;
			}

			//2. si ce n'est pas un vecteur NaN, alors on cherche
			// la classe la plus proche
			float min=FLT_MAX;
			int argmin=0;
			for(int l=0; l<nb; l++) {
				float d=succ_dist(decade, successions[ l ], pp->ny, cdist, pp->n_annualprofiles);
				if( d<min ) {
					min=d;
					argmin=idx[l];
					if( d==0 ) {
						break;
					}
				}
			}

			//3. Recherche de la correspondance avec argmin !
			int val=-1;
			//chercher la valeur de correspondance entre 1 et nbcor
			bool found=false;
			for(int k=0; k<nbcor; k++) {
				if( correspondance[k]==argmin ) {
					val=k;
					found=true;
					break;
				}
			}
			if( !found ) { // Si rien n'a été trouvé, on ajoute une nouvelle entrée
				correspondance[nbcor]=argmin;
				val=nbcor;
				nbcor++;
			}

			*p=val;
			p++;
			x++;
		}
	}
	free(decade);
	decade=NULL;

	char filename[200];
	sprintf(filename, "%s/%s", ip->output_basename, SUCCESSIONS_FILE);
	save_successions(filename, successions, correspondance, idx, nb, pp->n_decade, pp->ny);

	//Liberation de la mémoire
	if(local_clustersdistances) free_matrix(local_clustersdistances, pp->n_annualprofiles);
	local_clustersdistances=NULL;

	free_matrix_int(successions, nb);
	successions=NULL;
	if(centroids) free(centroids);
	centroids=NULL;
	free(idx);
	idx=NULL;

	/*/
	// Construction des successions sous-échantillonnant les successions
	// -> Le problème de cette méthode est le temps de calcul des successions
	//    ainsi que le fait que beaucoup de successions ne sont présentent qu'une
	//    seule fois dans l'image : lesquelles choisir de conserver ?

	printf("*********\nConstruction des successions\n*********\n");
	// Construction de la liste des successions
	vector<int *> successions=compute_successions(result, pp);

	printf("*********\nSUCCESSION CLUSTERING %d successions %d valeurs, s=%lf\n*********\n", successions.size(), pp->n_annualprofiles, pp->s);
	int *idx=kmedoid_succession_clustering(successions, cdist, pp->n_annualprofiles, pp->ny, pp->n_decade, pp->kmedoid_maxiter);
	//int *idx=succession_clustering(successions, cdist, pp->n_annualprofiles, pp->ny, pp->s);


	printf("*********\nIMAGE RECONSTRUCTION %d classes\n*********\n", pp->n_decade);

	int *decade=(int *)malloc(sizeof(int)*pp->ny);
	if(decade==NULL) {
		cerr << "decade : memory allocation error"<<endl;
		return NULL;
	}
	int *idximage=(int*)malloc(sizeof(int)*pp->sx*pp->sy);
	if(idximage==NULL) {
		cerr << "idximage : memory allocation error"<<endl;
		return NULL;
	}

	int *p=idximage;
	for(int i=0; i<pp->sx*pp->sy; i++, p++) {
		for(int ny=0; ny<pp->ny; ny++) {
			decade[ny]=result[i+pp->sx*pp->sy*ny];
		}
		*p=-1;

		if( decade[0]==-1 ) {
			continue;
		}


		vector<int *>::iterator it=successions.begin();
		int s=0;
		float min=FLT_MAX;
		int argmin=0;

		while( it!=successions.end() ) {
			float d=dist(*it, decade, pp->ny, cdist, pp->n_annualprofiles);
			if( d==0 ) {
				break;
			} else {
				if(d<min) {
					min=d;
					argmin=s;
				}
			}
			s++;
			it++;
		}

		if(min!=0) {
			s=argmin;
		}
		//========= Faire l'attribution d'une valeur (exacte ou approchée)
		int val=-1;
		//chercher la valeur de correspondance entre 1 et nbcor
		bool found=false;
		for(int k=0; k<nbcor; k++) {
			if( correspondance[k]==idx[s] ) {
				val=k;
				found=true;
				break;
			}
		}
		if( !found ) { // Si rien n'a été trouvé, on ajoute une nouvelle entrée
			correspondance[nbcor]=idx[s];
			val=nbcor;
			nbcor++;
		}
		*p=val;
		//=========

	}


	char filename[200];
	sprintf(filename, "%s/%s", ip->output_basename, SUCCESSIONS_FILE);
	save_successions(filename, successions, correspondance, idx, &(pp->n_decade), pp->ny);

	free(decade);

	//free successions
	for(vector<int *>::iterator it=successions.begin(); it!=successions.end(); it++) {
		free(*it);
	}
	free(idx);
	//*/
	free(correspondance);

	pp->succ_ok=1;
	return idximage;
}


void save_params(images_params *ip, params *p)
{
	char filename[200];
	sprintf(filename, "%s/%s", ip->output_basename, PARAMS_FILE);
	FILE * fout=fopen(filename, "w+");
	if( fout == NULL)
		return;

	fprintf(fout, "%s\n", ip->hdrfilename);
	sprintf(filename, "%s/succ_%d_%d_c%d", ip->output_basename, ip->startyear, ip->endyear, p->n_annualprofiles);
	fprintf(fout, "%s\n", filename);
	if(p->succ_ok) {
		sprintf(filename, "%s/dec_%d_%d_c%d_c%d", ip->output_basename, ip->startyear, ip->endyear, p->n_annualprofiles, p->n_decade );
		fprintf(fout, "%s\n", filename);
	} else {
		fprintf(fout, "not_computed\n");
	}

	fprintf(fout, "%d %d\n", ip->startyear, ip->endyear);
	fprintf(fout, "%lfx%lf, %lfx%lf\n", ip->X0, ip->Y0, ip->X1, ip->Y1);

	fprintf(fout, "%d %dx%d, %dx%d\n", p->ny, p->x0, p->y0, p->x1, p->y1);
	fprintf(fout, "%d %d %d\n", p->n_annualprofiles, p->kmeans_maxiter, p->sample_rate);
	fprintf(fout, "%d %d %d %f", p->n_decade, p->n_successions, p->kmedoid_maxiter, p->s);

	fprintf(fout, "error : %f", p->attribution_error);

	fclose(fout);
}

void print(images_params *ip, params *p) {
	char filename[200];
	printf("HDR filename: %s\n", ip->hdrfilename);
	sprintf(filename, "%s/succ_%d_%d_c%d", ip->output_basename, ip->startyear, ip->endyear, p->n_annualprofiles);
	printf("Succession filename : %s\n", filename);
	if(p->succ_ok) {
		sprintf(filename, "%s/dec_%d_%d_c%d_c%d", ip->output_basename, ip->startyear, ip->endyear, p->n_annualprofiles, p->n_decade );
		printf("Decade filename : %s\n", filename);
	} else {
		printf("Decade filename : not_computed\n");
	}
	printf("selected years: %d-%d\n", ip->startyear, ip->endyear);
	printf("image first year: %d\n", ip->firstyear);
	printf("geographic extent: %lfx%lf, %lfx%lf\n", ip->X0, ip->Y0, ip->X1, ip->Y1);
	printf("images extent: %d %dx%d, %dx%d\n", p->ny, p->x0, p->y0, p->x1, p->y1);
	printf("succession params: %d %d %d\n", p->n_annualprofiles, p->kmeans_maxiter, p->sample_rate);
	printf("decades params: %d %d %d %f\n", p->n_decade, p->n_successions, p->kmedoid_maxiter, p->s);
	printf("error : %f\n", p->attribution_error);
}

