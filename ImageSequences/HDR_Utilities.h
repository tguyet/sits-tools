 /*
 * HDR_Utilities.h
 *
 * This file is a part of the long-term SITS (Satellite Image Time Series) classification tool.
 * 
 * author: guyet
 * version: 2.0
 * last modification : 24/10/2013
 */

#ifndef HDR_UTILITIES_H
#define HDR_UTILITIES_H

#include "ImageSequences.h"


int savehdr(const char *hdrfilename, images_params ip, params p, int *data);

float **load_data(images_params ip, params *p);
float **load_timeseries(images_params ip, params *p, std::list<int> &invalidpixels);

//float **load_data(images_params ip, params *p, int decal, std::list<int> &invalids);
float **load_annual_data_part(images_params ip, params p, int year, int part, int nbparts, int *nbli);

float *load_data(int x0, int y0, int annee, images_params ip, params p);


int save_hdr_decade(const char *hdrfilename, params p, int *data);
int *load_hdr_decade(const char *hdrfilename, params *pa);


#endif
