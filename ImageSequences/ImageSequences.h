/*
 * ImageSequences.h
 *
 * This file is a part of the long-term SITS (Satellite Image Time Series) classification tool.
 * 
 * author: guyet
 * version: 2.0
 * last modification : 24/10/2013
 */

#ifndef IMAGESEQUENCES_H_
#define IMAGESEQUENCES_H_

#include <list>

#define CLUSTERS_FILE		"clustersdistances.txt"
#define SUCCESSIONS_FILE	"successions.txt"
#define PROFILES_FILE		"annualprofiles.txt"
#define EXTEND_PROFILES_FILE		"extendannualprofiles.txt"
#define PARAMS_FILE			"params.txt"

typedef struct {
	char *hdrfilename;
	float X0, Y0, X1, Y1;
	int start_band;
	int band_order;
	int startyear; //Annee de debut de traitement
	int endyear;
	int firstyear; //Premiere annee du fichier
	int year_order; //ordre des annees dans le fichiers : 1 si dans l'ordre croissant et -1 sinon
	int band_per_year;

	char *input_basename;
	char *output_basename;
} images_params;

enum {EM_CLUSTERING, KM_CLUSTERING};

typedef struct {
	//Paramètres des images
	int sx, sy; //< Taille de l'image
	int x0, y0, x1, y1; //< Positions dans l'image d'origine
	double adfGeoTransform[6]; //< informations sur le géoréférencement de l'image
	int ny;		//< Nombre d'années
	int *annee; //< liste des années utilisées

	int nb_pixels;
	int sample_rate; //< Taux d'échantillonage pour la construction des profils annuels : 1 pixel sur sample_rate est alors utilisé pour la classification (régulièrement).
	int sample_rate_dec; //< Taux d'échantillonage pour la construction des profils successions (régulièrement)
	int nb_processing_parts;
	bool reuse_clusteringid;

	int clustering_method;

	int n_annualprofiles; //Nombre de profiles annuels

	//nombre de successions décadaires typiques
		int n_successions; //< Nombre de successions à conserver pour la classification (récupère uniquement les plus fréquentes)
		float s; //< paramètre pour l'algorithme AP (influe sur le nombre de successions décadaires typiques)
		int n_decade; //< algorithme K-Medoid
		int succ_ok; //<Indique si l'image des classes de successions ont été calculées

	bool use_invalidpixels;

	//autres paramètres
	int kmeans_maxiter;
	int kmedoid_maxiter;
	int ap_convit ;
	int ap_maxit;

	float attribution_error;
} params;


typedef struct {
	int length;
	int nb;
	double *mean; //utilisation de doubl pour limiter les débordements !
	double *std;
	float *envmin;
	float *envmax;
} profile;


int *create_decades(images_params ip, params *p, float **cdist);
int *create_decades_em(images_params ip, params *p, float **cdist);
int *create_successions(int *result, float **cdist, images_params *ip, params *pp);
float ** load_clustersdistances(const char *filename, int *nclusters);
void save_clustersdistances(const char *filename, float **, int nclusters);

void init_params(params *p);
void init_images_params(images_params *p);
void free_images_params(images_params p);
void save_params(images_params *ip, params *p);
void print(images_params *ip, params *p);


#endif /* IMAGESEQUENCES_H_ */
