# -*- coding: utf-8 -*-
"""
@author: T. Guyet, AGROCAMPUS-OUEST
@date: 12/2013

Outil pour l'evaluation en cascade du clustering
"""

import sys
import numpy as np
from scipy import interp
import pylab as pl

from sklearn import svm, datasets
from sklearn.metrics import roc_curve, auc
from sklearn.cross_validation import StratifiedKFold
from sklearn import cross_validation, datasets, svm

#chargement des labels

labelsfile="labels_singleclass.csv"
csvfile="data.csv"
labels = np.array(np.genfromtxt(labelsfile, dtype=None, delimiter=','))

values= set()
for l in labels:
	values.add(l[1])
values = list(values)


y=[]
for l in labels:
	y.append( values.index(l[1]) )
y=np.array(y)

Xall = np.genfromtxt(csvfile, dtype=None, delimiter=',')
X=[]
for l in Xall:
	X.append(l[:23])
X=np.array(X)

Cl = np.genfromtxt("../output.csv", dtype=None, delimiter=',')
X2=[]
for l in range(len(X)):
	X2.append( list(X[l]) + list(Cl[l]) ) 
X2=np.array(X2)

print len(y)
print len(X)

#suppression des exemples pour les classes trop rares pour la cross-validation
for l in values:
	v = values.index(l)
	nb = np.sum(y==v)
	print "#"+ str(v) + ":" + str(nb)
	if nb < 50:
		y,X,X2=y[y!=v], X[y!=v], X2[y!=v]

print len(y)
print len(X)

#print y
#print np.size(X[1])

#print X2
#sys.exit()

svc = svm.SVC(kernel='linear')
#C_s = np.logspace(-5, -3, 3)
C_s=[1]

cv = StratifiedKFold(y, k=3)

print "Cross validation of the single image"

scores = list()
scores_std = list()
for C in C_s:
    print "\tC=",C
    svc.C = C
    this_scores = cross_validation.cross_val_score(svc, X, y, cv=cv, n_jobs=4 )
    scores.append(np.mean(this_scores))
    scores_std.append(np.std(this_scores))

print "Cross validation of the image with classes"

scores2 = list()
scores_std2 = list()
for C in C_s:
    print "\tC=",C
    svc.C = C
    this_scores = cross_validation.cross_val_score(svc, X2, y, cv=cv, n_jobs=4 )
    scores2.append(np.mean(this_scores))
    scores_std2.append(np.std(this_scores))

# Do the plotting
pl.figure(1, figsize=(4, 3))
pl.clf()
pl.semilogx(C_s, scores)
pl.semilogx(C_s, np.array(scores) + np.array(scores_std), 'b--')
pl.semilogx(C_s, np.array(scores) - np.array(scores_std), 'b--')

pl.semilogx(C_s, scores2, 'r')
pl.semilogx(C_s, np.array(scores2) + np.array(scores_std2), 'r--')
pl.semilogx(C_s, np.array(scores2) - np.array(scores_std2), 'r--')

locs, labels = pl.yticks()
pl.yticks(locs, map(lambda x: "%g" % x, locs))

pl.ylabel('CV score')
pl.xlabel('Parameter C')
pl.ylim(0, 1.1)
pl.show()

print "score",scores,"+-",scores_std
print "score2",scores2,"+-",scores_std2



