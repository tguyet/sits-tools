# -*- coding: utf-8 -*-
"""
@author: T. Guyet, AGROCAMPUS-OUEST
@date: 12/2013

Outil pour l'evaluation en cascade du clustering
"""

import sys
import numpy as np
from scipy import interp
import pylab as pl

from sklearn import svm, datasets
from sklearn.metrics import roc_curve, auc
from sklearn.cross_validation import StratifiedKFold, ShuffleSplit
from sklearn import cross_validation, datasets, svm

from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.lda import LDA
from sklearn.qda import QDA
from scipy.stats import wilcoxon

#chargement des labels
labelsfile="labels_singleclass.csv"
csvfile="data.csv"
labels = np.array(np.genfromtxt(labelsfile, dtype=None, delimiter=','))

values= set()
for l in labels:
	values.add(l[1])
values = list(values)

y=[]
for l in labels:
	y.append( values.index(l[1]) )
y=np.array(y)

Xall = np.genfromtxt(csvfile, dtype=None, delimiter=',')
X=[]
for l in Xall:
	X.append(l[:3])
X=np.array(X)

Cl = np.genfromtxt("../output.csv", dtype=None, delimiter=',')

X2=[]
for l in range(len(X)):
	if isinstance(Cl[0], np.ndarray):
		X2.append( list(X[l]) + list(Cl[l]) ) 
	else:
		X2.append( list(X[l]) + Cl[l] ) 
X2=np.array(X2)

print "#examples:", len(y)

#suppression des exemples pour les classes trop rares pour la cross-validation
SkF=False # Stratified K Fold
if SkF:
	for l in values:
		v = values.index(l)
		nb = np.sum(y==v)
		print "#"+ str(v) + ":" + str(nb)
		if nb < 50:
			y,X,X2=y[y!=v], X[y!=v], X2[y!=v]

	print "#examples:", len(y)


classifier = RandomForestClassifier()
#classifier = DecisionTreeClassifier()
#classifier = KNeighborsClassifier()
#classifier = svm.SVC(kernel='rbf', probability=True)
#classifier.C=0.01
print classifier

cv = ShuffleSplit(len(y), n_iterations=5, test_fraction=0.5, indices=True, random_state=None)

error_rates=np.zeros( (5,4) )
pij=[]
s2=[]
i=0
for train_index, test_index in cv:
	print "Iteration "+str(i)
	
	#Construction des jeux d'exemple en 2-fold 50/50
	X_train, X_test = X[train_index], X[test_index]
	X2_train, X2_test = X2[train_index], X2[test_index]
	y_train, y_test = y[train_index], y[test_index]
	
	#Apprentissage 1, C1
	classifier.fit(X_train, y_train)
	#error_rate=1-classifier.score(X_test, y_test)
	pred=classifier.predict(X_test)
	error_rates[i,0]=np.sum( (pred-y_test)!=0 )/float(len(y_test))
	#Apprentissage 1, C2
	classifier.fit(X2_train, y_train)
	#error_rate2=1-classifier.score(X2_test, y_test)
	pred=classifier.predict(X2_test)
	error_rates[i,1]=np.sum( (pred-y_test)!=0 )/float(len(y_test))
	#difference entre les classifieurs
	pi1=error_rates[i,0]-error_rates[i,1]
	
	#apprentissage 2, C1
	classifier.fit(X_test, y_test)
	pred=classifier.predict(X_train)
	error_rates[i,2]=np.sum( (pred-y_train)!=0 )/float(len(y_train))
	#apprentissage 2, C2
	classifier.fit(X2_test, y_test)
	pred=classifier.predict(X2_train)
	error_rates[i,3]=np.sum( (pred-y_train)!=0 )/float(len(y_train))
	#difference entre les classifieurs
	pi2=error_rates[i,2]-error_rates[i,3]
	
	pi=(pi1+pi2)/2.0
	si2=(pi1 - pi)**2 + (pi2 - pi)**2
	pij.append(pi1)
	pij.append(pi2)
	s2.append(si2)
	i+=1
	
pij=np.array(pij)

#pl.boxplot(pij)
#pl.show()

print "mean error rate reduction:", np.mean(pij)
print "wilcoxon:", wilcoxon(pij)

s2=np.array(s2)
denum=np.sqrt(np.sum( s2/5 ))
t= pij/denum

print "5x2-fold F-test", t
print "5x2-fold F-test", t>2.571

