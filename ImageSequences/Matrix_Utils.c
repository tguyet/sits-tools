/*
 * Matrix_Utils.c
 *
 * This file is a part of the long-term SITS (Satellite Image Time Series) classification tool.
 * 
 * author: guyet
 * version: 2.0
 * last modification : 24/10/2013
 */

#include <stdlib.h>
#include <stdio.h>

float **tomatrix(float **rawdata, int npixels, int ncols, int ny)
{
	int nm, i;
	float **matrix=(float **)malloc( sizeof(float*)*npixels*ny );
	if( matrix == NULL ) {
		fprintf(stderr, "tomatrix : matrix allocation error\n");
		return NULL;
	}
	float **p=matrix;
	for(i=0; i<npixels; i++) {
		float *d=rawdata[i]; //Données pour 1 pixel
		for(nm=0; nm<ny; nm++) {
			*p=d;
			p++;
			d+=ncols;
		}
	}
	return matrix;
}

/**
 * input rawdata[i][j] matrice avec i la couche, et j le pixel
 * ouput data[k][l] matrice avec k le pixel d'une année (ny*npixels) et l la couche
 */
double **tomatrix_bis(double **rawdata, int nbpixels, int ncols, int ny)
{
	int nm, i;
	double **matrix=(double **)malloc( sizeof(double*)*nbpixels*ny );
	if( matrix == NULL ) {
		fprintf(stderr, "tomatrix : matrix allocation error\n");
		return NULL;
	}
	double **p=matrix;
	for(nm=0; nm<ny; nm++) {
		double *data=rawdata[nm];
		double *d=data;
		for(i=0; i<nbpixels; i++) {
			*p=d;
			p++;
			d+=ncols;
		}
	}
	return matrix;
}


/**
 * transforms a vector of length nrows*ncols into a matrix int[nrows][ncols]
 */
int **tomatrix_int(int *rawdata, int nrows, int ncols)
{
	int nm;
	int **matrix=(int **)malloc( sizeof(int*)*nrows );
	if( matrix == NULL ) {
		fprintf(stderr, "tomatrix : matrix allocation error\n");
		return NULL;
	}
	int **p=matrix;
	int *d=rawdata;
	for(nm=0; nm<nrows; nm++) {
		*p=d;
		p++;
		d+=ncols;
	}
	return matrix;
}

int **alloc_matrix_int(int nrows, int ncols) {
	int i;
	int **matrix=(int **)malloc( sizeof(int*)*nrows );
	if( matrix == NULL ) {
		fprintf(stderr, "alloc_matrix_int : matrix allocation error\n");
		return NULL;
	}

	for(i=0; i<nrows; i++) {
		matrix[i]=(int *)malloc( sizeof(int)*ncols );
		if(matrix[i]==NULL) {
			fprintf(stderr, "alloc_matrix_int : memory allocation error\n");
			return NULL;
		}
	}
	return matrix;
}

unsigned int **alloc_matrix_uint(int nrows, int ncols) {
	int i;
	unsigned int **matrix=(unsigned int **)malloc( sizeof(unsigned int*)*nrows );
	if( matrix == NULL ) {
		fprintf(stderr, "alloc_matrix_uint : matrix allocation error\n");
		return NULL;
	}

	for(i=0; i<nrows; i++) {
		matrix[i]=(unsigned int *)malloc( sizeof(unsigned int)*ncols );
		if(matrix[i]==NULL) {
			fprintf(stderr, "alloc_matrix_uint : memory allocation error\n");
			return NULL;
		}
	}
	return matrix;
}

/**
 * Allocation de matrice selon le principe des Ragged Array (liste de pointeurs de pointeurs)
 */
float **alloc_matrix_float(int nrows, int ncols) {
	int i;
	float **matrix=(float **)malloc( sizeof(float*)*nrows );
	if( matrix == NULL ) {
		fprintf(stderr, "alloc_matrix_float : matrix allocation error\n");
		return NULL;
	}

	for(i=0; i<nrows; i++) {
		matrix[i]=(float *)malloc( sizeof(float)*ncols );
		if(matrix[i]==NULL) {
			fprintf(stderr, "alloc_matrix_float : memory allocation error\n");
			//Libération de la mémoire déjà allouée
			int j;
			for(j=0; j<i; j++) free(matrix[j]);
			free(matrix);
			return NULL;
		}
	}
	return matrix;
}


void free_matrix(float **mat, int nclusters)
{
	int i;
	for(i=0; i<nclusters; i++) {
		free(mat[i]);
		mat[i]=NULL;
	}
	free(mat);
	mat=NULL;
}

void free_matrix_int(int **mat, int nclusters)
{
	int i;
	for(i=0; i<nclusters; i++) {
		free(mat[i]);
	}
	free(mat);
}

void free_matrix_uint(unsigned int **mat, int nclusters)
{
	int i;
	for(i=0; i<nclusters; i++) {
		free(mat[i]);
	}
	free(mat);
}
