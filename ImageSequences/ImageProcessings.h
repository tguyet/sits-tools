
#ifndef _IMAGEPROCESSINGS_H_
#define _IMAGEPROCESSINGS_H_

void convolution(float **image, int nx, int ny, float **filtre, int fnx, int fny);
void convolution_maj(unsigned int **image, int nx, int ny, int fs, int nmax);
void convolution_maj(unsigned int *image, int nx, int ny, int fs, int nmax);

#endif
