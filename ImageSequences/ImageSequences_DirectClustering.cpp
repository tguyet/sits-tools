 /*
 * ImageSequences_DirectClustering.cpp
 *
 * This file is a part of the long-term SITS (Satellite Image Time Series) classification tool.
 * 
 * author: guyet
 * version: 2.0
 * last modification : 24/10/2013
 */

#include "ImageSequences_DirectClustering.h"

#include <stdlib.h>
#include <string.h>
#include <list>
#include <vector>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <float.h>
#include <math.h>

#include "gdal_priv.h"
#include "cpl_conv.h" // for CPLMalloc()
#include "ogrsf_frmts.h"

extern "C" {
#include "cluster.h"
#include "Matrix_Utils.h"
}


using namespace std;


int get_timeseries_class(float *data, float** profiles, int dim, int nbprofiles, float *error = NULL)
{
	//Un pixel est invalide s'il ne contient que des NaN, sinon il est classé !
	int invalid=1;
	for(int k=0; k<dim; k++) {
		if( !isnan(data[k]) ) {
			invalid=0;
		}
	}
	if(invalid) return -1;

	float dist, r;
	float mindist=FLT_MAX;
	int argmin=-1;
	for(int i=0; i<nbprofiles;i++) {
		dist =0.0;
		for(int k=0; k<dim; k++) {
			if( isnan(data[k]) ) continue; //On passe les valeurs invalides !
			r=profiles[i][k]-data[k];
			dist+=r*r;
		}
		if( dist<mindist) {
			mindist=dist;
			argmin=i;
		}
	}

	assert(argmin!=-1);

	if(error!=NULL) *error = mindist;
	return argmin;
}

int *attributes(images_params ip, params &p, float **profiles)
{
	GDALDataset *poDataset;
	GDALRasterBand *poBand;
	float *pafScanline=NULL;
	float **data;
	std::list<int>::iterator ipit;
	int *clusterid = (int*)malloc(sizeof(int)*p.sx*p.sy);
	float *pixel=(float*)malloc(sizeof(float)*p.ny*ip.band_per_year);
	int *currentcluster=clusterid;
	int nbli=0;

	GDALAllRegister();

	poDataset = (GDALDataset *) GDALOpen( ip.hdrfilename, GA_ReadOnly );
	if( poDataset == NULL ) {
		std::cerr << "error while openning " << ip.hdrfilename << std::endl;
		return NULL;
	}

	int nbpart=p.nb_processing_parts;

	//Allocation de la mémoire nécessaire le chargement des bandes !
	data = (float **)malloc(sizeof(float*)*p.ny*ip.band_per_year);

	p.attribution_error=0;

	for(int part=0; part<nbpart; part++) {
		std::cout<< "part " << part << "/" << nbpart <<" !"<<endl;

		if( part!=(nbpart-1) ) {
			nbli=p.sy/nbpart;
		} else {
			//ICI c'est le dernier batch à traiter ... on s'assure d'aller jusqu'à la fin !
			nbli= p.sy - ( part*(p.sy/nbpart) );
		}

		int bandid=0;
		for(int annee=ip.startyear; annee<ip.endyear; annee++) {

			int start_band=ip.start_band+ip.band_per_year*(annee-ip.firstyear)*ip.year_order;
			int end_band=(ip.band_order<0?start_band-ip.band_per_year:start_band+ip.band_per_year);

			for(int band=start_band;(ip.band_order<0?band>end_band:band<end_band); band+=ip.band_order) {
				poBand = poDataset->GetRasterBand( band );

				if( poBand == NULL )
					continue;

				pafScanline = (float *) malloc(sizeof(float)*p.sx*nbli);
				if(pafScanline==NULL) {
					std::cerr << "memory allocation error" << std::endl;
					return NULL;
				}

				poBand->RasterIO( GF_Read, p.x0, p.y0 + part*(p.sy/nbpart), p.sx, nbli, pafScanline, p.sx, nbli, GDT_Float32, 0, 0 );

				data[bandid]=pafScanline;
				bandid++;
			}
		}

		//Attribution
		for(int j=0; j<p.sx*nbli; j++) {
			for(int d=0; d<p.ny*ip.band_per_year; d++) {
				pixel[d]=data[d][j];
			}
			float lerror;
			*currentcluster=get_timeseries_class( pixel, profiles, p.ny*ip.band_per_year, p.n_decade, &lerror);
			currentcluster++;
			p.attribution_error+=lerror;
		}

		//Liberation de la mémoire
		for(int d=0; d<p.ny*ip.band_per_year; d++) {
			free(data[d]);
		}
	}

	free(pixel);
	free(data);

	GDALClose(poDataset);

	return clusterid;
}



/*
int *fill_invalidpixels(int *input, list<int> invalidpixels, float **profiles, images_params ip, params p)
{
	//Allocation de toute la matrice
	int *resultalloc=(int *)malloc( sizeof(int)*p.sx*p.sy );
	if(resultalloc==NULL) {
		cerr << "fill_invalidpixels result allocation error" <<endl;
		return NULL;
	}
	memset(resultalloc, 0, sizeof(int)*p.sx*p.sy);

	list<int>::iterator ipit = invalidpixels.begin();

	int *result=resultalloc;
	int i=0;
	int j=0;
	while(i==*ipit && i< (p.sx)*(p.sy)) {
		*result=-1;
		i++;
		ipit++;
		result++;
		for(int k=1;k<p.sample_rate;k++) {
			if( i>=(p.sx)*(p.sy) ) break;
			*result = -2;
			i++;
			result++;
		}
	}
	assert( i<(p.sx)*(p.sy) );

	while( i<(p.sx)*(p.sy) ) {
		if(i%p.sample_rate==0) {
			*result= input[j];
			i++;
			result++;
			j++;
			for(int k=1;k<p.sample_rate;k++) {
				if(i>=(p.sx)*(p.sy)) break; //Evite de déborder tout à la fin !
				*result=-2;
				i++;
				result++;
			}
		} else {
			*result=-2;
			result++;
			i++;
		}
		while( i==*ipit && i<(p.sx)*(p.sy) ) {
			*result=-1;
			result++;
			i++;
			ipit++;
			for(int k=1;k<p.sample_rate;k++) {
				if(i>=(p.sx)*(p.sy)) break; //Evite de déborder tout à la fin !
				*result=-2;
				result++;
				i++;
			}
		}
	}
	assert( j==p.nb_pixels );

	return resultalloc;
}
*/


int directclustering(images_params ip, params &pp)
{
	float error;
	int ifound;
	float **data=NULL;
	float **profiles=NULL;
	list<int> invalidpixels;

	printf("LOAD TIME SERIES DATA\n");
	data= load_timeseries(ip, &pp, invalidpixels);
	printf("\t-> ok !\n");

	int ncols= ip.band_per_year * pp.ny; //Nombre de couches

	//Préparation du K-Means
	int *clusterid = (int *)malloc(sizeof(int)*pp.nb_pixels);
	if( clusterid==NULL ) {
		cerr << "clusterid allocation error" <<endl;
		return EXIT_FAILURE;
	}
	memset(clusterid, 0, sizeof(int)*pp.nb_pixels);

	float *weight = (float *)malloc(sizeof(float)*ncols);
	for(int i=0; i<ncols; i++) {
		weight[i]=1;
	}

	//
	// Lancement du K-Means
	//
	printf("*********\nKMEANS %d classes, %d individus, %d dimensions\n*********\n", pp.n_decade, pp.nb_pixels, ncols);
	kcluster(pp.n_decade, pp.nb_pixels, ncols, data, NULL /*mask*/, weight, 0 /*transpose*/, 1 /*nbpass*/, pp.kmeans_maxiter /*maxiter*/, 'a' /*method*/, 'e' /*dist*/, clusterid, &profiles, &error, &ifound);

	//
	// Transformation en valeurs de 1 à n_decade
	// + construction du vecteur des profils
	//
	float **profils = alloc_matrix_float(pp.n_decade, pp.ny*ip.band_per_year);
	int *correspondances = (int*)malloc( sizeof(int)*pp.n_decade );
	int nbcor=0;
	for(int i=0; i<pp.nb_pixels; i++) {
		int val = clusterid[i];
		int found=0;
		for(int j=0; j<nbcor; j++) {
			if( correspondances[j]==val ) {
				clusterid[i]=j;
				memcpy(profils[j], data[val], sizeof(float)*pp.ny*ip.band_per_year);
				found=true;
				break;
			}
		}
		if(found) continue;

		correspondances[nbcor]=clusterid[i];
		clusterid[i]=nbcor;
		nbcor++;
	}
	assert(nbcor==pp.n_decade);


	//Libération mémoire
	free_matrix(data, pp.nb_pixels);
	free(weight);

	//
	//	Reconstruction d'un vecteur avec tous les pixels !
	//
	//int *result=fill_invalidpixels(clusterid, invalidpixels, profils, ip, pp);
	int *result=attributes(ip, pp, profils);

	//Libération mémoire
	free(clusterid);
	invalidpixels.clear();

	//
	//	Sauvergarde de l'image
	//
	char filename[200];
	sprintf(filename, "%s/Direct_%d_%d_c%d_c%d", ip.output_basename, ip.startyear, ip.endyear, pp.n_annualprofiles, pp.n_decade );
	save_hdr_decade(filename, pp, result);

	//Libération mémoire
	free(result);

	return EXIT_SUCCESS;
}

