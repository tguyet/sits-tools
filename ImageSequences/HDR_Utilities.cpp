 /*
 * HDR_Utilities.cpp
 *
 * This file is a part of the long-term SITS (Satellite Image Time Series) classification tool.
 * 
 * author: guyet
 * version: 2.0
 * last modification : 24/10/2013
 */

#include "gdal_priv.h"
#include "cpl_conv.h" // for CPLMalloc()
#include "ogrsf_frmts.h"
#include <iostream>
#include <set>
#include "HDR_Utilities.h"
#include "ImageSequences.h"
#include <assert.h>
#include <list>

#include <stdio.h>
#include <math.h>


extern "C" {
#include "Matrix_Utils.h"
}

/**
 * p : contient les paramètres de l'image à sauvegarder
 */
int savehdr(const char *hdrfilename, images_params ip, params p, int *data)
{
	GDALAllRegister();

	const char *pszFormat = "ENVI";
	GDALDriver *poDriver= GetGDALDriverManager()->GetDriverByName(pszFormat);
	if( poDriver == NULL )
		exit( 1 );

	GDALDataset *poDstDS;
	poDstDS = poDriver->Create( hdrfilename, p.sx, p.sy, p.ny, GDT_Int32, NULL/*papszOptions*/ );

	if( poDstDS!=NULL ) {
		GDALRasterBand *poBand;

		poDstDS->SetGeoTransform( p.adfGeoTransform );

		char *metadata_name = (char *)malloc(sizeof(char)*10);
		sprintf(metadata_name, "YEAR");
		char *metadata_value = (char *)malloc(sizeof(char)*50);

		for(int ny=1; ny<=p.ny; ny++) {
			int *yresult = data + (p.sx*p.sy)*(ny-1);
			poBand = poDstDS->GetRasterBand(ny);
			poBand->RasterIO( GF_Write, 0 , 0, p.sx, p.sy, yresult, p.sx, p.sy, GDT_Int32, 0, 0 );

			sprintf(metadata_value, "%d", ip.startyear+ny-1);
			poBand->SetMetadataItem(metadata_name, metadata_value);
		}

		free(metadata_name);
		free(metadata_value);

		GDALClose( (GDALDatasetH) poDstDS );
	} else {
		return 1;
	}
	return 0;
}

int save_hdr_decade(const char *hdrfilename, params p, int *data)
{
	GDALAllRegister();

	const char *pszFormat = "ENVI";
	GDALDriver *poDriver= GetGDALDriverManager()->GetDriverByName(pszFormat);
	if( poDriver == NULL )
		return( 1 );

	GDALDataset *poDstDS;
	poDstDS = poDriver->Create( hdrfilename, p.sx, p.sy, 1, GDT_Int32, NULL/*papszOptions*/ );

	if( poDstDS!=NULL ) {
		GDALRasterBand *poBand;

		poDstDS->SetGeoTransform( p.adfGeoTransform );
		poBand = poDstDS->GetRasterBand(1);
		poBand->RasterIO( GF_Write, 0 , 0, p.sx, p.sy, data, p.sx, p.sy, GDT_Int32, 0, 0 );

		GDALClose( (GDALDatasetH) poDstDS );
	} else {
		std::cerr << "save_hdr_decade error"<<std::endl;
		return 1;
	}
	return 0;
}

/**
 * Charge l'intégralité des pa->ny couches de decades dans une vecteur de taille pa->ny * pa->sx * pa->sy !
 *
 * Les valeurs -A sont également chargées
 */
int *load_hdr_decade(const char *hdrfilename, params *pa)
{
	GDALDataset *poDataset;
	GDALRasterBand *poBand;
	int *data, *pdata;

	GDALAllRegister();

	poDataset = (GDALDataset *) GDALOpen( hdrfilename, GA_ReadOnly );
	if( poDataset == NULL ) {
		std::cerr << "error while openning " << hdrfilename << std::endl;
		return NULL;
	}

	poDataset->GetGeoTransform( pa->adfGeoTransform );
	pa->ny=poDataset->GetRasterCount();
	pa->sx=poDataset->GetRasterXSize();
	pa->sy=poDataset->GetRasterYSize();
	printf("> %d couches de taille %dx%d\n", pa->ny, pa->sx, pa->sy);

	data=(int*)malloc( sizeof(int*) * pa->ny * pa->sx * pa->sy );
	if(data==NULL) {
		std::cerr << "memory allocation error" << std::endl;
		return NULL;
	}
	pdata=data;
	for(int band=1; band<=pa->ny; band++) {
		poBand = poDataset->GetRasterBand( band );

		if( poBand == NULL )
			continue;

		poBand->RasterIO( GF_Read, 0, 0, pa->sx, pa->sy, pdata, pa->sx, pa->sy, GDT_Int32, 0, 0 );
		pdata+=(pa->sx)*(pa->sy);
	}

	GDALClose(poDataset);
	return data;
}


/**
 * Remplis quelques valeurs pour p à partir des informations du fichier hdr
 */
void get_fileparams(images_params ip, params *p)
{
	GDALDataset *poDataset;

	GDALAllRegister();

	poDataset = (GDALDataset *) GDALOpen( ip.hdrfilename, GA_ReadOnly );
	if( poDataset == NULL ) {
		std::cerr << "error while openning " << ip.hdrfilename << std::endl;
		return;
	}

	poDataset->GetGeoTransform( p->adfGeoTransform );

	if( ip.X0!=0 ) {
		p->x0=(int) ( (ip.X0-p->adfGeoTransform[0])/p->adfGeoTransform[1] );
		p->y0=(int) ( (ip.Y0-p->adfGeoTransform[3])/p->adfGeoTransform[5] );
		p->x1=(int) ( (ip.X1-p->adfGeoTransform[0])/p->adfGeoTransform[1] );
		p->y1=(int) ( (ip.Y1-p->adfGeoTransform[3])/p->adfGeoTransform[5] );

		p->sx = p->x1-p->x0;
		p->sy = p->y1-p->y0;
	} else {
		p->sx =poDataset->GetRasterXSize();
		p->sy =poDataset->GetRasterYSize();
		p->x0=0;
		p->y0=0;
		p->x1=p->sx;
		p->y1=p->sy;
	}
	GDALClose( poDataset );

	p->ny=ip.endyear-ip.startyear;

	//Modification des info géométriques pour retourner qqc qui correspond à la matrice
	p->adfGeoTransform[0]=p->adfGeoTransform[0]+p->x0*p->adfGeoTransform[1];
	p->adfGeoTransform[1]=p->adfGeoTransform[1];
	p->adfGeoTransform[2]=p->adfGeoTransform[2];
	p->adfGeoTransform[3]=p->adfGeoTransform[3]+p->y0*p->adfGeoTransform[5];
	p->adfGeoTransform[4]=p->adfGeoTransform[4];
	p->adfGeoTransform[5]=p->adfGeoTransform[5];
}

/**
 * La fonction lit le fichier et construit une liste de positions valides selon
 * une lecture tous les p->sample_rate pixels à partir du pixel decal.
 * La fonction calcul p->nb_pixels valides correspondant à ce type de lecture
 * \param ip (in) paramètres d'une image
 * \param p (in/out) paramètres des algorithmes, le valeur nb_pixels est modifiées
 * \param decal (in) indique la valeur de décalage de lecture (doit être entre 0 et p->sample_rate)
 */
std::list<int> search_invalidpixels(images_params ip, params *p, int decal)
{
	GDALDataset *poDataset;
	GDALRasterBand *poBand;
	float *pafScanline;

	if( decal<0 || decal>=p->sample_rate ) {
		std::cerr << "decal invalid\n";
		return std::list<int>();
	}

	GDALAllRegister();

	poDataset = (GDALDataset *) GDALOpen( ip.hdrfilename, GA_ReadOnly );
	if( poDataset == NULL ) {
		std::cerr << "error while openning " << ip.hdrfilename << std::endl;
    	return std::list<int>();
    }

	std::list< int > invalidpixels;
	pafScanline = (float *) malloc(sizeof(float)* p->sx * p->sy);
	if(pafScanline==NULL) {
		std::cerr << "memory allocation error" << std::endl;
		return std::list<int>();
	}


	for(int annee=ip.startyear; annee<ip.endyear; annee++) {
		int start_band=ip.start_band+ip.band_per_year*(annee-ip.firstyear)*ip.year_order;
		int end_band=(ip.band_order<0?start_band-ip.band_per_year:start_band+ip.band_per_year);
		printf("\ninvalid pixel : ****** %d **** : %d %d\n", annee, start_band, end_band);

		for(int band=start_band;(ip.band_order<0?band>end_band:band<end_band); band+=ip.band_order) {
			poBand = poDataset->GetRasterBand( band );

			if( poBand == NULL )
				continue;

			// ATTENTION : GDT_Float64 pour des float
			// 			mais GDT_Float32 pour des float !!
			poBand->RasterIO( GF_Read, p->x0, p->y0, p->sx, p->sy, pafScanline, p->sx, p->sy, GDT_Float32, 0, 0 );

			float *pa=pafScanline;
			pa+=decal;
			for(int i=decal; i<p->sx*p->sy; i+=p->sample_rate, pa+=p->sample_rate) {
				if( isnan(*pa) ) {
					invalidpixels.push_back(i);
				}
			}
		}
	}
	free(pafScanline);
	GDALClose(poDataset);

	invalidpixels.sort();
	invalidpixels.unique();

	p->nb_pixels= (p->sx*p->sy-decal)/p->sample_rate - invalidpixels.size();
	if( ((p->sx*p->sy)-decal)%p->sample_rate!=0 ) p->nb_pixels++;

	return invalidpixels;
}


/**
 * (deprecated) Fonction devant servir à compléter les données d'une classification d'utilisant
 * pas l'intégralité des données
 * La fonction nécessite une ouverture/fermeture de fichier (donc lente !)
 * \deprecated
 */
float *load_data(int x0, int y0, int annee, images_params ip, params p)
{
	GDALDataset *poDataset;
	GDALRasterBand *poBand;
	float pafScanpos;
	float *data;

	if(x0<0 || y0<0 || x0 >=p.sx || y0>=p.sy) return NULL;

	GDALAllRegister();

	poDataset = (GDALDataset *) GDALOpen( ip.hdrfilename, GA_ReadOnly );
	if( poDataset == NULL ) {
		std::cerr << "error while openning " << ip.hdrfilename << std::endl;
		return NULL;
	}

	//Allocation de l'ensemble des données
	data = (float*)malloc(sizeof(float)*ip.band_per_year);

	//Construction du vecteur avec lecture des bandes
	int start_band=ip.start_band+ip.band_per_year*(annee-ip.firstyear)*ip.year_order;
	int end_band=(ip.band_order<0?start_band-ip.band_per_year:start_band+ip.band_per_year);

	int bandid=0;
	for(int band=start_band;(ip.band_order<0?band>end_band:band<end_band); band+=ip.band_order) {
		poBand = poDataset->GetRasterBand( band );

		if( poBand == NULL )
			continue;

		// ATTENTION : GDT_Float64 pour des float
		// 			mais GDT_Float32 pour des float !!
		poBand->RasterIO( GF_Read, p.x0+x0, p.y0+y0, 1, 1, &pafScanpos, 1, 1, GDT_Float32, 0, 0 );
		data[bandid]=pafScanpos;
		bandid++;
	}

	GDALClose(poDataset);
	return data;
}


/**
 * \brief Chargement des bandes de données pour une année
 * \param ip paramètres de l'image
 * \param p paramètres des algorithmes
 * \param yearid numero de l'année depuis l'année de début (start_year)
 * \param part partie du fichier à charger (permet de déterminer la position de la partie à charger !)
 * \param nbparts nombre de parties à charger du fichier (permet de déterminer la taille de charque partie)
 * \param nbli (out) nombre de pixel dans la partie traitées, au début, il y a p.sx*(p.sy/nbparts) pixels
 * 		traités, la taille du dernier paquet dépend du nombre de pixels restants
 * \return Retourne une matrice data[i][j] où j(= x+width*y) est l'index
 * du pixel (x*y) dans l'image, et i donne la couche dans l'image
 * La taille de data est nbli * ip.band_per_year  !
 *
 *
 * Dans cette fonction il n'y a pas de gestion des NaN : toute l'image est chargée !
 * La séparation des parties se fait sur les lignes (les lignes sont lues entièrement).
 */
float **load_annual_data_part(images_params ip, params p, int yearid, int part, int nbparts, int *nbli)
{
	GDALDataset *poDataset;
	GDALRasterBand *poBand;
	float *pafScanline=NULL;

	float **data;

	GDALAllRegister();

	poDataset = (GDALDataset *) GDALOpen( ip.hdrfilename, GA_ReadOnly );
	if( poDataset == NULL ) {
		std::cerr << "load_annual_data_part : error while openning " << ip.hdrfilename << std::endl;
		return NULL;
	}

	int start_band=ip.start_band+ip.band_per_year*((ip.startyear+yearid) -ip.firstyear)*ip.year_order;
	int end_band=(ip.band_order<0?start_band-ip.band_per_year:start_band+ip.band_per_year);
	int b=0;

	if( part!=(nbparts-1) ) {
		*nbli=p.sy/nbparts;
	} else {
		//ICI c'est le dernier batch à traiter ... on s'assure d'aller jusqu'à la fin !
		*nbli= p.sy - ( part*(p.sy/nbparts) );
	}

	//alloc the matrix
	data = alloc_matrix_float(p.sx * (*nbli), ip.band_per_year);
	if(data==NULL) {
		std::cerr << "load_annual_data_part : memory allocation error" << std::endl;
		return NULL;
	}

	//alloc a line
	pafScanline = (float *) calloc(p.sx * (*nbli), sizeof(float) );
	if(pafScanline==NULL) {
		std::cerr << "load_annual_data_part : memory allocation error" << std::endl;
		//Liberation de mémoire déjà allouée :
		free_matrix(data, p.sx * (*nbli));
		return NULL;
	}

	for(int band=start_band;(ip.band_order<0?band>end_band:band<end_band); band+=ip.band_order) {
		poBand = poDataset->GetRasterBand( band );

		if( poBand == NULL )
			continue;

		// On commence la lecture à partir de la position p.x, p.sy+part*(p.sx * p.sy)/nbparts
		// On lit au total p.x * nbli pixels du fichier
		poBand->RasterIO( GF_Read, p.x0, p.y0+part*(p.sy/nbparts), p.sx, *nbli, pafScanline, p.sx, *nbli, GDT_Float32, 0, 0 );

		//on replace correctement les pixels
		for(int i=0; i<p.sx * (*nbli);i++) {
			data[i][b]=pafScanline[i];
		}
		b++;
	}
	free(pafScanline);

	GDALClose(poDataset);
	return data;
}

/**
 * \return Retourne une matrice data[i][j] prète à classer !
 * où i est l'index du pixel (anne+position), et j donne la couche dans l'image
 * \param decal : décalage de lecture pour le cas où il y a un p->sample_rate différent de 1. decal doit être entre 0 et p->sample_rate-1, sinon
 * on utilise decal%p->sample_rate comme valeur de décalage.
 */
float **load_data(images_params ip, params *p)
{
	GDALDataset *poDataset;
	GDALRasterBand *poBand;
	float *pafScanline;
	float **data;
	std::list<int>::iterator ipit;
	std::list<int> invalids;

	int decal = 0; //TODO random !

	if(decal >= p->sample_rate || decal<0 ) {
		std::cerr << "modification de decal\n";
		decal = decal%p->sample_rate;
	}

	get_fileparams(ip, p);

	print(&ip, p);

	if( p->use_invalidpixels ) {
		//Construction de la liste ORDONNEE des positions de pixels non-valides
		// Cette étape permet de calculer le nombre nécessaire de pixels à allouer !
		invalids=search_invalidpixels(ip, p, decal);
	} else {
		p->nb_pixels = p->sx*p->sy;
	}

	std::cout << "\tread " << p->sx << "x" << p->sy << " ("<<p->nb_pixels << " pixels)"<<std::endl;
	std::cout << "\t#years: " << p->ny << std::endl;
	std::cout << "\tsampling rate: " << p->sample_rate << std::endl;
	std::cout << "\tuse invalid pixels: " << p->use_invalidpixels << std::endl;


	GDALAllRegister();

	poDataset = (GDALDataset *) GDALOpen( ip.hdrfilename, GA_ReadOnly );
	if( poDataset == NULL ) {
		std::cerr << "error while openning " << ip.hdrfilename << std::endl;
		return NULL;
	}

	pafScanline = (float *) malloc(sizeof(float)* (p->sx) * (p->sy));
	if(pafScanline==NULL) {
		std::cerr << "memory allocation error" << std::endl;
		return NULL;
	}

	/*******************************
	 *  Construction d'une matrice data[i][j] où i est l'index du pixel
	 *  (annee+position), et j donne la couche dans l'image
	 *******************************/

	//Allocation de l'ensemble des données
	data = alloc_matrix_float(p->ny*p->nb_pixels, ip.band_per_year);


	// Lecture des bandes dans l'ordre ip.band_order et dans l'ordre des années ip.year_order
	// Parcours des bandes de sample_rate en sample_rate (sous échantillonnage) !

	for(int annee=ip.startyear; annee<ip.endyear; annee++) {
		printf("**** %d ****\n", annee);
		int start_band=ip.start_band+ip.band_per_year*(annee-ip.firstyear)*ip.year_order;
		int end_band=(ip.band_order<0?start_band-ip.band_per_year:start_band+ip.band_per_year);
			std::cout << "\t"<< ip.startyear <<", " << ip.band_per_year<<", "<< ip.firstyear<<", "<<annee << std::endl;
			std::cout << "\tread bands "<< start_band <<" to " << end_band<<"." << std::endl;

		int bandid=0;
		for(int band=start_band;(ip.band_order<0?band>end_band:band<end_band); band+=ip.band_order) {
			poBand = poDataset->GetRasterBand( band );
			std::cout << "\t\t band "<< band << std::endl;

			if( poBand == NULL )
				continue;

			// ATTENTION : GDT_Float64 pour des float
			// 			mais GDT_Float32 pour des float !!

			poBand->RasterIO( GF_Read, p->x0, p->y0, p->sx, p->sy, pafScanline, p->sx, p->sy, GDT_Float32, 0, 0 );

			//Recopie uniquement les pixels valides dans pixeldata
			ipit = invalids.begin();
			int i=decal;
			int j=0;
			while( i==*ipit && i<(p->sx)*(p->sy) ) {
				i+=p->sample_rate;
				ipit++;
			}
			if( i>=(p->sx)*(p->sy) ) { //Il doit au moins y avoir un pixel valide
				std::cerr << "aucun pixel valide ?" << std::endl;
				free(pafScanline);
				free(data);
				return NULL;
			}

			while( i<(p->sx)*(p->sy) ) {
				data[j + p->nb_pixels*(annee-ip.startyear)][bandid]=pafScanline[i];
				j++;
				i+=p->sample_rate;
				while( i==*ipit && i<(p->sx)*(p->sy) ) {
					i+=p->sample_rate;
					ipit++;
				}
			}

			assert( j<=p->nb_pixels );
			if(j!=p->nb_pixels) {
				std::cerr << "missing "<< (p->nb_pixels-j) << " pixels" << std::endl;
			}
			//assert( j==p->nb_pixels );

			bandid++;
		}

	}
	free(pafScanline);
	GDALClose(poDataset);

	return data;
}


/**
 * \return Retourne une matrice data[i][j] prète à classer !
 * où i est l'index du pixel (position), et j donne la couche dans l'image (23 * ny couches)
 * \param decal : décalage de lecture pour le cas où il y a un p->sample_rate différent de 1. decal doit être entre 0 et p->sample_rate-1, sinon
 * on utilise decal%p->sample_rate comme valeur de décalage.
 */
float **load_timeseries(images_params ip, params *p, std::list<int> &invalids)
{
	GDALDataset *poDataset;
	GDALRasterBand *poBand;
	float *pafScanline;
	float **data;
	std::list<int>::iterator ipit;

	int decal = 0; //TODO random !

	if( decal<0 ) decal=-decal;
	if(decal >= p->sample_rate ) {
		std::cerr << "modification de decal\n";
		decal = decal%p->sample_rate;
	}

	get_fileparams(ip, p);

	//Construction de la liste ORDONNEE des positions de pixels non-valides
	// Cette étape permet de calculer le nombre nécessaire de pixels à allouer !
	std::cout << "\tStart searching invalid pixels (sample_rate "<<p->sample_rate<<")"<<std::endl;
	invalids=search_invalidpixels(ip, p, decal);
	std::cout << "\t\tdone"<<std::endl;

	std::cout << "\tTotal pixels to read " << p->sx << "x" << p->sy << " ("<<p->nb_pixels << " pixels)"<<std::endl;

	GDALAllRegister();

	poDataset = (GDALDataset *) GDALOpen( ip.hdrfilename, GA_ReadOnly );
	if( poDataset == NULL ) {
		std::cerr << "error while openning " << ip.hdrfilename << std::endl;
		return NULL;
	}

	pafScanline = (float *) malloc(sizeof(float)* (p->sx) * (p->sy));
	if(pafScanline==NULL) {
		std::cerr << "memory allocation error" << std::endl;
		return NULL;
	}


	//Allocation de l'ensemble des données
	std::cout << "\tMemory allocation"<< std::endl;
	data = alloc_matrix_float(p->nb_pixels, p->ny*ip.band_per_year);

	std::cout << "\tStart reading"<<std::endl;
	int bandid=0;
	for(int annee=ip.startyear; annee<ip.endyear; annee++) {

		int start_band=ip.start_band+ip.band_per_year*(annee-ip.firstyear)*ip.year_order;
		int end_band=(ip.band_order<0?start_band-ip.band_per_year:start_band+ip.band_per_year);

		for(int band=start_band;(ip.band_order<0?band>end_band:band<end_band); band+=ip.band_order) {
			poBand = poDataset->GetRasterBand( band );

			if( poBand == NULL )
				continue;

			// ATTENTION : GDT_Float64 pour des float
			// 			mais GDT_Float32 pour des float !!

			poBand->RasterIO( GF_Read, p->x0, p->y0, p->sx, p->sy, pafScanline, p->sx, p->sy, GDT_Float32, 0, 0 );

			//Recopie uniquement les pixels valides dans pixeldata
			ipit = invalids.begin();
			int i=decal;
			int j=0;
			while( i==*ipit && i<(p->sx)*(p->sy) ) {
				i+=p->sample_rate;
				ipit++;
			}
			if( i>=(p->sx)*(p->sy) ) { //Il doit au moins y avoir un pixel valid
				std::cerr << "aucun pixel valide ?" << std::endl;
				free(pafScanline);
				free(data);
				return NULL;
			}

			while( i<(p->sx)*(p->sy) ) {
				data[j][bandid]=pafScanline[i];
				j++;
				i+=p->sample_rate;
				while( i==*ipit && i<(p->sx)*(p->sy) ) {
					i+=p->sample_rate;
					ipit++;
				}
			}
			assert( j<=p->nb_pixels );
			if(j!=p->nb_pixels) {
				std::cerr << "missing "<< (p->nb_pixels-j) << " pixels" << std::endl;
			}


			std::cout << "\t\tband "<< bandid << " done"<<std::endl;
			bandid++;
		}

	}
	std::cout << "\t\tFINISHED"<<std::endl;
	free(pafScanline);
	GDALClose(poDataset);

	return data;
}


