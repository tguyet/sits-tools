/*
 * ImageProcessings.cpp
 *
 *  Created on: 14 déc. 2013
 *      Author: tguyet
 */

#include "ImageProcessings.h"
#include <cassert>
#include <cstdlib>
#include <cstdio>


extern "C" {
#include "Matrix_Utils.h"
}


int min(int v, int u) {
	return (v<u?v:u);
}

int max(int v, int u) {
	return (v>u?v:u);
}

float process(int x, int y, float **image, int nx, int ny, float **filtre, int fnx, int fny) {
	int sx=max(0, x-(fnx-1)/2);
	int ex=min(nx, x+(fnx-1)/2);
	int sy=max(0, y-(fny-1)/2);
	int ey=min(ny, y+(fny-1)/2);
	int ii=sx, ji;
	float val=0;
	for(int i=0;i<fnx && ii<ex; i++) {
		ji=sy;
		for(int j=0; j<fny && ji<ey; j++) {
			val+=filtre[ii][ji]*image[i][j];
			ji++;
		}
		ii++;
	}
	return val;
}


//fnx et fny doivent être impair
void convolution(float **image, int nx, int ny, float **filtre, int fnx, int fny) {
	// on traite ligne par ligne, on ne conserve une image que de la hauteur d'un demi filtre
	//

	assert( fnx%2==1 && fny%2==1 );

	int tampon_size=(fnx+1)/2;

	float **tampon = alloc_matrix_float(tampon_size, ny);
	int cur_idx=0; //indices de la ligne à utiliser
	int img_idx=0;

	// Premiere partie du traitement avec
	// chargement du tampon
	for(int i=0; i<tampon_size; i++) {
		for(int j=0; j<ny; j++) {
			tampon[cur_idx][j]=process(i, j, image, nx, ny, filtre, fnx, fny);
		}
		cur_idx++;
	}
	cur_idx=0;

	// Reste de l'image
	for(int i=tampon_size+1; i<nx; i++) {
		//Copie la ligne dans l'image
		for(int j=0; j<ny; j++) {
			image[img_idx][j] = tampon[cur_idx][j];
		}
		img_idx++;

		// Calcul de la ligne suivante dans le tampon
		for(int j=0; j<ny; j++) {
			tampon[cur_idx][j]=process(i, j, image, nx, ny, filtre, fnx, fny);
		}
		//mise à jour de la ligne du tampon
		cur_idx=(tampon_size==(cur_idx-1)?0:cur_idx+1);
	}

	// Finalisation de l'image
	// on vide le tampon
	for(;cur_idx<nx;cur_idx++) {
		for(int j=0; j<ny; j++) {
			image[img_idx][j] = tampon[cur_idx][j];
		}
		img_idx++;
	}

	free_matrix(tampon, tampon_size);
}


/**
 * Compute the majority class
 * \param image array of integer assumed to be between 0 and nmax
 * \param fs filter size (must be an odd value)
 * \param nmax max value
 */
int process_maj(int x, int y, unsigned int **image, int nx, int ny, int fs, unsigned int nmax) {
	int sx=max(0, x-(fs-1)/2);
	int ex=min(nx, x+(fs-1)/2);
	int sy=max(0, y-(fs-1)/2);
	int ey=min(ny, y+(fs-1)/2);
	int *histo = (int*)malloc(sizeof(int)*nmax);
	for(int i=sx;i<ex; i++) {
		for(int j=sy; j<ey; j++) {
			histo[ image[i][j] ]++;
		}
	}
	//get the first max
	int argmax=0;
	int vmax=histo[0];
	for(unsigned int i=1;i<nmax; i++) {
		if(histo[i]>vmax) {
			vmax=histo[i];
			argmax=i;
		}
	}
	return argmax;
}


/*
 * Compute the majority class filter of an integer image
 * \param fs filter size, must be an odd value
 * \param image array of integer between 0 and nmax
 * \param nmax maximum value in the image
 * \param nx, ny : image size
 *
 * The image array is modified by the function
 * The required memory to compute the convolution is fs/2*ny.
*/
void convolution_maj(unsigned int **image, int nx, int ny, int nmax, int fs) {
	assert( fs%2==1 );

	int tampon_size=(fs+1)/2;

	unsigned int **tampon = alloc_matrix_uint(tampon_size, ny);
	int cur_idx=0; //indices de la ligne à utiliser
	int img_idx=0;

	// Premiere partie du traitement avec
	// chargement du tampon
	for(int i=0; i<tampon_size; i++) {
		for(int j=0; j<ny; j++) {
			tampon[cur_idx][j]=process_maj(i, j, image, nx, ny, fs, nmax);
		}
		cur_idx++;
	}
	cur_idx=0;

	// Reste de l'image
	for(int i=tampon_size+1; i<nx; i++) {
		//Copie la ligne dans l'image
		for(int j=0; j<ny; j++) {
			image[img_idx][j] = tampon[cur_idx][j];
		}
		img_idx++;

		// Calcul de la ligne suivante dans le tampon
		for(int j=0; j<ny; j++) {
			tampon[cur_idx][j]=process_maj(i, j, image, nx, ny, fs, nmax);
		}
		//mise à jour de la ligne du tampon
		cur_idx=(tampon_size==(cur_idx-1)?0:cur_idx+1);
	}

	// Finalisation de l'image
	// on vide le tampon
	for(;cur_idx<nx;cur_idx++) {
		for(int j=0; j<ny; j++) {
			image[img_idx][j] = tampon[cur_idx][j];
		}
		img_idx++;
	}

	free_matrix_uint(tampon, tampon_size);
}

////////////////////////////////////////////////////////////////////////////

/**
 * Compute the majority class
 * \param image array of integer assumed to be between 0 and nmax
 * \param fs filter size (must be an odd value)
 * \param nmax max value
 */
int process_maj(int x, int y, unsigned int *image, int nx, int ny, int fs, unsigned int nmax) {
	int sx=max(0, x-(fs-1)/2);
	int ex=min(nx, x+(fs-1)/2-1);
	int sy=max(0, y-(fs-1)/2);
	int ey=min(ny, y+(fs-1)/2-1);
	int *histo = (int*)malloc(sizeof(int)*nmax);
	for(unsigned int i=0;i<nmax; i++) histo[i]=0;
	for(int i=sx;i<ex; i++) {
		for(int j=sy; j<ey; j++) {
			assert( image[i*ny+j]<nmax && image[i*ny+j]>=0 );
			histo[ image[i*ny+j] ]++;
		}
	}
	//get the first max
	int argmax=0;
	int vmax=histo[0];
	for(unsigned int i=1;i<nmax; i++) {
		if(histo[i]>vmax) {
			vmax=histo[i];
			argmax=i;
		}
	}
	free(histo);
	return argmax;
}


void convolution_maj(unsigned int *image, int nx, int ny, int nmax, int fs) {
	assert( fs%2==1 );

	int tampon_size=(fs+1)/2+1;

	unsigned int **tampon = alloc_matrix_uint(tampon_size, ny);
	int cur_idx=0; //indices de la ligne à utiliser
	int img_idx=0;

	// Premiere partie du traitement avec
	// chargement du tampon
	for(int i=0; i<tampon_size; i++) {
		for(int j=0; j<ny; j++) {
			tampon[cur_idx][j]=process_maj(i, j, image, nx, ny, fs, nmax);
		}
		cur_idx++;
	}

	//on recommence le tampon
	cur_idx=0;

	// Reste de l'image
	for(int i=tampon_size; i<nx; i++) {
		//Copie la ligne dans l'image
		for(int j=0; j<ny; j++) {
			image[img_idx*ny+j] = tampon[cur_idx][j];
		}
		img_idx++;

		// Calcul de la ligne suivante dans le tampon
		for(int j=0; j<ny; j++) {
			tampon[cur_idx][j]=process_maj(i, j, image, nx, ny, fs, nmax);
		}
		// Mise à jour de la ligne du tampon
		cur_idx=(tampon_size==(cur_idx+1)?0:cur_idx+1);
		//printf("%d\n", cur_idx);
	}

	// Finalisation de l'image
	// on vide le tampon
	for(;img_idx<nx;img_idx++) {
		for(int j=0; j<ny; j++) {
			image[img_idx*ny+j] = tampon[cur_idx][j];
		}
		cur_idx=(tampon_size==(cur_idx+1)?0:cur_idx+1);
	}

	free_matrix_uint(tampon, tampon_size);
}
