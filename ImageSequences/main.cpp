/*
 * main.cpp
 *
 * This file is a part of the long-term SITS (Satellite Image Time Series) classification tool.
 * 
 * author: guyet
 * version: 2.0
 * last modification : 24/10/2013
 */


/**
 * Faire un:
 *
 * export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH
 * setenv LD_LIBRARY_PATH /local/lib:$LD_LIBRARY_PATH
 *
 * avant de lancer le programme pour qu'il sache où aller la librairie (non installée)
 */

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include "ImageSequences.h"
#include "HDR_Utilities.h"
#include "QFZSegmentation.h"
#include "ImageSequences_DirectClustering.h"
#include "ImageProcessings.h"

extern "C" {
#include "Matrix_Utils.h"
}


void usage(const char *comm);

int main(int argc, char **argv)
{
	int *result=NULL;
	char filename[50];
	float **cdist=NULL;
	char outputname[50];
	outputname[0]=0;
	int *idximage=NULL;
	bool first_stage_only=0, second_stage_only=0, direct=0;
	int datetag=0;

	//Paramètres de l'images
	images_params ip;
	init_images_params(&ip);

	//Paramètres de l'algorithme
	params p;
	init_params(&p);
/*
	ip.endyear=2007;
	p.sample_rate=13;
	p.kmeans_maxiter=30;
	p.n_annualprofiles=30;
	p.n_decade=100;
	p.n_successions=3000;
	p.n_threads=2;
//*/

/* Zone test avec trous ...
	ip.endyear=2004;
	ip.X0=302651;
	ip.Y0=2197244;
	ip.X1=407595;
	ip.Y1=2130476;
	p.kmeans_maxiter=50;
	p.sample_rate=20;
	p.sample_rate_dec=20;
	p.n_annualprofiles=10;
	p.n_decade=30;
	p.n_successions=3000;
//*/


/* Zone de taille intermédiaire
	ip.endyear=2008;
	ip.X0=258428;
	ip.Y0=1604205;
	ip.X1=844586;
	ip.Y1=1195301;
	p.kmeans_maxiter=50;
	p.sample_rate=20;
	p.n_annualprofiles=30;
	p.n_decade=100;
	p.n_successions=3000;
//*/


/* Traitement de la région de Kelkome seule
	ip.endyear=2004;

	//ip.X0=388809;
	//ip.Y0=1662763;
	//ip.X1=501883;
	//ip.Y1=1581741;

	p.kmeans_maxiter=50;
	p.kmedoid_maxiter=50;
	p.sample_rate=20;
	p.sample_rate_dec=50;
	p.n_annualprofiles=7;
	p.n_decade=15;
	p.n_successions=3000;
//*/

//* Traitement du Sénégal
	ip.endyear=2006;
	ip.X0=227046;
	ip.Y0=1845443;
	ip.X1=896135;
	ip.Y1=1361950;
	p.kmeans_maxiter=50;
	p.kmedoid_maxiter=50;
	p.sample_rate=20;
	p.sample_rate_dec=500;
	p.n_annualprofiles=10;
	p.n_decade=50;
	p.n_successions=2000;
//*/

	for(int i=1;i<argc;i++)
	{
		if( !strcmp(argv[i],"-h") ) {
			usage(argv[0]);
			return EXIT_SUCCESS;
		} else if(!strcmp(argv[i],"-na")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&p.n_annualprofiles) || p.n_annualprofiles<=0 ) {
				fprintf(stderr, "Parameter error: Expected strictly positive integer after -na\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-kmeans")) {
			p.clustering_method=KM_CLUSTERING;
		} else if(!strcmp(argv[i],"-nd")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&p.n_decade) || p.n_decade<=0 ) {
				fprintf(stderr, "Parameter error: Expected strictly positive integer after -nd\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-ns")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&p.n_successions) || p.n_successions<=0 ) {
				fprintf(stderr, "Parameter error: Expected strictly positive integer after -ns\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		}  else if(!strcmp(argv[i],"-s")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%f",&p.s) ) {
				fprintf(stderr, "Parameter error: Expected number after -s\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-start")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&ip.startyear) || ip.startyear<=0 ) {
				fprintf(stderr, "Parameter error: Expected strictly positive integer after -start\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		}  else if(!strcmp(argv[i],"-end")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&ip.endyear) || ip.endyear<=0 ) {
				fprintf(stderr, "Parameter error: Expected strictly positive integer after -end\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-step")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&p.sample_rate) || p.sample_rate<=0 ) {
				fprintf(stderr, "Parameter error: Expected strictly positive integer after -step\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
		} else if(!strcmp(argv[i],"-step_dec")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&p.sample_rate_dec) || p.sample_rate_dec<=0 ) {
				fprintf(stderr, "Parameter error: Expected strictly positive integer after -step_dec\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
	    } else if(!strcmp(argv[i],"-geom")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%f",&ip.X0) || ip.X0<=0 ) {
				fprintf(stderr, "Parameter error: Expected 4 strictly positive integer after -geom\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%f",&ip.Y0) || ip.Y0<=0 ) {
				fprintf(stderr, "Parameter error: Expected 4 strictly positive integer after -geom\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%f",&ip.X1) || ip.X1<=0 ) {
				fprintf(stderr, "Parameter error: Expected 4 strictly positive integer after -geom\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%f",&ip.Y1) || ip.Y1<=0 ) {
				fprintf(stderr, "Parameter error: Expected 4 strictly positive integer after -geom\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
			if( ip.Y1 >ip.Y0) {
				float t=ip.Y1;
				ip.Y1=ip.Y0;
				ip.Y0=t;
			}
			if( ip.X0 >ip.X1) {
				float t=ip.X1;
				ip.X1=ip.X0;
				ip.X0=t;
			}

		}  else if(!strcmp(argv[i],"-mi")) {
			i++;
			if( i<0 || 1!=sscanf(argv[i],"%d",&p.kmeans_maxiter) || p.kmeans_maxiter<=0 ) {
				fprintf(stderr, "Parameter error: Expected strictly positive integer after -mi\n");
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}
			p.kmedoid_maxiter=p.kmeans_maxiter;
			p.ap_maxit=p.kmeans_maxiter;
		}  else if(!strcmp(argv[i],"-dt")) {
			datetag=1;
		}  else if(!strcmp(argv[i],"-o")) {
			i++;
			strcpy(outputname, argv[i]);
		} else if( !strcmp(argv[i],"-succ") ) {
			direct=0;
			if(first_stage_only) {
				second_stage_only=0;
				first_stage_only=0;
			} else {
				second_stage_only=1;
			}
		} else if( !strcmp(argv[i],"-dec") ) {
			direct=0;
			if(second_stage_only) {
				second_stage_only=0;
				first_stage_only=0;
			} else {
				first_stage_only=1;
			}
		} else if( !strcmp(argv[i],"-direct") ) {
			second_stage_only=0;
			first_stage_only=0;
			direct=1;
		} else {

			if( argv[i][0]=='-' ) {
				fprintf(stderr, "Parameter error: Unknown parameter %s\n", argv[i]);
				usage( argv[0] );
				exit( EXIT_FAILURE );
			}

			// C'est un fichier à charger
			ip.hdrfilename = argv[i];

			// Récupération du chemin ...
			char *co=(char *)malloc( sizeof(char)*(strlen(ip.hdrfilename)+1) );
			if(co!=NULL) {
				strcpy(co, argv[i]);
				char *lo=strrchr(co, '/');
				*lo=0;
				ip.input_basename=co;
			}
		}
	}

	//Construction d'un nom de fichier par défaut
	if( outputname[0]==0 ) {
		strcpy(outputname, "output");

		if(datetag) {
			time_t rawtime;
			struct tm * timeinfo;
			char strint[5];

			//Ajout d'un tag temporel sur le nom de fichier
			time ( &rawtime );
			timeinfo = localtime ( &rawtime );
			strcat(outputname, "_");
			sprintf(strint, "%d", timeinfo->tm_mday);
			strcat(outputname, strint );
			sprintf(strint, "%d", timeinfo->tm_mon);
			strcat(outputname, strint );
			strcat(outputname, "_");
			sprintf(strint, "%d", timeinfo->tm_hour);
			strcat(outputname, strint );
			sprintf(strint, "%d", timeinfo->tm_min);
			strcat(outputname, strint );
		}

		//Creation d'un repertoire de travail specifique
		char cmd[250];
		sprintf(cmd, "mkdir %s",outputname );
		int ret=system(cmd);
		if(ret) {
			fprintf(stderr, "Impossible de créer le répertoire %s\n", outputname);
			//exit(1);
		}
	}

	ip.output_basename=outputname;

	if( first_stage_only ) {
		cdist=alloc_matrix_float(p.n_annualprofiles, p.n_annualprofiles);
		if(p.clustering_method==EM_CLUSTERING) {
			result=create_decades_em(ip, &p, cdist);
		} else {
			result=create_decades(ip, &p, cdist);
		}
		if(!result) return 1;
		printf("overall attribution error : %f\n", p.attribution_error);

		printf("*********\nSAUVEGARDE SUCCESSIONS\n*********\n");
		sprintf(filename, "%s/succ_%d_%d_c%d", outputname, ip.startyear, ip.endyear, p.n_annualprofiles );
		savehdr(filename, ip, p, result );
		sprintf(filename, "%s/%s", outputname, CLUSTERS_FILE );
		save_clustersdistances(filename, cdist, p.n_annualprofiles);

	} else if( second_stage_only ) {
		result=load_hdr_decade(ip.hdrfilename, &p);
		if(!result) return 1;
		if(ip.input_basename==NULL) {
			sprintf(filename, "%s/%s", outputname, CLUSTERS_FILE );
		} else {
			sprintf(filename, "%s/%s", ip.input_basename, CLUSTERS_FILE );
		}
		cdist=load_clustersdistances(filename, &(p.n_annualprofiles));
		if(!cdist) return 1;

		idximage=create_successions(result, cdist, &ip, &p);
		if(!idximage) return 1;

		printf("*********\nSAUVEGARDE IMAGE FINALE\n*********\n");
		sprintf(filename, "%s/dec_%d_%d_c%d_c%d", outputname, ip.startyear, ip.endyear, p.n_annualprofiles, p.n_decade );
		save_hdr_decade(filename, p, idximage);
		free(idximage);

	} else if( direct ) {
		printf("************\nDIRECT CLASSIFICATION\n************\n");
		directclustering(ip, p);
		printf("overall attribution error : %lf\n", p.attribution_error);
	} else {
		cdist=alloc_matrix_float(p.n_annualprofiles, p.n_annualprofiles);
		if(p.clustering_method==EM_CLUSTERING) {
			result=create_decades_em(ip, &p, cdist);
		} else {
			result=create_decades(ip, &p, cdist);
		}
		if(!result) return 1;

		printf("*********\nSAUVEGARDE SUCCESSIONS\n*********\n");
		sprintf(filename, "%s/succ_%d_%d_c%d", outputname, ip.startyear, ip.endyear, p.n_annualprofiles );
		savehdr(filename, ip, p, result );
		sprintf(filename, "%s/%s", outputname, CLUSTERS_FILE );
		save_clustersdistances(filename, cdist, p.n_annualprofiles);

		printf("*********\nCREATE TYPICAL SUCCESSIONS\n*********\n");
		idximage=create_successions(result, cdist, &ip, &p);
		if(!idximage) return 1;

		printf("*********\nSAUVEGARDE IMAGE FINALE\n*********\n");
		sprintf(filename, "%s/dec_%d_%d_c%d_c%d", outputname, ip.startyear, ip.endyear, p.n_annualprofiles, p.n_decade );
		save_hdr_decade(filename, p, idximage);

		/*
		printf("*********\nIMAGE PROCESSING\n*********\n");
		convolution_maj((unsigned int*)idximage, p.sy, p.sx, p.n_decade, 5);

		printf("*********\nSAUVEGARDE IMAGE FINALE\n*********\n");
		sprintf(filename, "%s/fdec_%d_%d_c%d_c%d", outputname, ip.startyear, ip.endyear, p.n_annualprofiles, p.n_decade );
		save_hdr_decade(filename, p, idximage);
		*/

		/*
		printf("*********\nIMAGE PROCESSING\n*********\n");
		int **matrix=tomatrix_int(idximage, p.sx, p.sy);
		vector<CC> cc;
		QFZ(matrix, p.sx, p.sy, 0.1, cc, 0.01);
		//convolution_maj((unsigned int*)idximage, p.sy, p.sx, p.n_decade, 5);

		printf("*********\nSAUVEGARDE IMAGE FINALE\n*********\n");
		sprintf(filename, "%s/fdec_%d_%d_c%d_c%d", outputname, ip.startyear, ip.endyear, p.n_annualprofiles, p.n_decade );
		save_hdr_decade(filename, p, idximage);
		//*/


		free(idximage);
	}

	save_params(&ip, &p);

	if(result) free(result);
	if(cdist) free_matrix(cdist, p.n_annualprofiles);
	//free_images_params(ip);
	return 0;
}


/**
* \brief Fonction d'information sur l'usage du programme
*/
void usage(const char *comm)
{
  printf("*******************\n");
  printf("Satellite images time series analysis (SISTA)\n\n");
  printf("Brief : Segment SITS based on two temporal scales : years and decades.\n");
  printf("Author : Guyet Thomas, AGROCAMPUS-OUEST\n");
  printf("Year : 2013\n");
  printf("*******************\n\n");
  printf("command : %s [-na %%d] [-nd %%d] [-ns %%d] datafile\n\n", comm);
  printf("Options :\n");
  printf("\t -na : number of annual profiles (default: 30)\n");
  printf("\t -nd : number of annual decade successions (default: 200)\n");
  printf("\t -ns : number of annual clustered successions (default: 3000)\n");
  printf("\t -step : step size between pixels to clusters (default: 1, full clustering)\n");
  printf("\t -step_dec : step size between pixels to clusters in succession clustering (default: 1, full clustering)\n");
  printf("\t -o file : output file prefix (default: output) \n");
  printf("\t -start : start year (default: 2001) \n");
  printf("\t -end : end year (default: 2004) \n");
  printf("\t -mi : max. iteration of classification (KMeans and KMedoid) (default: 50) \n");
  printf("\t -dec : constructs only decades \n");
  printf("\t -succ : constructs only succession based on a decades file \n");
  printf("\t -direct : constructs a file based on time series clustering \n");
  printf("\t -kmeans : use kmeans to cluster time series (EM by default)\n");
  printf("\t -geom X0 Y0 X1 Y1 : geolocalisation of the region to proceed (default: whole file)\n");
  printf("\t datafile : HDR datafile name (main data file, not the .hdr file)\n");
}
