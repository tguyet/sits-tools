

#include "global.h"
#include "workers.h"
#include "data.h"
#include "gmm.h"


#ifndef _train_h
#define _train_h

gmm* train_em(decimal **dataset, int dim, int size, int nmix);
number *classify_em(gmm * gmix, decimal **dataset, int dim, int size);

#endif

