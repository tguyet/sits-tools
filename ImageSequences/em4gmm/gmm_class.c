/* Expectation Maximization for Gaussian Mixture Models.
Copyright (C) 2012-2014 Juan Daniel Valor Miro

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details. */

#include "global.h"
#include "workers.h"
#include "data.h"
#include "gmm.h"

typedef struct{
	decimal result;       /* Variable to store the result found. */
	data *feas;           /* Shared pointer to loaded samples.   */
	number *clusterid;	  /* Shared pointer to the clustering result (TG) */
	gmm *gmix;            /* Shared pointer to gaussian mixture. */
	gmm *gworld;          /* Shared pointer to gaussian mixture. */
	number ini, end;      /* Initial and final sample processed. */
	/* Data used only on detailed classifier to store the log.   */
	workers_mutex *mutex; /* Common mutex to lock shared data.   */
	number *flag;         /* A flag for the first line on file.  */
}classifier;

/* Initialize the classifier by calculating the non-data dependant part. */
void gmm_init_classifier(gmm *gmix){
	decimal cache=gmix->dimension*log(2*NUM_PI);
	number m,j; gmix->llh=0;
	for(m=0;m<gmix->num;m++){
		gmix->mix[m].cgauss=gmix->mix[m]._z=0;
		for(j=0;j<gmix->dimension;j++){
			gmix->mix[m].cgauss+=log(gmix->mix[m].dcov[j]);
			gmix->mix[m].dcov[j]=1/gmix->mix[m].dcov[j];
			gmix->mix[m]._mean[j]=gmix->mix[m]._dcov[j]=0; /* Caches to 0. */
		}
		gmix->mix[m].cgauss=(2*gmix->mix[m].prior-(gmix->mix[m].cgauss+cache));
	}
}

/* Parallel and fast implementation of the Gaussian Mixture classifier. */
void thread_simple_classifier(void *tdata){
	classifier *t=(classifier*)tdata;
	decimal x,max,prob;
	number i,m,j,c=0;
	if(t->gworld!=NULL){ /* If the world model is defined, use it. */
		for(i=t->ini;i<t->end;i++){
			max=-HUGE_VAL;
			for(m=0;m<t->gworld->num;m++){
				prob=t->gworld->mix[m].cgauss;
				if(prob<max)continue; /* Speed-up the classifier. */
				for(j=0;j<t->gworld->dimension;j++){
					x=t->feas->data[i][j]-t->gworld->mix[m].mean[j];
					prob-=(x*x)*t->gworld->mix[m].dcov[j];
				}
				if(max<prob)max=prob;
			}
			t->result-=max; /* Compute final probability. */
		}
	}
	for(i=t->ini;i<t->end;i++){
		max=-HUGE_VAL;
		for(m=0;m<t->gmix->num;m++){
			prob=t->gmix->mix[m].cgauss; /* The precalculated non-data dependant part. */
			if(prob<max)continue; /* Speed-up the classifier. */
			for(j=0;j<t->gmix->dimension;j++){
				x=t->feas->data[i][j]-t->gmix->mix[m].mean[j];
				prob-=(x*x)*t->gmix->mix[m].dcov[j];
			}
			if(max<prob)max=prob,c=m; /* Fast classifier using Viterbi aproximation. */
		}
		t->result+=max;
		t->clusterid[i]=c;
	}
}

/* Efficient Gaussian Mixture classifier using a Viterbi approximation. */
number *gmm_simple_classify(data *feas, gmm *gmix, gmm *gworld, workers *pool) {
	number i,n=workers_number(pool),inc=feas->samples/n;
	decimal result=0;
	classifier *t=(classifier*)calloc(n,sizeof(classifier));
	number *clusterid=(number*)calloc(feas->samples,sizeof(number));
	for(i=0;i<n;i++){ /* Set and launch the parallel classify. */
		t[i].feas=feas;
		t[i].clusterid=clusterid;
		t[i].gmix=gmix;
		t[i].gworld=gworld;
		t[i].ini=i*inc;
		t[i].end=(i==n-1)?(feas->samples):((i+1)*inc);
		workers_addtask(pool,thread_simple_classifier,(void*)&t[i]);
	}
	workers_waitall(pool); /* Wait to the end of the parallel classify. */
	for(i=0;i<n;i++)
		result+=t[i].result;
	free(t);
//	return (result*0.5)/feas->samples;
	return clusterid;
}

