/* Expectation Maximization for Gaussian Mixture Models.
Copyright (C) 2012-2014 Juan Daniel Valor Miro

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details. */


#include "train.h"

/**
- dataset : the data matrix
- dim : data dimension
- size : nb of examples in the dataset
- nmix : number of clusters, if -1 default selection
*/
gmm *train_em(decimal **dataset, int dim, int size, int nmix) {
	number imax=100; //iteration max
	number i,o,x=0;
	number t=sysconf(_SC_NPROCESSORS_ONLN); //nombre de processeurs utilisés
	decimal last=INT_MIN, llh, sigma=0.01, m=-1.0;
	
	workers *pool=workers_create(t);
	//Allocation
	data *feas= (data*)malloc(sizeof(data));//feas_load(fnf,pool); /* Load the features from the specified disc file. s*/
	feas->data = dataset;
	feas->samples = size;    /* Samples number of the overall data.   */
	feas->dimension = dim;  /* Dimension number of the data loaded.  */
	feas->mean=(decimal*)calloc(2*feas->dimension,sizeof(decimal));
	feas->variance=feas->mean+feas->dimension;
	
	//initialisation of mean and variances
	int d;
	for(d=0;d<dim;d++) {
		for(i=0; i<size;i++) {
			feas->mean[d]+=feas->data[i][d];
			feas->variance[d]+=feas->data[i][d]*feas->data[i][d];
		}
		feas->mean[d]/=feas->samples;
		feas->variance[d]=(feas->variance[d]/feas->samples)-(feas->mean[d]*feas->mean[d]);
		if(feas->variance[d]<0.000001) feas->variance[d]=0.000001;
	}
	
	if(nmix==-1){
		if(m<0) m=0.95;
		nmix=sqrt(feas->samples/2);
	}
	gmm *gmix=gmm_initialize(feas, nmix); /* Good GMM initialization using data.    */
	fprintf(stdout,"Number of Components: %06i\n",gmix->num);
	for(o=1;o<=imax;o++){
		for(i=1;i<=imax;i++){
			llh=gmm_EMtrain(feas,gmix,pool); /* Compute one iteration of EM algorithm.   */
			fprintf(stdout,"Iteration: %05i    Improvement: %3i%c    LogLikelihood: %.3f\n", i,abs(round(-100*(llh-last)/last)),'%',llh); /* Show the EM results.  */
			if(last-llh>-sigma||isnan(last-llh))break; /* Break with sigma threshold. */
			last=llh;
		}
		x=gmix->num;
		if(m>=0){
			gmix=gmm_merge(gmix, feas, m, pool);
			fprintf(stdout,"Number of Components: %06i\n",gmix->num);
		}
		if(x==gmix->num) break;
		last=INT_MIN;
	}
	workers_finish(pool);
	
	//liberation memoire
	free(feas->mean);
	free(feas);
	
	return gmix;
}

number *classify_em(gmm * gmix, decimal **dataset, int dim, int size){
	number i=0;
	number t=sysconf(_SC_NPROCESSORS_ONLN);
	number *result;

	//Allocation
	data *feas= (data*)malloc(sizeof(data));//feas_load(fnf,pool); /* Load the features from the specified disc file. s*/
	feas->data = dataset;
	feas->samples = size;    /* Samples number of the overall data.   */
	feas->dimension = dim;  /* Dimension number of the data loaded.  */
	feas->mean=(decimal*)calloc(2*feas->dimension,sizeof(decimal));
	feas->variance=feas->mean+feas->dimension;
	
	//initialisation of mean and variances
	int d;
	for(d=0;d<dim;d++) {
		for(i=0; i<size;i++) {
			feas->mean[d]+=feas->data[i][d];
			feas->variance[d]+=feas->data[i][d]*feas->data[i][d];
		}
		feas->mean[d]/=feas->samples;
		feas->variance[d]=(feas->variance[d]/feas->samples)-(feas->mean[d]*feas->mean[d]);
		if(feas->variance[d]<0.000001) feas->variance[d]=0.000001;
	}

	workers *pool=workers_create(t);
	result=gmm_simple_classify(feas, gmix, NULL/*gworld*/, pool);
	workers_finish(pool);
	
	//liberation memoire
	free(feas->mean);
	free(feas);
	
	return result;
}

