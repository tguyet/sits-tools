/*
 * QFZSegmentation.cpp
 *
 *  Created on: 18 déc. 2013
 *      Author: tguyet
 */


#include "QFZSegmentation.h"

extern "C" {
#include "../Matrix_Utils.h"
}

#include <cmath>
#include <cfloat>
#include <set>
#include <map>
#include <iostream>
#include <cstdlib>
#include <cstring>


#define DEBUG 0


float max_dist(set<int> cor, vector<CC> concomp) {
	float difmax=-FLT_MAX;
	set<int>::iterator it = cor.begin();
	while( it!=cor.end() ) {
		set<int>::iterator jt = cor.begin();
		while( jt!=cor.end() ) {
			float val=abs( concomp[(*it)].min - concomp[(*jt)].max );
			difmax = (val<difmax?difmax:val);
			val=abs( concomp[(*it)].max - concomp[(*jt)].min );
			difmax = (val<difmax?difmax:val);
			jt++;
		}
		it++;
	}
	return difmax;
}

/**
 * \param input_image matrice de float à deux dimensions (accessible par notation double crochetée uniquement
 */
int **QFZ(float **input_image, int height, int width, float alpha, vector<CC> &concomp, float alphastep) {
	set< pair<int,int> > fusions;

	//matrice des CC
	int **labels = alloc_matrix_int(height, width);
	concomp.resize(height*width);
	for(int i=0;i<height; i++) {
		for(int j=0; j<width; j++) {
			labels[i][j]=i*width+j;
			concomp[i*width+j].min=input_image[i][j];
			concomp[i*width+j].max=input_image[i][j];
			concomp[i*width+j].mean=input_image[i][j];
			concomp[i*width+j].nb=1;
		}
	}

	float w=0;

	list< set<int> > corr;

	set<int> possibles; //add test
	for(int i=0;i<width*height;i++) possibles.insert(i); //add test

	for(float a=0; a<alpha; a+=alphastep) {
#if DEBUG
		cout << "level " << a << endl;
#endif
		fusions.clear();
		corr.clear();

		//set<int> possibles;
		for(int i=0; i<height; i++) {
			for(int j=0; j<width; j++) {
				bool fusdone=false;
				//Test la fusion vers la droite
				if( j<width-1 && labels[i][j]!=labels[i][j+1] ) { //on traite que si c'est un bord de zone
					if( abs(input_image[i][j]-input_image[i][j+1])<=a ) { //le bord permet la fusion
						//cout << "\tmerged " << i <<"," << j << " right" << endl;
						if( labels[i][j]<labels[i][j+1]) {
							fusions.insert( make_pair(labels[i][j], labels[i][j+1]) );
						} else {
							fusions.insert( make_pair(labels[i][j+1], labels[i][j]) );
						}
						fusdone=true;
						possibles.erase( labels[i][j+1] );
						possibles.erase( labels[i][j] );
					}
				}

				//Test la fusion vers le bas
				if( i<height-1 && labels[i][j]!=labels[i+1][j] ) { //on traite que si c'est un bord de zone
					if( abs(input_image[i][j]-input_image[i+1][j])<=a ) { //le bord permet la fusion
						//cout << "\tmerged " << i <<"," << j << " bottom" << endl;
						if( labels[i][j]<labels[i+1][j]) {
							fusions.insert( make_pair(labels[i][j], labels[i+1][j]) );
						} else {
							fusions.insert( make_pair(labels[i+1][j], labels[i][j]) );
						}
						fusdone=true;
						possibles.erase( labels[i+1][j] );
						possibles.erase( labels[i][j] );
					}
				}

				//if( !fusdone )  possibles.insert( labels[i][j] );

			}//for j
		}// for i


		set<int>::iterator itp=possibles.begin();
		while( itp!=possibles.end() ) {
			set<int> s;
			s.insert(*itp);
			corr.push_back(s);
			itp++;
		}

		list< set<int> >::iterator itm;

		set< pair<int,int> >::iterator it=fusions.begin();
		while( it!=fusions.end() ) {
			int c1=(*it).first;
			int c2=(*it).second;
#if DEBUG
			cout << "fusion "<< c1 << ","<< c2 << endl;
#endif

			// on recherche si c1 ou c2 sont déjà référencés
			bool found1=false, found2=false;
			list< set<int> >::iterator itm1=corr.begin();
			while( itm1!=corr.end() ) {
				if( (*itm1).find(c1)!=(*itm1).end() ) {
					// on ajoute c2
					found1=true;
					break;
				}
				itm1++;
			}
			list< set<int> >::iterator itm2=corr.begin();
			while( itm2!=corr.end() ) {
				if( (*itm2).find(c2)!=(*itm2).end() ) {
					found2=true;
					break;
				}
				itm2++;
			}
			if( !found1 && !found2 ) {
#if DEBUG
			cout << "\tnew entry, test max(" << w <<")." << endl;
#endif
				//on ajoute une nouvelle entrée

				set<int> s;
				s.insert(c1);
				s.insert(c2);
				if( max_dist(s, concomp) <= w) {
					corr.push_back(s);
#if DEBUG
					cout << "\t\tdo merge"<<endl;
#endif
				} else {
					set<int> s1;
					s1.insert(c1);
					corr.push_back(s1);
					set<int> s2;
					s2.insert(c2);
					corr.push_back(s2);
				}
			} else if( found1 && found2 ) {
#if DEBUG
			cout << "\t merging 2 existing components, test max(" << w <<")." << endl;
#endif
				if( itm1!=itm2 ) {
					//on teste la fusion
					set<int> s;
					s.insert((*itm2).begin(), (*itm2).end());
					s.insert((*itm1).begin(), (*itm1).end());
					if( max_dist(s, concomp) <= w) {
#if DEBUG
						cout << "\t\tdo merge"<<endl;
#endif
						//on transvase tout dans un seul semble
						(*itm1).insert((*itm2).begin(), (*itm2).end());
						//(*itm2).clear();
						corr.erase(itm2);
					} //sinon, on ne fait rien
				}
			} else if (found1) {
#if DEBUG
			cout << "\tadd to existing region (1), test max(" << w <<")." << endl;
#endif
				set<int> s;
				s.insert((*itm1).begin(), (*itm1).end());
				s.insert(c2);
				if( max_dist(s, concomp) <= w) {
#if DEBUG
						cout << "\t\tdo merge"<<endl;
#endif
					(*itm1).insert(c2);
				} else {
					set<int> s;
					s.insert(c2);
					corr.push_back(s);
				}
			} else if (found2) {
#if DEBUG
			cout << "\tadd to existing region (2), test max(" << w <<")." << endl;
#endif
				set<int> s;
				s.insert((*itm2).begin(), (*itm2).end());
				s.insert(c1);
				if( max_dist(s, concomp) <= w) {
					(*itm2).insert(c1);
				} else {
					set<int> s;
					s.insert(c1);
					corr.push_back(s);
				}
			}
			it++;
		}

#if DEBUG
		cout << "///////////"<<endl;
		itm=corr.begin();
		while( itm!=corr.end() ) {
			set<int>::iterator itp=(*itm).begin();
			while( itp!=(*itm).end() ) {
				cout << *itp << ",";
				itp++;
			}
			cout << endl;
			itm++;
		}
		cout << "///////////"<<endl;
#endif

		map<int, int> labelcorr; //correspondances entre les labels avant (en clé) pour donner les nouveaux labels (en valeur)
		list<CC> newcomp; //liste des nouvelles CC, l'ordre correspond aux valeurs de labelcorr

		possibles.clear(); //add test

		//On met à jour en calculant les plus grandes différences
		itm=corr.begin();
		int id=0;
		while( itm!=corr.end() ) {
			//on fusionne dans de nouvelles connected components
			CC cc;
			cc.max=FLT_MIN;
			cc.min=FLT_MAX;
			cc.nb=0;
			cc.mean=0;
			set<int>::iterator it = (*itm).begin();
			while( it!=(*itm).end() ) {
				labelcorr[*it]=id;
				cc.max=(cc.max<concomp[*it].max?concomp[*it].max:cc.max);
				cc.min=(cc.min>concomp[*it].min?concomp[*it].min:cc.min);
				cc.mean+= (concomp[*it].mean*concomp[*it].nb);
				cc.nb+=concomp[*it].nb;
				it++;
			}
			cc.mean/=concomp[*it].nb;
			newcomp.push_back(cc);
			possibles.insert(id); //add test

			id++;
			itm++;
		}

		//on modifie les labels (pixel à pixel)
		for(int i=0;i<height; i++) {
			for(int j=0; j<width; j++) {
				labels[i][j]=labelcorr[ labels[i][j] ];
			}
		}

		//on recopie les CC
		concomp.clear();
		concomp.resize(id);
		list<CC>::iterator itl= newcomp.begin();
		id=0;
		while( itl!= newcomp.end() ) {
			concomp[id]=*itl;
			id++;
			itl++;
		}

#if DEBUG
		cout << "//////////////"<<endl;
		for(int i=0;i<height; i++) {
			for(int j=0; j<width; j++) {
				cout.width(3);
				cout << labels[i][j]<< ",";
			}
			cout << endl;
		}
		cout << "//////////////"<<endl;
#endif

		w+=alphastep; //update the threshold
	} // next a
	return labels;
}


/**
 * Calcul la différence absolue maximum entre les
 * deux vecteurs selon les différentes dimensions.
 */
float diffmax(float *v1, float *v2, int d) {
	float dmax=abs(v1[0]-v2[0]);
	for(int j=1; j<d; j++) {
		float diff=abs(v1[j]-v2[j]);
		dmax=(diff>dmax?diff:dmax);
	}
	return dmax;
}

/**
 * Calcul de la distance maximale entre deux éléments si on fusionne
 * les composantes de concomp
 * La distance est calculée comme la distance euclidienne entre deux valeurs d'une unique dimension !
 */
float max_dist(set<int> cor, vector<CCd> concomp, int d) {
	float difmax=-FLT_MAX;
	set<int>::iterator it = cor.begin();
	while( it!=cor.end() ) {
		set<int>::iterator jt = cor.begin();
		while( jt!=cor.end() ) {
			float val=diffmax( concomp[(*it)].min, concomp[(*jt)].max, d );
			difmax = (val<difmax?difmax:val);
			val=diffmax( concomp[(*it)].max, concomp[(*jt)].min, d );
			difmax = (val<difmax?difmax:val);
			jt++;
		}
		it++;
	}
	return difmax;
}




/**
 * QFZ en multi-dimension
 * \param input_image vecteur de taille (height*width) x d correspondant à une image de taille
 * height*width avec des pixels de d dimensions
 */
int *QFZMD(float **input_image, int height, int width, int d, float alpha, vector<CCd> &concomp, float alphastep, int debuglevel) {
	set< pair<int,int> > fusions;

	//matrice des labels (output)
	int *labels = (int *)calloc(height*width, sizeof(int) );
	//matrice des CC
	concomp.resize(height*width);

	set<int> possibles; //add test
	for(int i=0;i<height*width; i++) {
		labels[i]=i;
		concomp[i].min=(float*)calloc(d, sizeof(float));
		memcpy(concomp[i].min,input_image[i],d*sizeof(float));
		concomp[i].max=(float*)calloc(d, sizeof(float));
		memcpy(concomp[i].max,input_image[i],d*sizeof(float));
		concomp[i].mean=(float*)calloc(d, sizeof(float));
		memcpy(concomp[i].mean,input_image[i],d*sizeof(float));
		concomp[i].nb=1;

		possibles.insert(i); //add test
	}

#if DEBUG
	cout << "///// INPUT /////"<<endl;
	for(int dd=0; dd<d; dd++) {
		cout << "///// dim " << dd << " /////"<<endl;
		for(int i=0; i<height; i++) {
			for(int j=0; j<width; j++) {
				cout.width(3);
				cout << input_image[i*width+j][dd]<< ",";
			}
			cout << endl;
		}
	}
	cout << "////////////////////"<<endl;
#endif

	float v=0;
	// corr est une liste d'ensemble de régions (initialement des pixels). Chaque ensemble de régions est une zone de l'image au niveau supérieur.
	list< set<int> > corr;

	for(float a=0; a<alpha; a+=alphastep) { // Pour des niveaux décroissants (relaxation progressive de la contrainte de fusion)
		if(debuglevel>0) cout << "Level " << a << endl;

		//////////  ANALYZING POSSIBLE FUSIONS  //////////////
		// fusion est la liste des fusions possibles entre pairs d'éléments
		fusions.clear();

		for(int i=0; i<height; i++) {
			for(int j=0; j<width; j++) {
				bool fusdone=false;

				//Test la fusion vers la droite
				if( j<width-1 && labels[i*width+j]!=labels[i*width+j+1] ) { //on traite que si c'est un bord de zone
					if( diffmax(input_image[i*width+j],input_image[i*width+j+1], d)<=a ) { //le bord permet la fusion
						if( labels[i*width+j]<labels[i*width+j+1]) {
							fusions.insert( make_pair(labels[i*width+j], labels[i*width+j+1]) );
						} else {
							fusions.insert( make_pair(labels[i*width+j+1], labels[i*width+j]) );
						}
						fusdone=true;
						possibles.erase( labels[i*width+j+1] );
						possibles.erase( labels[i*width+j] );
						//cout << "fusion right ("<< i <<","<<j <<")" << endl;
					}
				}

				//Test la fusion vers le bas
				if( i<height-1 && labels[i*width+j]!=labels[(i+1)*width+j] ) { //on traite que si c'est un bord de zone
					if( diffmax(input_image[i*width+j],input_image[(i+1)*width+j],d)<=a ) { //le bord permet la fusion
						if( labels[i*width+j]<labels[(i+1)*width+j]) {
							fusions.insert( make_pair(labels[i*width+j], labels[(i+1)*width+j]) );
						} else {
							fusions.insert( make_pair(labels[(i+1)*width+j], labels[i*width+j]) );
						}
						fusdone=true;
						possibles.erase( labels[(i+1)*width+j] );
						possibles.erase( labels[i*width+j] );
					}
				}

				//if( !fusdone ) possibles.insert( labels[i*width+j] );

			} // for j
		} // for i

		//// MERGING ZONES ACCORDING TO POSSIBLE FUSIONS /////

		if(debuglevel>1) {
			cout << "\t#possible merging " << possibles.size() << endl;
		}

		corr.clear();
		// On remplit corr avec les zones qu'il ne sera pas possible de fusioner
		set<int>::iterator itp=possibles.begin();
		while( itp!=possibles.end() ) {
			set<int> s;
			s.insert(*itp);
			corr.push_back(s);
			itp++;
		}

		set< pair<int,int> >::iterator it=fusions.begin();
		while( it!=fusions.end() ) {
			//it est la description d'une fusion possible en deux régions c1 et c2 (labels)
			//	c1 est toujours inférieur à c2 (par construction des paires dans l'étape précédente)
			int c1=(*it).first;
			int c2=(*it).second;
#if DEBUG
			cout << "fusion "<< c1 << ","<< c2 << endl;
#endif

			// on recherche si c1 ou c2 sont déjà référencés
			bool found1=false, found2=false;
			list< set<int> >::iterator itm1=corr.begin();
			while( itm1!=corr.end() ) {
				if( (*itm1).find(c1)!=(*itm1).end() ) {
					// on ajoute c2
					found1=true;
					break;
				}
				itm1++;
			}
			list< set<int> >::iterator itm2=corr.begin();
			while( itm2!=corr.end() ) {
				if( (*itm2).find(c2)!=(*itm2).end() ) {
					found2=true;
					break;
				}
				itm2++;
			}

			/*
			if( c1==18*width+8 && c2==18*width+9) {
				cout << found1 << "," << found2 << endl;
			}
			if( c1==18*width+7 && c2==18*width+8) {
				cout << found1 << "," << found2 << endl;
			}
			if( c1==18*width+9 && c2==18*width+10) {
				cout << found1 << "," << found2 << endl;
			}*/

			if( !found1 && !found2 ) {
				//on ajoute une nouvelle entrée

				set<int> s;
				s.insert(c1);
				s.insert(c2);
				if( max_dist(s, concomp, d) <= v) {
					corr.push_back(s);
				} else {
					set<int> s1;
					s1.insert(c1);
					corr.push_back(s1);
					set<int> s2;
					s2.insert(c2);
					corr.push_back(s2);
				}
			} else if( found1 && found2 ) {
				if( itm1!=itm2 ) {
					//on teste la fusion
					set<int> s;
					s.insert((*itm2).begin(), (*itm2).end());
					s.insert((*itm1).begin(), (*itm1).end());
					if( max_dist(s, concomp, d) <= v) {
						//on transvase tout dans un seul semble
						(*itm1).insert((*itm2).begin(), (*itm2).end());
						//(*itm2).clear();
						corr.erase(itm2);
					} //sinon, on ne fait rien
				}
			} else if (found2) {
				//c2 est le seul élément en cours de fusion, c1 est 'libre'
				// on ajoute
				set<int> s;
				s.insert((*itm2).begin(), (*itm2).end());
				s.insert(c1);
				if( max_dist(s, concomp, d) <= v) {
					(*itm2).insert(c1);
				} else {
					set<int> s;
					s.insert(c1);
					corr.push_back(s);
				}
			} else if (found1) {
				set<int> s;
				s.insert((*itm1).begin(), (*itm1).end());
				s.insert(c2);
				if( max_dist(s, concomp, d) <= v) {
					(*itm1).insert(c2);
				} else {
					set<int> s;
					s.insert(c2);
					corr.push_back(s);
				}
			}
			it++;
		}

		if(debuglevel>1) {
			cout << "\t#labels after merging " << corr.size() << endl;
		}

		list< set<int> >::iterator itm;
#if DEBUG
		cout << "///////////"<<endl;
		itm=corr.begin();
		while( itm!=corr.end() ) {
			set<int>::iterator itp=(*itm).begin();
			while( itp!=(*itm).end() ) {
				cout << *itp << ",";
				itp++;
			}
			cout << endl;
			itm++;
		}
		cout << "///////////"<<endl;
#endif

		possibles.clear(); //add test

		map<int, int> labelcorr; //correspondances entre les labels avant (en clé) pour donner les nouveaux labels (en valeur)
		list<CCd> newcomp; //liste des nouvelles CC, l'ordre correspond aux valeurs de labelcorr

		//On met à jour en calculant les plus grandes différences
		itm=corr.begin();
		int id=0;
		while( itm!=corr.end() ) {
			//on fusionne dans de nouvelles connected components
			CCd cc;
			cc.max=(float*)calloc(sizeof(float),d);
			cc.min=(float*)calloc(sizeof(float),d);
			cc.mean=(float*)calloc(sizeof(float),d);
			for(int dim=0;dim<d;dim++) {
				cc.max[dim]=FLT_MIN;
				cc.min[dim]=FLT_MAX;
				cc.mean[dim]=0;
			}
			cc.nb=0;
			// les max/min d'une nouvelle zone sont calculées comme les max/min des max/min des régions qui ont été fusionnés
			set<int>::iterator it = (*itm).begin();
			while( it!=(*itm).end() ) {
				labelcorr[*it]=id;
				for(int dim=0;dim<d;dim++) {
					cc.max[dim]=(cc.max[dim]<concomp[*it].max[dim]?concomp[*it].max[dim]:cc.max[dim]);
					cc.min[dim]=(cc.min[dim]>concomp[*it].min[dim]?concomp[*it].min[dim]:cc.min[dim]);
					cc.mean[dim]+= (concomp[*it].mean[dim]*concomp[*it].nb);
				}
				cc.nb+=concomp[*it].nb;
				it++;
			}
			for(int dim=0;dim<d;dim++) {
				cc.mean[dim]/=concomp[*it].nb;
			}
			newcomp.push_back(cc);

			possibles.insert(id); //add test

			id++;
			itm++;
		}

		if(debuglevel>1) {
			cout << "modify label matrix " << corr.size() << endl;
		}

		//on modifie les labels (pixel à pixel)
		for(int i=0;i<height; i++) {
			for(int j=0; j<width; j++) {
				labels[i*width+j]=labelcorr[ labels[i*width+j] ];
			}
		}

		//on recopie les CC
		for(unsigned int c=0; c<concomp.size(); c++) {
			free(concomp[c].max);
			free(concomp[c].min);
			free(concomp[c].mean);
		}
		concomp.clear();
		concomp.resize(id);
		list<CCd>::iterator itl= newcomp.begin();
		id=0;
		while( itl!= newcomp.end() ) {
			concomp[id]=*itl;
			id++;
			itl++;
		}

#if DEBUG
		cout << "//////////////"<<endl;
		for(int i=0;i<height; i++) {
			for(int j=0; j<width; j++) {
				cout.width(3);
				cout << labels[i*width+j]<< ",";
			}
			cout << endl;
		}
		cout << "//////////////"<<endl;
#endif
		v+=alphastep;
	} // next a

	//on recopie les CC
	for(unsigned int c=0; c<concomp.size(); c++) {
		free(concomp[c].max);
		free(concomp[c].min);
		free(concomp[c].mean);
	}

	return labels;
}


void testQFZ(int alpha) {
	float **matrix=alloc_matrix_float(7,7);
	matrix[0][0]=1;matrix[0][1]=3;matrix[0][2]=8;matrix[0][3]=9;matrix[0][4]=7;matrix[0][5]=7;matrix[0][6]=2;
	matrix[1][0]=2;matrix[1][1]=0;matrix[1][2]=9;matrix[1][3]=8;matrix[1][4]=8;matrix[1][5]=9;matrix[1][6]=0;
	matrix[2][0]=3;matrix[2][1]=1;matrix[2][2]=4;matrix[2][3]=2;matrix[2][4]=1;matrix[2][5]=2;matrix[2][6]=4;
	matrix[3][0]=1;matrix[3][1]=1;matrix[3][2]=6;matrix[3][3]=3;matrix[3][4]=4;matrix[3][5]=2;matrix[3][6]=3;
	matrix[4][0]=3;matrix[4][1]=3;matrix[4][2]=7;matrix[4][3]=7;matrix[4][4]=6;matrix[4][5]=1;matrix[4][6]=1;
	matrix[5][0]=1;matrix[5][1]=0;matrix[5][2]=8;matrix[5][3]=8;matrix[5][4]=9;matrix[5][5]=5;matrix[5][6]=7;
	matrix[6][0]=2;matrix[6][1]=3;matrix[6][2]=9;matrix[6][3]=7;matrix[6][4]=8;matrix[6][5]=5;matrix[6][6]=9;

	vector<CC> concomp;
	int **labels = QFZ(matrix, 7, 7, alpha, concomp, 1);

	for(int i=0;i<7; i++) {
		for(int j=0; j<7; j++) {
			cout.width(3);
			cout << labels[i][j]<< ",";
		}
		cout << endl;
	}

	free_matrix_int(labels, 7);
	free_matrix(matrix, 7);
}


void testQFZMD() {
	float **matrix=alloc_matrix_float(49,2);
	/*
	matrix[0][0]=1;matrix[1][0]=3;matrix[2][0]=8;matrix[3][0]=9;matrix[4][0]=7;matrix[5][0]=7;matrix[6][0]=2;
	matrix[7][0]=2;matrix[8][0]=0.5;matrix[9][0]=9;matrix[10][0]=8;matrix[11][0]=8;matrix[12][0]=9;matrix[13][0]=0;
	matrix[14][0]=3;matrix[15][0]=1;matrix[16][0]=4;matrix[17][0]=2;matrix[18][0]=1;matrix[19][0]=2;matrix[20][0]=4;
	matrix[21][0]=1;matrix[22][0]=1;matrix[23][0]=6;matrix[24][0]=3;matrix[25][0]=4;matrix[26][0]=2;matrix[27][0]=3;
	matrix[28][0]=3;matrix[29][0]=3;matrix[30][0]=7;matrix[31][0]=7;matrix[32][0]=6;matrix[33][0]=1;matrix[34][0]=1;
	matrix[35][0]=1;matrix[36][0]=0;matrix[37][0]=8;matrix[38][0]=8;matrix[39][0]=9;matrix[40][0]=5;matrix[41][0]=7;
	matrix[42][0]=2;matrix[43][0]=3;matrix[44][0]=9;matrix[45][0]=7;matrix[46][0]=8;matrix[47][0]=5;matrix[48][0]=9;
	matrix[0][1]=1;matrix[1][1]=4;matrix[2][1]=8;matrix[3][1]=9;matrix[4][1]=7;matrix[5][1]=7;matrix[6][1]=2;
	matrix[7][1]=2;matrix[8][1]=0.5;matrix[9][1]=9;matrix[10][1]=8;matrix[11][1]=8;matrix[12][1]=9;matrix[13][1]=0;
	matrix[14][1]=3;matrix[15][1]=1;matrix[16][1]=4;matrix[17][1]=2;matrix[18][1]=1;matrix[19][1]=2;matrix[20][1]=4;
	matrix[21][1]=1;matrix[22][1]=1;matrix[23][1]=6;matrix[24][1]=3;matrix[25][1]=4;matrix[26][1]=2;matrix[27][1]=3;
	matrix[28][1]=3;matrix[29][1]=3;matrix[30][1]=7;matrix[31][1]=7;matrix[32][1]=6;matrix[33][1]=1;matrix[34][1]=1;
	matrix[35][1]=1;matrix[36][1]=0;matrix[37][1]=8;matrix[38][1]=8;matrix[39][1]=9;matrix[40][1]=5;matrix[41][1]=7;
	matrix[42][1]=2;matrix[43][1]=3;matrix[44][1]=9;matrix[45][1]=7;matrix[46][1]=8;matrix[47][1]=5;matrix[48][1]=9;
	*/

	//island
	matrix[0][0]=1;matrix[1][0]=3;matrix[2][0]=8;matrix[3][0]=9;matrix[4][0]=7;matrix[5][0]=7;matrix[6][0]=2;
	matrix[7][0]=2;matrix[8][0]=0.5;matrix[9][0]=9;matrix[10][0]=8;matrix[11][0]=8;matrix[12][0]=9;matrix[13][0]=0;
	matrix[14][0]=3;matrix[15][0]=1;matrix[16][0]=4;matrix[17][0]=2;matrix[18][0]=1;matrix[19][0]=2;matrix[20][0]=4;
	matrix[21][0]=1;matrix[22][0]=1;matrix[23][0]=6;matrix[24][0]=3;matrix[25][0]=4;matrix[26][0]=2;matrix[27][0]=3;
	matrix[28][0]=3;matrix[29][0]=3;matrix[30][0]=7;matrix[31][0]=7;matrix[32][0]=6;matrix[33][0]=1;matrix[34][0]=1;
	matrix[35][0]=1;matrix[36][0]=0;matrix[37][0]=8;matrix[38][0]=8;matrix[39][0]=9;matrix[40][0]=5;matrix[41][0]=7;
	matrix[42][0]=2;matrix[43][0]=3;matrix[44][0]=9;matrix[45][0]=7;matrix[46][0]=8;matrix[47][0]=5;matrix[48][0]=9;
	matrix[0][1]=1;matrix[1][1]=4;matrix[2][1]=8;matrix[3][1]=9;matrix[4][1]=7;matrix[5][1]=7;matrix[6][1]=2;
	matrix[7][1]=2;matrix[8][1]=0.5;matrix[9][1]=9;matrix[10][1]=8;matrix[11][1]=8;matrix[12][1]=9;matrix[13][1]=0;
	matrix[14][1]=3;matrix[15][1]=1;matrix[16][1]=4;matrix[17][1]=2;matrix[18][1]=1;matrix[19][1]=2;matrix[20][1]=4;
	matrix[21][1]=1;matrix[22][1]=1;matrix[23][1]=6;matrix[24][1]=3;matrix[25][1]=4;matrix[26][1]=2;matrix[27][1]=3;
	matrix[28][1]=3;matrix[29][1]=3;matrix[30][1]=7;matrix[31][1]=7;matrix[32][1]=6;matrix[33][1]=1;matrix[34][1]=1;
	matrix[35][1]=1;matrix[36][1]=0;matrix[37][1]=8;matrix[38][1]=8;matrix[39][1]=9;matrix[40][1]=5;matrix[41][1]=7;
	matrix[42][1]=2;matrix[43][1]=3;matrix[44][1]=9;matrix[45][1]=7;matrix[46][1]=8;matrix[47][1]=5;matrix[48][1]=9;

	vector<CCd> concomp;
	int *labels = QFZMD(matrix, 7, 7, 2, 2, concomp, 1);

	for(int i=0;i<7; i++) {
		for(int j=0; j<7; j++) {
			cout.width(3);
			cout << labels[i*7+j]<< ",";
		}
		cout << endl;
	}

	free(labels);
	free_matrix(matrix, 49);
}

